package constants;

/** Descriptions all success. Merchant, market, admin **/
public enum SuccessMsg {
    registration_merchant_msg("Ваша заявка получена. Наш менеджер свяжется с Вами."),
    ;

    private String value;
    private SuccessMsg(String value) {
        this.value = value; }

    public String getSuccessMsg(){
        return value;
    }
}
