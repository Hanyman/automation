package constants;


import com.github.javafaker.Faker;

import configs.AdminConfig;
import configs.GeneralMerchConfig;
import configs.SecondMerchConfig;
import configs.UrlsConfig;
import helpers.api.Steps;

import io.restassured.specification.RequestSpecification;
import lombok.AccessLevel;
import lombok.experimental.FieldDefaults;
import org.aeonbits.owner.ConfigFactory;

import java.util.Map;

import static steps.api.merchant.CommonMerchantApiSteps.getAbsPath;


@FieldDefaults(level = AccessLevel.PROTECTED)

public class ConstantApi {

    public static final String TOKEN_EMAIL = "0572c387c1b342a117d6a19aff7408e6";
    public static final String XSRF_TOKEN = "XSRF-TOKEN";
    public static final String BAR_CODE_EXIST = "4751023310473";
    public static final String ADMIN_SESSION = "admin_session";
    public static final String MAS_SESSION = "mas_session";
    public static final String LARAVEL_SESSION = "laravel_session";
    public static final String APIMS_SESSION = "apims_session";
    public static final String CART_SESSION = "cart_session";

    public static Faker faker = new Faker();
    public static Steps steps = new Steps();
    public static final UrlsConfig urlsConfig = ConfigFactory.create(UrlsConfig.class, System.getProperties());
    public static final AdminConfig adminConfig = ConfigFactory.create(AdminConfig.class, System.getProperties());
    public static final GeneralMerchConfig generalMerchConfig = ConfigFactory.create(GeneralMerchConfig.class, System.getProperties());
    public static final SecondMerchConfig secondMerchConfig = ConfigFactory.create(SecondMerchConfig.class, System.getProperties());
    public static String url_admin,url_merchant,url_market;
    Map<String, String> cookies_merchant, cookies_admin, cookies_market;
    String xsrf_token_merchant, xsrf_token_admin, session_merchant, session_admin, apims_session_market;
    public static RequestSpecification adminSpec,merchantSpec, merchSpecOnlyCookies,fileMerchantSpec,fileAdminSpec,fileMarketSpec;
    public static final String FILE_XLSX = getAbsPath("src/test/resources/files/testFile.xlsx");
    public static final String FILE_PDF = getAbsPath("src/test/resources/files/testFile.pdf");
    public static final String EXIST_EMAIL = "nomakanton@yandex.ru";




}
