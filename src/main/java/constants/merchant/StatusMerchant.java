package constants.merchant;

/** Descriptions all errors. Merchant, market, admin **/
public enum StatusMerchant {
    ACTIVATION("5"),
    FULL_CHECK("6"),
    ACTIVE("7"),
    SUSPENDED("8"),
    DISABLED("9");

    private String value;
    private StatusMerchant(String value) {
        this.value = value; }

    public String getStatusMerch(){
        return value;
    }
}
