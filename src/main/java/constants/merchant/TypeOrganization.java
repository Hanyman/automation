package constants.merchant;

/** Descriptions all errors. Merchant, market, admin **/
public enum TypeOrganization {
    OOO(1),
    IP(2);

    private int value;
    private TypeOrganization(int value) {
        this.value = value; }

    public int getTypeOrg(){
        return value;
    }
}
