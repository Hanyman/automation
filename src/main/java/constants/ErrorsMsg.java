package constants;

/** Descriptions all errors. Merchant, market, admin **/
public enum ErrorsMsg {
    invalid_number("Неверно введен номер"),
    invalid_fio("Неверно введено ФИО"),
    wrong_code("Неверный код"),
    obligatory_field("Обязательное поле"),
    obligatory_name_field("Укажите имя"),
    obligatory_email_field("Укажите email"),
    maximum_length__field("Не более 50 символов!"),
    maximum_length__surname_field("Превышена максимальная длина фамилии"),
    maximum_length__name_field("Превышена максимальная длина имени"),
    incorrect_email("Некорректный email"),
    obligatory_field_merchant("Обязательное поле!"),
    maximum_length_surname_field_merchant("Превышена максимальная длина фамилии!"),
    incorrect_email_merchant("Введите валидный e-mail!"),
    exist_email_merchant("Такой e-mail уже зарегистрирован!"),
    invalid_number_merchant("Неверный формат телефона!"),
    incorrect_inn_merchant("Должно быть не менее 10 символов!"),
    only_digits_merchant("Только цифры!"),
    incorrect_format_phone_merchant("Неверный формат телефона!");

    private String value;
    private ErrorsMsg(String value) {
        this.value = value; }

    public String getErrorMsg(){
        return value;
    }
}
