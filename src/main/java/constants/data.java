package constants;

public final class  data {

    public static final String DEFAULT_ADDRESS = "г Москва, ул Советская, д 9";
    public static final String DEFAULT_ADDRESS_FOR_RETURN = "г Москва, ул Советская, д 9 стр 3";
    public final class Products {

        public static final String PRODUCT_TEST_1 = "Продавец обуви";
        public static final String PRODUCT_TEST_2 = "Фломастеры";
        public static final String PRODUCT_NOT_AGREED = "NOT_AGREED_AUTOMATED";

    }

    public final class OrganizationInfoMerchant {
        public static final String KPP = "261544607";
        public static final String PAYMENT_ACCOUNT = "40817810099910004312";
        public static final String LEGAL_ADDRESS = "111677, г Москва, р-н Некрасовка, пр-кт Защитников Москвы";
        public static final String GENERAL_ADDRESS = "107078, г Москва, Красносельский р-н, пр-кт Академика Сахарова, д 10, помещ 24/13";
        public static final String FACT_ADDRESS = "111677, г Москва, р-н Некрасовка, пр-кт Защитников Москвы";
        public static final String BANK = "ПАО СБЕРБАНК";

    }

    public final class Phone {
        public static final String FOR_ORDER_1 = "+79999999921";
        public static final String FOR_ORDER_DEFAULT = "+79999999980";
        public static final String FOR_RETURN_DEFAULT = "+79999999970";
        public static final String FOR_RETURN_2 = "+79999999971";
        public static final String FOR_ORDER_2 = "+79999999981";
        public static final String FOR_PROFILE = "+79999999923";
        public static final String FOR_BLOCK = "+79999999959";
        public static final String SMS_CODE = "1234";

    }

    public final class PaymentMethods {
        public static final String ONLINE = "1";
        public static final String CASH = "2";
        public static final String BANK = "3";
    }
    public final class ReturnReasons {
        public static final String SIZE_NOT_FIT = "Не подошел размер";
        public static final String BOUGHT_WRONG  = "Купил не то";
    }
}
