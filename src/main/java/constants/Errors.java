package constants;


import lombok.AccessLevel;
import lombok.experimental.FieldDefaults;


@FieldDefaults(level = AccessLevel.PROTECTED)

public class Errors {

    public static final String INCORRECT_EMAIL = "Введите валидный e-mail!";
    public static final String EXIST_EMAIL = "Такой e-mail уже зарегистрирован!";
    public static final String SYMBOLS_IS_LESS_10 = "Должно быть не менее 10 символов!";
    public static final String SYMBOLS_BETWEEN_10_AND_12 = "Должно быть 10 или 12 символов!";
    public static final String SYMBOLS_IS_LESS_12 = "Должно быть не более 12 символов!";
    public static final String ONLY_DIGITS = "Только цифры!";
    public static final String SYMBOLS_MUST_BE_9 = "Должно быть 9 символов!";
    public static final String SYMBOLS_MUST_BE_20 = "Должно быть 20 символов!";
    public static final String SYMBOLS_MUST_BE_GREAT_20 = "Должно быть не менее 20 символов!";
    public static final String INCORRECT_FORMAT_PHONE = "Неверный формат телефона!";
    public static final String PASSWORD_DO_NOT_MATCH = "Введённые пароли не совпадают!";
    public static final String OBLIGATORY_FIELD = "Обязательное поле";
    public static final String INVALID_NUMBER = "Неверно введен номер";
    public static final String WRONG_CODE = "Неверный код";



}
