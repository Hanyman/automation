package constants.end_point;

public enum MerchantPanelEndpoint {

    login_merchant("/login"),
    logout_merchant("/logout"),
    registration("/registration"),
    registration_user_exist("/registration/user-exists"),
    merchant_stores("/merchant/stores"),
    delete_cart_all("/v1/cart/all"),
    create_product("/catalog/products/createProduct"),
    create("/catalog/products/create"),
    catalog_products("/catalog/products/"),
    upload("/upload"),
    merchant_managers("/merchant/managers"),
    merchant("/merchant"),
    merchant_docs_create("/merchant/documents/create"),
    shipment_list("/shipment/list"),
    product_check("/claims/product-check/page?page=1&filter[status][]=1"),
    returns("/returns"),
    returns_accept_items("/returns/accept-items")
    ;

    private final String path;

    MerchantPanelEndpoint(String path) {
        this.path = path;
    }

    public String path() {
        return path;
    }

    public String pathAndEndpoint(int additionalPath) {
        return path + additionalPath;
    }
    public String pathSaveOfferPrice(String idProduct) { return path + ""+idProduct+"/saveOfferPrice";    }
    public String pathSaveStocks(String idProduct) { return path + ""+idProduct+"/saveStocks"; }
    public String pathSaveProduct(String idProduct) { return path + ""+idProduct+"/saveProduct"; }
    public String pathSaveImage(String idProduct) { return path + ""+idProduct+"/saveImage"; }
    public String pathSaveClaim(String idProduct) { return path + ""+idProduct+"/saveClaim"; }
    public String pathUpload(String destination) { return path + "?destination="+ destination +""; }
    public String pathMerchantManagers(int id) { return path + "/"+ id +""; }
    public String pathMerchant(int id) { return path + "/"+ id +""; }
    public String apiChangeStatusOrder(int id) { return path + "/"+ id +"/changeStatus"; }
    public String apiGetBarcodes(int id) { return path + "/"+ id +"/barcodes"; }
    public String apiAddShipmentPackageOrder(int id) { return path + "/"+ id +"/shipment-packages/addShipmentPackage"; }
    public String apiAddShipmentPackageItems(int id, int idPackage) { return path + "/"+ id +"/shipment-packages/"+idPackage+"/items"; }
    public String changeReturnStatus(String idReturn) { return path + "/set-return-status/"+idReturn+""; }

   // public String pathRegistrationUserExist(String email) { return path + "?email="+ email; }
}
