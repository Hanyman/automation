package constants.end_point;

public enum AdminPanelEndpoint {

    login_admin("login"),
    marketing_discounts("marketing/discounts"),
    claims_product_check("claims/product-check/"),
    products_approval("products/approval"),
    merchant_list_registration("merchant/list/registration"),
    merchant_list_active("merchant/list/active"),
    merchant_list_page("/merchant/list/page"),
    merchant_detail("merchant/detail"),
    order("/orders"),
    merchant_confirm_email_link("api/merchant/confirm/email/link"),
    login("login"),
    /**ENDPOINTS FOR MC**/
    customers_activities("customers/activities"),
    // types
    public_events_types_save("public-events/types/save"),
    public_events_types_page("public-events/types/page"),
    public_events_types_delete("public-events/types/delete"),
    // speakers
    public_events_speakers_save("public-events/speakers/save"),
    public_events_speakers_delete("public-events/speakers/delete"),
    public_events_speakers_page("public-events/speakers/page"),
    // places
    public_events_places_save("public-events/places/save"),
    public_events_places_delete("public-events/places/delete"),
    public_events_places_page("public-events/places/page"),
    /**ENDPOINTS FOR RETURNS**/
    returns_config_reasons("returns/config/reasons"),
    returns("returns"),
    list_returns_config_reasons("returns/config/reasons/reasons"),
    returns_item_problem_reasons("/returns/item-problem-reasons"),
    list_returns_problem("/returns/item-problem-reasons/json"),
    /**ENDPOINTS FOR CUSTOMERS**/
    customers_filter("/customers/filter"),
    customers("/customers")

    ;

    private final String path;

    AdminPanelEndpoint(String path) {
        this.path = path;
    }

    public String path() {
        return path;
    }

    public String pathAndEndpoint(int additionalPath) {
        return path + additionalPath;
    }
    public String pathChangeStatus(String idProduct) { return path + ""+idProduct+"" + "/changeStatus"; }
    public String pathActivateMerchant(int id) { return path + "/"+ id +""; }
    public String apiOrderPay(int id) { return path + "/"+ id +"/pay"; }
    public String cardOrder(int id) { return path + "/"+ id +"/json"; }
    public String returnData(String id) { return path + "/"+ id +"/json"; }
    public String deliveriesOrder(int idOrder,int idDeliveries) { return path + "/"+idOrder+"/deliveries/"+idDeliveries+""; }
    public String changeReturnStatus(String idReturn, String status) { return path + "/"+idReturn+"/change-status/"+status+""; }
    public String changeReturnStatusAccept(String idReturn) { return path + "/"+idReturn+"/set-status"; }
    public String acceptItems(String idItem) { return path + "/accept-item/"+idItem; }
    public String customersId(String id) { return path + "/"+id; }
    public String filterMerchants(String merchantName) { return path + "?done=1&page=1&filter[id]=&filter[legal_name]="+merchantName+"&filter[operator_full_name]=&filter[operator_email]=&filter[operator_phone]=&filter[manager_id]="; }
    public String filterMerchants(String idMerchant,String merchantName,String status, String msg) { return path + "/"+idMerchant+"?merchant[organization_form]=2&merchant[legal_name]="+merchantName+"&merchant[status]="+status+"&merchant[city]=&merchant[rating_id]=&merchant[manager_id]=&merchant[cancel_reason]="+msg+""; }
}
