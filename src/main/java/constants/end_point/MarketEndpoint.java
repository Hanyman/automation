package constants.end_point;

public enum MarketEndpoint {

    v1_categories("v1/categories"),
    v1_auth_loginbyphone_sendsms("v1/auth/loginByPhone/sendSMS"),
    v1_auth_loginbyphone_checkcode("v1/auth/loginByPhone/checkCode"),
    v1_auth_is_login("v1/auth/is-login"),
    v1_lk_profile("v1/lk/profile"),
    v1_cart_item("v1/cart/item"),
    v1_checkout_data("v1/checkout/data"),
    v1_auth_logout("v1/auth/logout"),
    profile_orders("profile/orders/"),
    profile_personal("v1/lk/profile/personal"),
    receivers("v1/lk/receivers"),
    remove_preferences_brands("v1/lk/preferences/1/brands"),
    remove_preferences_category("v1/lk/preferences/1/categories"),
    v1_lk_preferences_catalog("v1/lk/preferences/catalog"),
    v1_lk_address("/v1/lk/address"),
    v1_lk_receivers("/v1/lk/receivers/"),
    v1_checkout_commit("v1/checkout/commit"),
    cart_products("cart/products"),
    v1_lk_order("/v1/lk/order"),
    v1_lk_returns_add_items("/v1/lk/returns/add-items"),
    v1_lk_returns_current_items("/v1/lk/returns/current/items"),
    v1_lk_returns_add_photo("/v1/lk/returns/add-photo"),
    v1_lk_returns_reasons("/v1/lk/returns/reasons"),
    v1_lk_returns("/v1/lk/returns"),
    v1_points_filtered("/v1/points/filtered"),
    v1_points("/v1/points/"),

    cart("cart");

    private final String path;

    MarketEndpoint(String path) {
        this.path = path;
    }

    public String path() {
        return path;
    }

    public String pathAndEndpoint(int additionalPath) {
        return path + "/" + additionalPath;
    }
    public String pathAddressDefault(int idAddress) { return path + "/"+idAddress+"/default"; }
    public String pathGetOrder(int numbOrder) { return path + "/" + numbOrder; }
    public String pathGetRetItem(int numbOrder) { return path + "/" + numbOrder; }
    public String pathGetReturnItem(String numbOrder) { return path + "/" + numbOrder; }
    public String pathAddReasonToReturn(int numbOrder,String numbItem) { return path + "/" + numbOrder + "/update-item/" + numbItem; }
    public String pathGetDeliveriesPoints(String data) { return path + data; }
    public String pathCalculatePoint(String returnId, String point) { return path + "/" + returnId + "/calculate-point/"+point+""; }
    public String pathUpdateDelivery(String returnId) { return path + "/" + returnId + "/update-delivery"; }
    public String pathSendReturn(String returnId) { return path + "/" + returnId + "/send"; }

}
