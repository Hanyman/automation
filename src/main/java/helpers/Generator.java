package helpers;

import com.github.javafaker.Faker;
import lombok.*;
import lombok.experimental.FieldDefaults;

import static helpers.Helper.getClearName;

@Getter
@FieldDefaults(level = AccessLevel.PUBLIC)
/** The class generates various data for tests**/
public class Generator {
    Faker faker = new Faker();

    String firstName = getClearName(faker.name().prefix()),
            lastName = getClearName(faker.name().firstName()),
            middleName = getClearName(faker.name().lastName()),
            company = faker.company().name(),
            inn = faker.number().digits(10),
            inn_9 = faker.number().digits(9),
            inn_11 = faker.number().digits(11),
            inn_13 = faker.number().digits(13),
            kpp = faker.number().digits(9),
            bank_account = faker.number().digits(20),
            bank_account_21 = faker.number().digits(21),
            number_15 = faker.number().digits(15),
            email = faker.internet().emailAddress(),
            longName = faker.lorem().characters(130,false,false),
            phoneNumb = "+7900" + faker.number().digits(7),
            code = faker.number().digits(12),
            article = faker.number().digits(5),
            nameProduct = faker.name().title(),
            password = faker.internet().password(),
            domainName = faker.internet().domainName(),
            combinationForBarcode = faker.number().digits(12);
}
