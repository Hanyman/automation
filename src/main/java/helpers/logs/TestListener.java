package helpers.logs;

import env.Base_Container;
import io.qameta.allure.Attachment;
import io.qameta.allure.selenide.LogType;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.logging.LogEntries;
import org.testng.ITestContext;
import org.testng.ITestResult;
import org.testng.TestListenerAdapter;

import java.util.Date;

public class TestListener extends TestListenerAdapter {
    private static String getTestMethodName(ITestResult iTestResult) {
        return iTestResult.getMethod().getConstructorOrMethod().getName();
    }

    //Text attachments for Allure
    @Attachment(value = "{1}", type = "image/png")
    public byte[] saveScreenshotPNG(WebDriver driver) {
        return ((TakesScreenshot) driver).getScreenshotAs(OutputType.BYTES);
    }

    //Text attachments for Allure
    @Attachment(value = "{1}", type = "text/plain")
    public static String saveTextLog(WebDriver driver) {
        LogEntries logEntries = driver.manage().logs().get(String.valueOf(LogType.BROWSER));

        StringBuilder logs = new StringBuilder();


        for (org.openqa.selenium.logging.LogEntry entry : logEntries) {
            logs.append(new Date(entry.getTimestamp()) + " "
                    + entry.getLevel() + " " + entry.getMessage());
            logs.append(System.lineSeparator());

        }
        return String.valueOf(logs);
    }

    @Override
    public void onStart(ITestContext iTestContext) {
        Log.info("I am in onStart method " + iTestContext.getName());
    }

    @Override
    public void onFinish(ITestContext iTestContext) {
        Log.info("I am in onFinish method " + iTestContext.getName());
    }

    @Override
    public void onTestStart(ITestResult iTestResult) {
        Log.info(getTestMethodName(iTestResult) + " test is starting.");
    }

    @Override
    public void onTestSuccess(ITestResult iTestResult) {
        Log.log("Success: " + getTestMethodName(iTestResult) + "\n");
    }

    @Override
    public void onTestFailure(ITestResult iTestResult) {
        Log.log("Failed: " + getTestMethodName(iTestResult) + "\n");

        //Get driver from BaseTest and assign to local webdriver variable.
        Object testClass = iTestResult.getInstance();
        WebDriver driver = ((Base_Container) testClass).getWebDriver();
        //Allure ScreenShotRobot and SaveTestLog
        if (driver != null) {
          saveScreenshotPNG(driver);
        }
        //Save a log on allure.
        saveTextLog(driver);

    }

    @Override
    public void onTestSkipped(ITestResult iTestResult) {
        Log.log("Skipped: " + getTestMethodName(iTestResult) + "\n");
    }

    @Override
    public void onTestFailedButWithinSuccessPercentage(ITestResult iTestResult) {
        Log.info("Test failed but it is in defined success ratio " + getTestMethodName(iTestResult));
    }
}
