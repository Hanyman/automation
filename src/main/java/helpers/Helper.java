package helpers;

import com.codeborne.selenide.SelenideElement;
import com.codeborne.selenide.WebDriverRunner;
import org.openqa.selenium.*;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.ITestResult;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.time.Duration;
import java.util.concurrent.TimeUnit;


/** Class for functions with elements missing in selenide. **/
public class Helper {
    private SelenideElement _current;

    public static void clearInputForReact(SelenideElement element) {
        int n =0;
        while (!element.getValue().equals("")) {
            element.sendKeys(Keys.BACK_SPACE);
            n++;
            if (n == 50) {
                break;
            }
        }


    }
    public static String getDigits(String input){
        String numberOrder = null;
        String[] parts = input.split("[^0-9]");
        for (int i = 0; i < parts.length; i++) {
            if (!parts[i].equals("")) {
                numberOrder = parts[i];
            }
        }
        return numberOrder;
    }

    public Helper current(SelenideElement element) {
        _current = element;
        return this;
    }
    public static String getClearName(String value)  {
        return value.replaceAll("[^a-zA-Z]", "");

    }

    public static String getPhoneForAuth(String fullPhoneNumb)  {
        return fullPhoneNumb.replaceAll("\\+7", "");

    }
    public static String deleteQuotes(String param)  {
        return param.replaceAll("\"", "");

    }
    public static void waitPageLoaded(WebDriver driver,int timeout){
        waitJsCondition("return document.readyState == 'complete'",driver,true, timeout);
        waitJquery(driver);

    }

    public static void waitJsCondition(String script, WebDriver _driver, boolean expectedResult, int timeout) {
        WebDriverWait wait = new WebDriverWait(_driver, Duration.ofSeconds(timeout));
        wait.until((ExpectedCondition<Boolean>) wdriver
                -> runScript(script, _driver).equals(expectedResult));
    }
    public static Object runScript(String script, WebDriver _driver) {
        //Helper.log("Run Script:" + script);
        try {
            Object result = ((JavascriptExecutor) _driver).executeScript(script);
            if (result != null) {
//				log("Run Script: " + script + " ==> " + result.toString());
            } else {
//				log("Run Script: " + script + " ==> undefined");
            }
            return result;
        }
        catch(org.openqa.selenium.JavascriptException ex){
            //warning("Script error: "+script +"\nError: "+ ex.getMessage());
            return null;
        }
    }
    public static void waitJquery(WebDriver _driver) {
        //log("wait jquery");
        try {
            WebDriverWait wait = new WebDriverWait(_driver, Duration.ofSeconds(15));
            wait.until((ExpectedCondition<Boolean>) wdriver -> ((JavascriptExecutor) _driver)
                    .executeScript("return((window.jQuery != null) && (jQuery.active === 0))")
                    .equals(true));
        } catch (WebDriverException exWebDriverException) {
            Assert.fail("Timeout driver. Not enough time to display the item.");
        }
        //log("wait jquery done");

    }
    public static String getTestName(ITestResult iTestResult){
        return iTestResult.getMethod().getConstructorOrMethod().getName();
    }

    public static String getResource(String name) {
        try {
            return new String(Files.readAllBytes(Paths.get(name)));
        } catch (IOException ex) {
        }
        return "";
    }

    public static void switchOnAnotherWindow() {
        WebDriver driver = WebDriverRunner.getWebDriver();
        String currentWindowHandle = driver.getWindowHandle();
        for (String windowHandle : driver.getWindowHandles()) {
            if (!windowHandle.equals(currentWindowHandle)) {
                // Переключаемся на новую вкладку
                driver.switchTo().window(windowHandle);
            }
        }
    }
}
