package helpers.api;
import com.jayway.jsonpath.JsonPath;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import org.openqa.selenium.Cookie;

import java.io.File;
import java.net.URL;
import java.net.URLDecoder;
import java.nio.charset.StandardCharsets;
import java.util.*;

import static com.codeborne.selenide.WebDriverRunner.getWebDriver;
import static io.qameta.allure.Allure.step;
import static io.restassured.RestAssured.given;

/** Class for working with api functions **/
public class ApiHelper {

    // function of obtaining a link to activate the merchant to the specified email
    public static String requestPost(String email){
        return given()
                .when()
                .queryParam("token","0572c387c1b342a117d6a19aff7408e6")
                .queryParam("email",email)
                .post().then()
                .assertThat().statusCode(200).extract()
                .asString();
    }

    public static Map<String, String> getTokensLogin(RequestSpecification request_spec,String tokenName){

        Map<String, String> cookies = new HashMap<>();
        step("Получение токена - " + tokenName, () -> {
            Response response = (Response) given()
                    .spec(request_spec)
                    .when()
                    .get().then()
                    .assertThat().statusCode(200).extract();
            String xtoken = response.getCookie("XSRF-TOKEN");
            String token_session = response.getCookie(tokenName);
            cookies.put("XSRF-TOKEN",xtoken);
            cookies.put(tokenName,token_session);
        });
        return cookies;

    }

    public static Map<String, String> getTokenMarket(RequestSpecification request_spec,String tokenName){
        Map<String, String> cookies = new HashMap<>();
        step("Получение токена - " + tokenName, () -> {
            Response response = (Response) given()
                    .spec(request_spec)
                    .when()
                    .get().then()
                    .assertThat().statusCode(200).extract();
            String token_session = response.getCookie(tokenName);
            cookies.put(tokenName,token_session);
        });
        return cookies;
    }

    public static String getToken(Map<String, String> cookies, String cookieName){
        if(cookieName.contains("XSRF-TOKEN")){
           return cookies.get(cookieName).replace("%3D","=");
        }else {
          return cookies.get(cookieName);
        }

    }

    // simple functions for work with JSON
    public static String getParamJson(String param)  {
        return param.replaceAll("\\[|\\]", ""); }

    public static String getClearUrl(String param)  {
        return param.replaceAll("\"", "");
    }

    public static String getClearString(String param)  {
        return param.replaceAll("\"", "");
    }

    public static String clearQuery(String param)  {
        return param.replaceAll("^\\[|\\]$", "");
    }
    //function to get parameters from url
    public static Map<String, List<String>> getParamUrl(URL url)  {

        final Map<String, List<String>> query_pairs = new LinkedHashMap<>();
        final String[] pairs = url.getQuery().split("&");
        for (String pair : pairs) {
            final int idx = pair.indexOf("=");
            final String key = idx > 0 ? URLDecoder.decode(pair.substring(0, idx), StandardCharsets.UTF_8) : pair;
            if (!query_pairs.containsKey(key)) {
                query_pairs.put(key, new LinkedList<>());
            }
            final String value = idx > 0 && pair.length() > idx + 1 ? URLDecoder.decode(pair.substring(idx + 1), StandardCharsets.UTF_8) : null;
            query_pairs.get(key).add(value);
        }
        return query_pairs;
    }

    // get cookies after auth user
    public static String getCookies(String nameCookie) {
        Cookie sessionCookie = getWebDriver().manage().getCookieNamed(nameCookie);
        return sessionCookie.getValue();
    }

    public static int GetIdMerchant(String str) {
        str = str.replaceAll("[^?0-9]+", "");
        return Integer.parseInt(str);
        }

    public static int getLengthJson(String resp, String type){

        return Integer.parseInt(getParamJson(JsonPath.read(resp, "$.."+type+".length()").toString()));
    }
    public static String getNamePreference(String resp, int currentIndexElement, String type) {
        String currentElement;
        currentElement = getParamJson(JsonPath.read(resp, "$.."+type+"["+currentIndexElement+"].name").toString());
        return currentElement;
    }

    public static String getAbsPath(String path) {
        File file = new File(path);
        return file.getAbsolutePath();
    }
}
