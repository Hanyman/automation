package helpers.api;

import env.WebDriverConfig;
import io.restassured.RestAssured;
import io.restassured.builder.RequestSpecBuilder;
import io.restassured.filter.log.LogDetail;
import io.restassured.http.ContentType;
import io.restassured.specification.RequestSpecification;
import org.aeonbits.owner.ConfigFactory;

import static io.restassured.config.RedirectConfig.redirectConfig;

public class Spec_api {
    static final WebDriverConfig config = ConfigFactory.create(WebDriverConfig.class, System.getProperties());
    static boolean LogView = Boolean.parseBoolean(config.getLogView());
    public static RequestSpecification default_spec(String baseUrl) {
        return onLogsAndGetReqSpecBuilder()
                .setBaseUri(baseUrl)
                .setContentType(ContentType.JSON)
                .build();
    }

    public static RequestSpecification spec_client_with_token(String baseUrl, String token_session, String token_name) {
        return onLogsAndGetReqSpecBuilder()
                .setBaseUri(baseUrl)
                .addCookie(""+token_name+"="+token_session+";")
                .setContentType(ContentType.JSON)
                .build();
    }

    public static RequestSpecification spec_client_with_token_market(String baseUrl, String token_session, String token_name) {
        return onLogsAndGetReqSpecBuilder()
                .setBaseUri(baseUrl)
                .addCookie(""+token_name+"="+token_session+";")
                .setAccept("application/json, text/plain, */*")
                .setContentType(ContentType.JSON)
                .build();
    }

    public static RequestSpecification spec_with_token(String baseUrl, String x_xsrf_token, String token_name, String token_session) {
        return onLogsAndGetReqSpecBuilder()
                .setBaseUri(baseUrl)
                .addHeader("x-xsrf-token",x_xsrf_token)
                .addCookie("XSRF-TOKEN=" + x_xsrf_token +";"+""+token_name+"="+token_session+";")
                //.setConfig(RestAssured.config().redirect(redirectConfig().followRedirects(false)))
                .setContentType(ContentType.JSON)
                .build();
    }
    public static RequestSpecification spec_with_token_withoutHeader(String baseUrl, String x_xsrf_token,String token_name, String token_session) {
        return onLogsAndGetReqSpecBuilder()
                .setBaseUri(baseUrl)
                .addCookie("XSRF-TOKEN=" + x_xsrf_token +";"+""+token_name+"="+token_session+";")
                .setContentType(ContentType.JSON)
                .build();
    }

    public static RequestSpecification spec_upload_file(String baseUrl, String x_xsrf_token,String token_name, String token_session) {
        return onLogsAndGetReqSpecBuilder()
                .setBaseUri(baseUrl)
                .addHeader("x-xsrf-token",x_xsrf_token)
                .addCookie("XSRF-TOKEN=" + x_xsrf_token +";"+""+token_name+"="+token_session+";")
                .setContentType("multipart/form-data")
                .build();
    }

    private static RequestSpecBuilder onLogsAndGetReqSpecBuilder(){
        RequestSpecBuilder reqBuilder = new RequestSpecBuilder();
        if (LogView) {
            reqBuilder.log(LogDetail.ALL)
            .addFilter(LogFilter.filters().withCustomTemplates());
        }
        return reqBuilder;
    }

    public static RequestSpecification spec_upload_file_market(String baseUrl, String token_session, String token_name) {
        return onLogsAndGetReqSpecBuilder()
                .setBaseUri(baseUrl)
                .addCookie(""+token_name+"="+token_session+";")
                .setContentType("multipart/form-data")
                .build();
    }

}
