package helpers.api;

import env.WebDriverConfig;
import io.restassured.response.Response;
import org.aeonbits.owner.ConfigFactory;

public class Steps {
    static final WebDriverConfig config = ConfigFactory.create(WebDriverConfig.class, System.getProperties());
    static boolean LogView = Boolean.parseBoolean(config.getLogView());
    public Response checkResponseWithStatusCodeAndLogView(Response response, int statusCode) {
        if (LogView) {
            response.then().log().body()
                    .statusCode(statusCode);
        }else{
            response.then()
                    .statusCode(statusCode);
        }

        return response;
    }
}
