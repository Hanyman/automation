package configs;

import org.aeonbits.owner.Config;

import static org.aeonbits.owner.Config.LoadType.MERGE;

@Config.LoadPolicy(MERGE)
@Config.Sources({
        "system:properties",
        "classpath:configs/platforms/market_data.properties"
})
/** data url and data users(login and password)
 path configs/platforms/market_data.properties **/
public interface MarketConfig extends Config {

}
