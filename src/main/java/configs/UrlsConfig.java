package configs;

import org.aeonbits.owner.Config;

import static org.aeonbits.owner.Config.LoadType.MERGE;

@Config.LoadPolicy(MERGE)
@Config.Sources({
        "system:properties",
        "classpath:configs/platforms/urls.properties"
})
/** data url and data users(login and password)
 path configs/platforms/urls.properties **/
public interface UrlsConfig extends Config {

    @Key("market.url")
    String getMarketUrl();

    @Key("merchant.url")
    String getMerchantUrl();

    @Key("admin.url")
    String getAdminUrl();

}
