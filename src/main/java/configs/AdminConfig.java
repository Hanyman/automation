package configs;

import org.aeonbits.owner.Config;

import static org.aeonbits.owner.Config.LoadType.MERGE;

@Config.LoadPolicy(MERGE)
@Config.Sources({
        "system:properties",
        "classpath:configs/platforms/admin_data.properties"
})
/** data url and data users(login and password)
 path configs/platforms/admin_data.properties **/
public interface AdminConfig extends Config {

    @Key("admin.login")
    String getAdminLogin();

    @Key("admin.password")
    String getAdminPassword();

}
