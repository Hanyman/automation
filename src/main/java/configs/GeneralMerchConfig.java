package configs;

import org.aeonbits.owner.Config;

import static org.aeonbits.owner.Config.LoadType.MERGE;

@Config.LoadPolicy(MERGE)
@Config.Sources({
        "system:properties",
        "classpath:configs/platforms/general_merch_data.properties"
})
/** data url and data users(login and password)
 path configs/platforms/general_merch_data.properties **/
public interface GeneralMerchConfig extends Config {

    @Key("merchant.login")
    String getMerchantLogin();

    @Key("merchant.password")
    String getMerchantPassword();
    @Key("merchant.product.one")
    int getProductFirst();
    @Key("merchant.store")
    int getStore();


}
