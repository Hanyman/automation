package steps.UI.market;

import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.SelenideElement;
import env.Base_Container;
import io.qameta.allure.Step;
import org.openqa.selenium.Keys;
import org.openqa.selenium.interactions.Actions;
import org.testng.Assert;
import steps.UI.OpenPageSteps;
import testPageLocator.market.ReturnsPageLocators;
import testPageLocator.market.mainPage.MainLocators;
import testPageLocator.market.productsPage.ProductsLocators;
import testPageLocator.market.searchAndFilters.SearchFilterLocators;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import static com.codeborne.selenide.Condition.*;


public class ReturnsSteps extends Base_Container {
    ReturnsPageLocators returnsPage;


    @Step("Нажимаем на кнопку Вернуть товары на заказе")
    public ReturnsSteps clickReturnOrderOnCard(String numbOrder) {
        returnsPage.ReturnOrderBtn(numbOrder).click();
        return this;
    }
    @Step("Выбираем чекбоксом продукт для возврата")
    public ReturnsSteps chooseProductToReturn(String numbProduct) {
        returnsPage.OrderReturnSection.shouldBe(visible);
        Actions action = new Actions(getWebDriver());
        action.moveToElement(returnsPage.ReturnCheck(numbProduct)).click().build().perform();
        return this;
    }
    @Step("Нажимаем на кнопку Оформить возврат на этапе выбора товара")
    public ReturnsSteps clickOrderReturnBtn() {
        returnsPage.OrderReturnButton.click();
        return this;
    }

    @Step("Выбираем причину возврата")
    public ReturnsSteps clickOnReasonReturn(String reason) {
        returnsPage.ReasonsDropdown.click();
        returnsPage.ReasonsDropdownItem(reason).click();
        return this;
    }

    @Step("Описываем проблему возврата")
    public ReturnsSteps textProblemDescription(String problem) {
        returnsPage.ProblemDescriptionTextarea.setValue(problem);
        return this;
    }
    @Step("Загрузка фото для возврата")
    public ReturnsSteps uploadPhoto() throws InterruptedException {
        returnsPage.inputFile.uploadFile(new File("src/test/resources/files/img/1.jpg"));
        returnsPage.dropzoneProgress.shouldBe(visible);
        returnsPage.dropzoneProgress.shouldBe(disappear);
        return this;
    }

    @Step("Нажимаем кнопку Оформить возврат на странице выбора причин, проблемы и загрузки фото")
    public ReturnsSteps nextReturnBtn() throws InterruptedException {
        returnsPage.UpdateReturnBtn.scrollTo().click();
        return this;
    }
    @Step("Нажимаем кнопку Продолжить Оформить возврат на странице выбора адреса")
    public ReturnsSteps createReturnBtn() throws InterruptedException {
        returnsPage.CreateReturnBtn.scrollTo().click();
        return this;
    }
    @Step("Нажимаем кнопку  Оформить возврат на странице завершения")
    public ReturnsSteps finishReturnBtn() throws InterruptedException {
        returnsPage.ConfirmFinishReturnBtn.scrollTo().click();
        return this;
    }
    @Step("Нажимаем на кнопку Курьером в блоке выбор способа доставки")
    public ReturnsSteps clickExpressDelivery() throws InterruptedException {
        returnsPage.CreateReturnForm.shouldBe(visible);
        returnsPage.ExpressDeliveryBtn.click();
        return this;
    }
    @Step("Блок с таймслотами после выбора адреса должен отображаться")
    public ReturnsSteps checkTimeslotsBlock() throws InterruptedException {
        returnsPage.ReturnTimeslotsBlock.shouldBe(visible);
        return this;
    }

    @Step("Вставляем адрес в поле и выбираем адрес в выпадающем списке")
    public ReturnsSteps fillAddress(String address) {
        returnsPage.AddressInput.setValue(address);
        returnsPage.AddressSuggestionItem.click();
        return this;
    }
    @Step("Нажимаем на кнопку изменить состав возврата")
    public ReturnsSteps clickReturnLink() {
        returnsPage.ReturnLinkEdit.click();
        return this;
    }

    @Step("Проверяем статус возврата")
    public ReturnsSteps checkStatusReturn(String status) {
        Assert.assertEquals(returnsPage.AlertText.getText().trim(),status,"Статус возврата должен отображаться корректно.");
        return this;
    }

    @Step("Проверяем статус возврата")
    public ReturnsSteps checkReasonMsg(String status) {
        Assert.assertEquals(returnsPage.ReasonErrorMsg.getText().trim(),status,"Ошибка о необходимости выбора причины возврата.");
        return this;
    }

}
