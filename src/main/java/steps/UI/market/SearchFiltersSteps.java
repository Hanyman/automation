package steps.UI.market;

import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.SelenideElement;
import env.Base_Container;
import io.qameta.allure.Step;
import org.openqa.selenium.Keys;
import steps.UI.OpenPageSteps;
import testPageLocator.market.mainPage.MainLocators;
import testPageLocator.market.productsPage.ProductsLocators;
import testPageLocator.market.searchAndFilters.SearchFilterLocators;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import static com.codeborne.selenide.Condition.*;
import static io.qameta.allure.Allure.step;


public class SearchFiltersSteps extends Base_Container {
    SearchFilterLocators searchFiltersPage;
    ProductsLocators productsLocators;
    MainLocators mainPage;
    OpenPageSteps openPageSteps = new OpenPageSteps();

    // Common Steps
    // Search
    public String stepGetProduct(String pathToUrl) throws InterruptedException {
        openPageSteps.stepOpenPageMarket(pathToUrl);
        productsLocators.LoaderProductsList.shouldBe(visible);
        productsLocators.LoaderProductsList.shouldBe(disappear);
        productsLocators.GridProductsList.shouldBe(visible);
        String nameProduct = getTextElements(productsLocators.NamesProducts);
        return nameProduct;

    }

    @Step("Вводим в поле название товара")
    public SearchFiltersSteps inputNameFieldInSearch(String nameProduct) {
        mainPage.SearchField.setValue(nameProduct);
        mainPage.SearchField.sendKeys(Keys.ENTER);
        return this;

    }

    @Step("Ожидаем пустые результаты поиска")
    public SearchFiltersSteps checkEmptyProduct() {
        searchFiltersPage.EmptyProduct.shouldBe(appear);
        return this;

    }
    @Step("Вводим в поле название товара")
    public SearchFiltersSteps inputProductNameInSearchField(String nameProduct) {
        mainPage.SearchField.setValue(nameProduct);
        return this;

    }
    @Step("Нажимаем на пункт меню Каталог")
    public SearchFiltersSteps clickProductMenu() {
        mainPage.ProductsMenu.hover().click();
        return this;

    }
    @Step("Нажимаем на категорию Книги")
    public SearchFiltersSteps clickSwitCategory() {
        searchFiltersPage.CategoriesField.click();
        return this;

    }

    @Step("Ожидаем карточку товара в поиске")
    public SearchFiltersSteps waitAppearCardProduct(String productName) {
        searchFiltersPage.productName(productName).shouldBe(appear);
        return this;

    }

    @Step("Не ожидаем карточку товара в поиске")
    public SearchFiltersSteps waitDisappearCardProduct(String productName) {
        searchFiltersPage.productName(productName).shouldBe(disappear);
        return this;

    }

    public static String getTextElements(ElementsCollection listElement){
        String nameProducts;
        List<String> data = new ArrayList<>();
        Random rand = new Random();

        for(SelenideElement element : listElement){
            String product = element.getText();
            if(product.isEmpty()==false) {
                data.add(product);
            }
        }
        nameProducts = data.get(rand.nextInt(data.size())).trim();
        return nameProducts;
    }
}
