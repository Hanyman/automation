package steps.UI.market;

import env.Base_Container;
import io.qameta.allure.Step;
import org.testng.Assert;
import steps.UI.OpenPageSteps;
import testPageLocator.market.orderPage.CheckoutOrderLocators;

import static com.codeborne.selenide.Condition.disappear;
import static com.codeborne.selenide.Condition.visible;
import static com.codeborne.selenide.Selenide.open;
import static constants.end_point.MarketEndpoint.*;
import static io.qameta.allure.Allure.step;

public class CancelOrderSteps extends Base_Container {
    CheckoutOrderLocators orderPage;
    OpenPageSteps openPageSteps = new OpenPageSteps();

    @Step("Переходим в карточку заказа и нажимаем отмену")
    public CancelOrderSteps clickCancelOrder(String numbOrder) {
        orderPage.cardOrder(numbOrder).click();
        orderPage.CancelOrderBtn.click();
        orderPage.SendBtn.click();
        orderPage.LoaderSpinner.shouldBe(disappear);
        return this;
    }

    @Step("Проверяем статус заказа")
    public CancelOrderSteps checkStatusOrderCancel(String status) {
        Assert.assertEquals(orderPage.OrderStatus.getText(),status,"Статус заказа должен отображаться корректно.");

        return this;
    }


}
