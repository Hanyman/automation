package steps.UI.market;

import env.Base_Container;
import io.qameta.allure.Step;
import org.openqa.selenium.interactions.Actions;
import org.testng.Assert;
import steps.UI.market.PersonnelData.ContactsSteps;
import testPageLocator.market.mainPage.MainLocators;
import testPageLocator.market.orderPage.CheckoutOrderLocators;

import static com.codeborne.selenide.Condition.*;

public class CheckoutSteps extends Base_Container {
    CheckoutOrderLocators checkoutOrderPage;
    MainLocators mainLocators;


    @Step("Выбираем способ получения доставки - Курьерская доставка")
    public CheckoutSteps clickExpressDeliveryBlock() {
        checkoutOrderPage.ExpressDelivery.click();
        checkoutOrderPage.CheckoutBodyPending.shouldBe(disappear);
        return this;
    }
    @Step("Выбираем способ получения доставки - Самовывоз и пункт вывода")
    public CheckoutSteps choosePickupPoint() {
        checkoutOrderPage.Pickup.click();
        checkoutOrderPage.CheckoutBodyPending.shouldBe(disappear);
        checkoutOrderPage.PickupBlock.shouldBe(appear);
        checkoutOrderPage.PickupSelectBtn.click();
        checkoutOrderPage.PickupModal.shouldBe(appear);
        checkoutOrderPage.PickupPointItemFirst.click();
        String PickupAddress = checkoutOrderPage.PickupModalAddress.getText();
        checkoutOrderPage.PickupModalSelectBtn.click();
        checkoutOrderPage.PickupModal.shouldBe(disappear);
        checkPickupAddress(PickupAddress);
        return this;
    }

    @Step("Нажимаем на кнопку Оформить заказ в корзине")
    public CheckoutSteps clickCheckoutCartBtn() {
        checkoutOrderPage.CheckoutCartBtn.hover().click();
        return this;
    }
    @Step("Выбираем получателя")
    public CheckoutSteps clickOnRecipient(String name) {
        checkoutOrderPage.PersonalInfoRecipient(name).hover().click();
        checkSelectedRecipient(name);
        return this;

    }
    @Step("Нажимаем на кнопку Добавить нового получателя")
    public CheckoutSteps clickAddNewRecipient() {
        checkoutOrderPage.AddRecipientCheckout.click();
        return this;
    }

    @Step("Нажимаем на кнопку Добавить новый адрес")
    public CheckoutSteps clickAddNewAddress() {
        checkoutOrderPage.AddAddressCheckout.click();
        return this;
    }

    @Step("Нажимаем на кнопку Изменить у существующего получателя")
    public CheckoutSteps clickEditRecipient() {
        checkoutOrderPage.EditRecipientCheckout.click();
        return this;

    }
    @Step("Проверяем строку с данными нового получателя")
    public CheckoutSteps checkRecipientRow(String fio, String phone, String email){
        Assert.assertEquals( checkoutOrderPage.RecipientName.getText().trim(),fio,"Фио получателя должно отображаться.");
        Assert.assertEquals( checkoutOrderPage.RecipientPhone.getText().trim(),phone,"Phone получателя должен отображаться.");
        Assert.assertEquals( checkoutOrderPage.RecipientEmail.getText().trim(),email,"Email получателя должен отображаться.");
        return this;
    }
    @Step("Проверяем строку с данными нового адреса")
    public CheckoutSteps checkAddressRow(String address){
        Assert.assertTrue( checkoutOrderPage.DeliveryAddress.getText().contains(address),"Фио получателя должно отображаться.");
       return this;
    }

    @Step("Выбираем чекбокс подтверждения условий")
    public CheckoutSteps clickOnConfirmationCheckBox() {
        checkoutOrderPage.CheckoutConfirmation.scrollTo().hover();
        Actions action = new Actions(getWebDriver());
        action.moveToElement(checkoutOrderPage.CheckoutConfirmation).click().build().perform();
        return this;

    }
    @Step("Нажимаем на кнопку Оформить заказ в карточке заказа")
    public CheckoutSteps clickCheckoutBtn() {
        checkoutOrderPage.CheckoutBtn.click();
        return this;

    }

    @Step("Выбираем необходимый способ оплаты")
    public CheckoutSteps clickOnCheckoutPayment(String paymentName) {
        checkoutOrderPage.checkoutPayment(paymentName).click();
        return this;

    }

    @Step("Ожидаем пока пройдет загрзка данных для оформления товара, переход из корзины")
    public CheckoutSteps waitDisappearLoader() {
        //checkoutOrderPage.LoaderCheckoutInCart.shouldBe(visible);
        checkoutOrderPage.LoaderCheckoutInCart.shouldBe(disappear);
        return this;

    }
    @Step("Ожидаем пока завершится оформление заказа")
    public CheckoutSteps waitDisappearLoaderSpinnerEndCheckout() {
        checkoutOrderPage.LoaderSpinnerEndCheckout.shouldBe(visible);
        checkoutOrderPage.LoaderSpinnerEndCheckout.shouldBe(disappear);
        return this;

    }
    @Step("Возвращаемся назад на странице онлайн оплаты")
    public CheckoutSteps clickExitPaymentProcess() {
        checkoutOrderPage.ExitPaymentProcessBtn.click();
        return this;

    }
    @Step("Нажимаем на посмтреть заказ и проверяем информацию")
    public CheckoutSteps checkLastOrder(String text) {
        checkoutOrderPage.ViewOrderBtn.click();
        mainLocators.pageLoadingBar.shouldBe(visible);
        mainLocators.pageLoadingBar.shouldBe(disappear);
        String data = checkoutOrderPage.OrderDeliveryDate.getText();
        Assert.assertTrue(data.contains(text),text + " должна отображаться в заказе.");
        return this;

    }

    @Step("Проверяем что получатель выбран")
    public CheckoutSteps checkSelectedRecipient(String name) {
        Assert.assertTrue(checkoutOrderPage.PersonalInfoRecipientIsSelected(name).exists(),"Получатель " + name + "должен быть выбран.");
        return this;
    }
    @Step("Проверяем что адрес самовывоза корректно отображается")
    public CheckoutSteps checkPickupAddress(String expectedAddress) {
        Assert.assertEquals(checkoutOrderPage.PickupModalAddress.getText(),expectedAddress,"Адрес должен отображаться корректно.");
        return this;
    }

    @Step("Проверяем что отображается сообщение в блоке получателей")
    public CheckoutSteps checkMsgInBlockRecipient() {
        Assert.assertEquals(checkoutOrderPage.RecipientMsg.getText().trim(),"Необходимо выбрать или добавить получателя","Сообщение в блоке получатли должно отображаться.");
        return this;
    }
    @Step("Проверяем что отображается сообщение в блоке получателей")
    public CheckoutSteps checkMsgInBlockAddress() {
        Assert.assertEquals(checkoutOrderPage.AddressMsg.getText().trim(),"Необходимо выбрать или добавить новый адрес","Сообщение в блоке Адреса должно отображаться.");
        return this;
    }
    @Step("Проверяем что отображается сообщение в блоке подтверждения")
    public CheckoutSteps checkMsgInBlockCheckoutConfirmation() {
        Assert.assertEquals(checkoutOrderPage.CheckoutConfirmationMsg.getText().trim(),"Подтвердите согласие с условиями заказа и доставки","Сообщение в блоке Подтверждения должно отображаться.");
        return this;
    }



}

