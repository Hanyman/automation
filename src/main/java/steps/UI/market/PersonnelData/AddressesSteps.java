package steps.UI.market.PersonnelData;

import com.codeborne.selenide.Condition;
import env.Base_Container;
import io.qameta.allure.Step;
import org.testng.Assert;
import testPageLocator.market.mainPage.MainLocators;
import testPageLocator.market.profilePage.DeliveryAddressesLocators;
import testPageLocator.market.profilePage.MessagesLocators;

import static com.codeborne.selenide.Condition.*;
import static helpers.Helper.clearInputForReact;
import static helpers.Helper.waitPageLoaded;
import static org.testng.AssertJUnit.assertEquals;

public class AddressesSteps extends Base_Container {
    DeliveryAddressesLocators addressPage;
    MainLocators mainLocators;

    @Step("Нажимаем на кнопку редактирования адреса")
    public AddressesSteps clickOnEditAddressBtn(String address){
        addressPage.AddressCardEditBtn(address).click();
        return this;
    }

    @Step("Нажимаем на карточку адреса")
    public AddressesSteps clickAddressCard(String address){
        addressPage.AddressCard(address).shouldBe(appear);
        addressPage.AddressCard(address).hover().click();
        waitPageLoaded(getWebDriver(),globalTimeout);
        addressPage.AddressCardSelected(address).shouldBe(appear);
        return this;
    }

    @Step("Нажимаем на кнопку добавления адреса")
    public AddressesSteps clickAddAddressBtn(){
        addressPage.AddAddressBtn.click();
        return this;
    }
    @Step("Нажимаем на кнопку привезти сюда")
    public AddressesSteps clickBringHereBtn() throws InterruptedException {
        addressPage.AddNewAddress.click();
        addressPage.ModalAddressMap.shouldBe(disappear);
        mainLocators.pageLoadingBar.shouldBe(disappear);
        Thread.sleep(1000);
        waitPageLoaded(getWebDriver(),globalTimeout);
        return this;
    }
    @Step("Нажимаем на кнопку удаления адреса")
    public AddressesSteps clickOnRemoveAddressBtn(String address){
        addressPage.AddressCardDeleteBtn(address).hover().click();
        addressPage.AddressCardDeleteBtn(address).shouldBe(disappear);
        return this;
    }
    @Step("Заполняем поля нового адреса данными")
    public AddressesSteps setNewAddress(String address) throws InterruptedException {
        addressPage.ModalAddressMap.shouldBe(appear);
        addressPage.AddressField.clear();
        addressPage.AddressField.setValue(address);
        Thread.sleep(1000);
        addressPage.suggestionsAddress().hover().click();
        waitPageLoaded(getWebDriver(),globalTimeout);
        Thread.sleep(1000);

        return this;
    }
    @Step("Заполняем поля нового адреса данными")
    public AddressesSteps editAddress(String index,String street, String home, String construction){
        //addressPage.IndexField.setValue(index);
        addressPage.ModalAddressMap.shouldBe(appear);
        clearInputForReact(addressPage.StreetField);
        addressPage.StreetField.setValue(street);
       // addressPage.suggestionsAddress(street).click();
        addressPage.HomeField.setValue(home);
       // addressPage.suggestionsAddress(home).click();
        addressPage.ConstructionField.setValue(construction);
        //addressPage.suggestionsAddress(construction).click();

        return this;
    }

    @Step("Проверяем наличие карточки с адерсом")
    public AddressesSteps checkExistAddressCard(String address){
        Assert.assertTrue( addressPage.AddressCard(address).isDisplayed(),"Краточка с адресом - " + address + " - должна отображаться.");
        return this;
    }
    @Step("Проверяем наличие дефолтного выбора карточки с адерсом и галочкой")
    public AddressesSteps checkDefaultAddressCard(String address){
        Assert.assertTrue( addressPage.AddressCardSelected(address).isDisplayed(),"Краточка с адресом - " + address + " - должна отображаться выделенной и иметь галочку.");
        return this;
    }

    @Step("Проверяем отсутствие карточки с адерсом")
    public AddressesSteps checkNoExistAddressCard(String address){
        //addressPage.AddressCard(address).shouldBe(disappear);
        Assert.assertFalse( addressPage.AddressCard(address).isDisplayed(),"Краточка с адресом - " + address + " - не должна отображаться.");
        return this;
    }
    @Step("Проверяем адрес карточки")
    public AddressesSteps checkAddressCard(String address){
        Assert.assertEquals( addressPage.AddressCardTitleLast.getText(),address,"Измененный адрес должен отображаться.");
        return this;
    }
}
