package steps.UI.market.PersonnelData;

import env.Base_Container;
import io.qameta.allure.Step;
import org.testng.Assert;
import testPageLocator.market.mainPage.MainLocators;
import testPageLocator.market.profilePage.PreferencesLocators;

import static com.codeborne.selenide.Condition.*;
import static org.testng.AssertJUnit.assertEquals;

public class PreferencesSteps extends Base_Container {
    PreferencesLocators preferencesPage;
    MainLocators mainLocators;

    /**STEPS FOR BRANDS**/
    @Step("Нажимаем на кнопку добавить по середине блока Брендов")
    public PreferencesSteps clickAddBtnMiddle(){
        preferencesPage.AddBrandsMiddleBtn.hover().click();
        //checkExistPreferenceModal();
        return this;
    }

    @Step("Нажимаем на кнопку добавить правее блока Брендов")
    public PreferencesSteps clickAddBtnRight(){
        preferencesPage.AddBrandsRightBtn.hover().click();
        //checkExistPreferenceModal();
        return this;
    }
    @Step("Скролим и нажимаем на кнопку добавить правее блока Брендов")
    public PreferencesSteps clickAddBtnRightAndScroll(){
        preferencesPage.AddBrandsRightBtn.scrollTo().hover().click();
        //checkExistPreferenceModal();
        return this;
    }
    @Step("Вставляем значение в поле поиска")
    public PreferencesSteps SetValueInSearchField(String value){
        preferencesPage.SearchBrandsField.setValue(value);
        return this;
    }

    @Step("Проверка отсутствия модального окна предпочтений")
    public PreferencesSteps checkAbsencePreferenceModal(){
        Assert.assertEquals(preferencesPage.PreferencesBrandModal.getAttribute("style"),"display: none;","Preference modal should not be displayed.");
        return this;
    }

    @Step("Проверка отображения модального окна предпочтений")
    public PreferencesSteps checkExistPreferenceModal(){
        Assert.assertEquals(preferencesPage.PreferencesBrandModal.getAttribute("style"),"","Preference modal should be displayed.");
        return this;
    }

    @Step("Выбираем Бренда из списка брендов и добавляем")
    public PreferencesSteps chooseBrandFromListAndSave(String brandName){
        preferencesPage.PreferencesAddModal.hover();
        preferencesPage.BrandCheckbox(brandName).hover().click();;
        ClickAddBtnInListBrands();
        return this;
    }

    @Step("Выбираем Бренд из списка брендов")
    public PreferencesSteps chooseBrandFromList(String brandName){
        preferencesPage.BrandCheckbox(brandName).hover().click();;
        return this;
    }
    @Step("Нажимаем на кнопку Добавить бренд после выбора из списка")
    public PreferencesSteps ClickAddBtnInListBrands(){
        preferencesPage.AddBrandsBtnModal.hover().click();
        CheckNoExistLoadingBar();
        checkAbsencePreferenceModal();
        return this;
    }

    @Step("Выбираем Бренда из списка брендов и закрываем модальное окно")
    public PreferencesSteps chooseBrandFromListAndClose(String brandName){
        preferencesPage.BrandCheckbox(brandName).scrollTo().click();;
        preferencesPage.CloseModalBrandsBtn.hover().click();
        checkAbsencePreferenceModal();
        return this;
    }
    @Step("Проверяем добавление бренда в блок предпочтений по брендам.")
    public PreferencesSteps CheckExistBrandInBlock(String brandName){
        CheckNoExistLoadingBarVisibleDisappear();
        Assert.assertEquals( preferencesPage.PreferenceItem().getText(),brandName ,brandName +" ,should be displayed in the block.");
        return this;
    }
    @Step("Проверяем отсутствие бренда в блок предпочтений по брендам.")
    public PreferencesSteps CheckNotExistBrandInBlock(String brandName){
        CheckNoExistLoadingBarVisibleDisappear();
        Assert.assertFalse( preferencesPage.PreferenceItem(brandName).exists(),"Brand - " + brandName + " should not be displayed in the block.");
        return this;
    }

    @Step("Проверяем отсутствие бренда после добавления в списке брендов.")
    public PreferencesSteps CheckBrandIsNotList(String brandName){
        CheckNoExistLoadingBarVisibleDisappear();
        Assert.assertFalse( preferencesPage.BrandCheckbox(brandName).exists(),"Brand - " + brandName + " should not be displayed in the lists.");
        return this;
    }
    @Step("Проверяем присутствие бренда в списке брендов.")
    public PreferencesSteps CheckBrandIsList(String brandName){
        CheckNoExistLoadingBarVisibleDisappear();
        Assert.assertTrue( preferencesPage.BrandCheckbox(brandName).exists(),"Brand - " + brandName + " should be displayed in the lists.");
        return this;
    }

    @Step("Проверяем отсутствие бара загрузки.")
    public PreferencesSteps CheckNoExistLoadingBar(){
       // mainLocators.pageLoadingBar.shouldBe(visible);
        mainLocators.pageLoadingBar.shouldBe(disappear);
        return this;
    }
    @Step("Проверяем отсутствие бара загрузки( сперва он видим, а потом нет).")
    public PreferencesSteps CheckNoExistLoadingBarVisibleDisappear(){
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
        mainLocators.pageLoadingBar.shouldBe(disappear);
        return this;
    }

    @Step("Нажимаем на иконку удаления рядом с брендом")
    public PreferencesSteps clickRemoveBrandBtn(String brand){
        preferencesPage.PreferenceItemRemoveBtn(brand).hover().click();
        CheckNoExistLoadingBar();
        return this;
    }
    @Step("Нажимаем на иконку удалить все бренды")
    public PreferencesSteps clickRemoveAllBrandBtn(){
        preferencesPage.DeleteAllBrandsBtn.hover().click();
        CheckNoExistLoadingBar();
        return this;
    }

    /**STEPS FOR CATEGORIES**/
    @Step("Нажимаем на кнопку добавить по середине блока Категорий")
    public PreferencesSteps clickAddCategoriesBtnMiddle(){
        preferencesPage.AddCategoriesMiddleBtn.hover().click();
        //checkExistPreferenceModal();
        return this;
    }

    @Step("Нажимаем на кнопку добавить правее блока Категорий")
    public PreferencesSteps clickAddCategoriesBtnRight(){
        preferencesPage.AddCategoriesRightBtn.scrollTo().hover().click();
        return this;
    }
    @Step("Скроллим и Нажимаем на кнопку добавить правее блока Категорий")
    public PreferencesSteps clickAddCategoriesBtnRightAndScroll(){
        preferencesPage.AddCategoriesRightBtn.scrollTo().hover().click();
        return this;
    }
    @Step("Вставляем значение в поле поиска")
    public PreferencesSteps SetValueInSearchFieldCategories(String value){
        preferencesPage.SearchCategoriesField.setValue(value);
        return this;
    }

    @Step("Проверка отсутствия модального окна предпочтений категорий")
    public PreferencesSteps checkAbsencePreferenceCategoriesModal(){
        Assert.assertEquals(preferencesPage.PreferencesCategoriesModal.getAttribute("style"),"display: none;","Preference modal should not be displayed.");
        return this;
    }

    @Step("Проверка отображения модального окна предпочтений")
    public PreferencesSteps checkExistPreferenceCategoriesModal(){
        Assert.assertEquals(preferencesPage.PreferencesCategoriesModal.getAttribute("style"),"","Preference modal should be displayed.");
        return this;
    }

    @Step("Выбираем категорию из списка категорий и добавляем")
    public PreferencesSteps chooseCategoryFromListAndSave(String categoryName){
        preferencesPage.PreferencesAddModalCategories.shouldBe(visible);
        preferencesPage.PreferencesAddModalCategories.hover();;
        preferencesPage.CategoryCheckbox(categoryName).scrollTo().click();
        ClickAddBtnInListCategories();
        return this;
    }

    @Step("Выбираем категорию из списка категорий")
    public PreferencesSteps chooseCategoryFromList(String categoryName){
        preferencesPage.PreferencesAddModalCategories.shouldBe(visible);
        preferencesPage.PreferencesAddModalCategories.hover();
        preferencesPage.CategoryCheckbox(categoryName).scrollTo().hover().click();;
        return this;
    }

    @Step("Выбираем категорию из списка категорий")
    public PreferencesSteps chCategoryFromList(String categoryName){
        preferencesPage.PreferencesAddModalCategories.shouldBe(visible);
        preferencesPage.PreferencesAddModalCategories.hover();
        preferencesPage.CategoryFewCheckbox(categoryName).scrollTo().hover().click();;
        return this;
    }
    @Step("Нажимаем на кнопку Добавить категорию после выбора из списка")
    public PreferencesSteps ClickAddBtnInListCategories(){
        preferencesPage.AddCategoriesBtnModal.hover().click();
        CheckNoExistLoadingBar();
        checkAbsencePreferenceCategoriesModal();
        return this;
    }

    @Step("Выбираем категорию из списка категорий и закрываем модальное окно")
    public PreferencesSteps chooseCategoryFromListAndClose(String categoryName){
        preferencesPage.CategoryCheckbox(categoryName).scrollTo().click();;
        preferencesPage.CloseModalCategoriesBtn.hover().click();
        checkAbsencePreferenceCategoriesModal();
        return this;
    }
    @Step("Проверяем добавление категории в блок предпочтений по категориям.")
    public PreferencesSteps CheckExistCategoryInBlock(String categoryName){
        CheckNoExistLoadingBarVisibleDisappear();
        Assert.assertTrue( preferencesPage.PreferenceItem(categoryName).exists(),"Category - " + categoryName + " should be displayed in the block.");
        return this;
    }
    @Step("Проверяем отсутствие категори в блок предпочтений по категориям.")
    public PreferencesSteps CheckNotExistCategoryInBlock(String categoryName){
        CheckNoExistLoadingBarVisibleDisappear();
        Assert.assertFalse( preferencesPage.PreferenceItem(categoryName).exists(),"Category - " + categoryName + " should not be displayed in the block.");
        return this;
    }

    @Step("Проверяем отсутствие Category после добавления в списке категорий.")
    public PreferencesSteps CheckCategoryIsNotList(String categoryName){
        CheckNoExistLoadingBarVisibleDisappear();
        Assert.assertFalse( preferencesPage.CategoryCheckbox(categoryName).exists(),"Category - " + categoryName + " should not be displayed in the lists.");
        return this;
    }
    @Step("Проверяем присутствие Category в списке категорий.")
    public PreferencesSteps CheckCategoryIsList(String categoryName){
        CheckNoExistLoadingBarVisibleDisappear();
        Assert.assertTrue( preferencesPage.CategoryCheckbox(categoryName).exists(),"Category - " + categoryName + " should be displayed in the lists.");
        return this;
    }

    @Step("Нажимаем на иконку удаления рядом с категорией")
    public PreferencesSteps clickRemoveCategoryBtn(String categoryName){
        preferencesPage.PreferenceItemRemoveBtn(categoryName).hover().click();
        CheckNoExistLoadingBar();
        return this;
    }
    @Step("Нажимаем на иконку удалить все категории")
    public PreferencesSteps clickRemoveAllCategoryBtn(){
        preferencesPage.DeleteAllCategoriesBtn.hover().click();
        CheckNoExistLoadingBar();
        return this;
    }
}
