package steps.UI.market.PersonnelData;

import env.Base_Container;
import io.qameta.allure.Step;
import org.openqa.selenium.Keys;
import testPageLocator.market.mainPage.MainLocators;
import testPageLocator.market.profilePage.MessagesLocators;
import testPageLocator.market.profilePage.ProfileLocators;

import static helpers.Helper.clearInputForReact;
import static org.testng.AssertJUnit.assertEquals;

public class MessagesSteps extends Base_Container {
    MessagesLocators messagePage;

    @Step("Нажимаем кнопку добавить новое сообщение")
    public MessagesSteps clickAddNewMsg(){
        messagePage.AddNewMessage.click();
        return this;
    }

}
