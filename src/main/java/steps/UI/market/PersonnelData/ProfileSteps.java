package steps.UI.market.PersonnelData;

import env.Base_Container;
import io.qameta.allure.Step;
import org.openqa.selenium.Keys;
import testPageLocator.market.mainPage.MainLocators;
import testPageLocator.market.profilePage.ProfileLocators;

import static helpers.Helper.clearInputForReact;
import static io.qameta.allure.Allure.step;
import static org.testng.AssertJUnit.*;

public class ProfileSteps extends Base_Container {
    ProfileLocators profilePage;
    MainLocators mainPage;

    // В витрине после авторизации открываем меню пользователя и переходим в профиль
    public void clickProfile(){
        mainPage.UserMenu.click();
        mainPage.ProfileBtn.click();
    }

    @Step("Оставляем пустое поле ФИО и переводим курсор на другое поле")
    public ProfileSteps stepClearFieldFio(){
        profilePage.NameField.sendKeys(Keys.CONTROL, "a");
        profilePage.NameField.sendKeys(Keys.DELETE);
        profilePage.PhoneField.click();
        return this;
    }

    @Step("Оставляем пустое поле E-mail и переводим курсор на другое поле")
    public ProfileSteps stepClearFieldEmail(){
        profilePage.EmailField.sendKeys(Keys.CONTROL, "a");
        profilePage.EmailField.sendKeys(Keys.DELETE);
        profilePage.PhoneField.click();
        return this;
    }
    @Step("Оставляем пустое поле Phone и переводим курсор на другое поле")
    public ProfileSteps clearFieldPhone(){
        profilePage.PhoneField.hover().click();
        clearInputForReact(profilePage.PhoneField);
        //profilePage.NameField.scrollTo().click();
        return this;
    }
    @Step("Заполняем поле ФИО")
    public ProfileSteps inputNameField(String longName){
        profilePage.NameField.setValue(longName);
        return this;
    }

    @Step("Заполняем поле Телефон")
    public ProfileSteps inputPhoneField(String phone){
        profilePage.PhoneField.scrollTo().setValue(phone);
       // profilePage.NameField.scrollTo().click();
        return this;
    }
    @Step("Заполняем поле email")
    public ProfileSteps inputEmailField(String email){
        profilePage.EmailField.setValue(email);
        profilePage.PhoneField.click();
        return this;
    }

    @Step("Нажимаем на кнопку Готово")
    public ProfileSteps stepClickReadyBtn(){
        profilePage.ProfileReadyBtn.scrollTo().hover().click();
        return this;
    }

    @Step("Проверка отображения алерта об успешном сохранении данных")
    public ProfileSteps stepCheckDisplayAlert(){
        profilePage.AlertField.shouldBe();
        return this;
    }

    @Step("Проверяем данные в поле FIO")
    public ProfileSteps stepCheckFieldFio(String name){
       assertEquals("В поле должны быть корректные данные.", name, profilePage.NameField.getValue());
       return this;
    }

    @Step("Проверяем ошибку под полем ФИО ")
    public ProfileSteps stepCheckErrorFieldFio(String errorMsg){
        assertEquals("Ошибка должна отображаться.", errorMsg, profilePage.NameErrorMsg.getText());
        return this;
    }

    @Step("Проверяем ошибку под полем телефон")
    public ProfileSteps stepCheckErrorInvalidNumberFieldPhone(String errorMsg){
        assertEquals("Ошибка должна отображаться.", errorMsg, profilePage.PhoneErrorMsg.getText());
        return this;
    }

    @Step("Проверяем ошибку под полем Email ")
    public ProfileSteps stepCheckErrorInvalidEmailFieldEmail(String errorMsg){
        assertEquals("Ошибка должна отображаться.", errorMsg , profilePage.EmailErrorMsg.getText());
        return this;
    }

    @Step("Проверяем текст в поле Phone ")
    public ProfileSteps stepCheckFieldPhone(){
        assertEquals("Поле должно быть пустым.", "", profilePage.PhoneField.getValue());
        return this;
    }


}
