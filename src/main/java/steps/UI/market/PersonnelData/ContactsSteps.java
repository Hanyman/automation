package steps.UI.market.PersonnelData;

import env.Base_Container;
import io.qameta.allure.Step;
import org.testng.Assert;
import testPageLocator.market.mainPage.MainLocators;
import testPageLocator.market.profilePage.ContactsLocators;

import static com.codeborne.selenide.Condition.disappear;
import static helpers.Helper.clearInputForReact;

public class ContactsSteps extends Base_Container {
    ContactsLocators contactsPage;
    MainLocators mainLocators;
    @Step("Нажимаем кнопку добавить получателя")
    public ContactsSteps clickAddNewRecipient(){
        contactsPage.AddNewRecipient.click();
        return this;
    }

    @Step("Нажимаем кнопку добавить получателя в окне ввода данных")
    public ContactsSteps clickSaveRecipient(){
        contactsPage.RecipientAddBtn.click();
        return this;
    }

    @Step("Нажимаем кнопку Изменить напротив сцществующего получателя")
    public ContactsSteps clickEditRecipient(String name){
        contactsPage.EditRecipientRowBtn(name).click();
        contactsPage.RecipientEditBtn.click();
        return this;
    }
    @Step("Нажимаем кнопку Удалить напротив сцществующего получателя")
    public ContactsSteps clickRemoveRecipient(String name) throws InterruptedException {
        contactsPage.EditRecipientRowBtn(name).click();
        contactsPage.RecipientDeleteBtn.click();
        mainLocators.pageLoadingBar.shouldBe(disappear);
        Thread.sleep(1000);
        return this;
    }

    @Step("Заполняем все поля для добавления нового получателя")
    public ContactsSteps inputRecipientFields(String name, String phone, String email) throws InterruptedException {
        clearInputForReact(contactsPage.RecipientNameField);
        contactsPage.RecipientNameField.setValue(name);
        clearInputForReact( contactsPage.RecipientPhoneField);
        contactsPage.RecipientPhoneField.setValue(phone);
        clearInputForReact(contactsPage.RecipientMailField);
        contactsPage.RecipientMailField.setValue(email);
        contactsPage.RecipientAddBtn.click();
        mainLocators.pageLoadingBar.shouldBe(disappear);
        Thread.sleep(1000);
        return this;
    }

    @Step("Проверяем строку с данными нового получателя")
    public ContactsSteps checkRecipientRow(String fio, String phone, String email){
        Assert.assertEquals( contactsPage.RecipientFioRow.getText().trim(),fio,"Фио получателя должно отображаться.");
        Assert.assertEquals( contactsPage.RecipientPhoneRow.getText().trim(),phone,"Phone получателя должен отображаться.");
        Assert.assertEquals( contactsPage.RecipientEmailRow.getText().trim(),email,"Email получателя должен отображаться.");
        return this;
    }
    @Step("Проверяем строку с пустым списком получателей")
    public ContactsSteps checkRecipientEmptyRow(){
        Assert.assertEquals( contactsPage.EmptyRecipient.getText().trim(),"Вы еще не добавляли получателей","Сообщение должно отображаться когда список получателей пустой.");
        return this;
    }
    @Step("Проверяем сообщение под полем ФИ")
    public ContactsSteps checkRecipientNameFieldMsg(String text){
        Assert.assertEquals( contactsPage.RecipientNameFieldMsg.getText().trim(),text,"Сообщение под полем должно отображаться.");
        return this;
    }
    @Step("Проверяем сообщение под полем Телефон")
    public ContactsSteps checkRecipientPhoneFieldMsg(String text){
        Assert.assertEquals( contactsPage.RecipientPhoneFieldMsg.getText().trim(),text,"Сообщение под полем должно отображаться.");
        return this;
    }
}
