package steps.UI.market;

import env.Base_Container;
import io.qameta.allure.Step;
import org.aeonbits.owner.ConfigFactory;
import org.openqa.selenium.interactions.Actions;
import org.testng.Assert;
import steps.UI.OpenPageSteps;
import steps.api.admin.Customers.CustomersAdminApiSteps;
import testPageLocator.market.authPage.AuthLocators;
import testPageLocator.market.mainPage.MainLocators;


import static com.codeborne.selenide.Condition.visible;

import static io.qameta.allure.Allure.step;

public class AuthSteps extends Base_Container {
    AuthLocators authPage;
    MainLocators mainPage;

    OpenPageSteps openPageSteps = new OpenPageSteps();


    @Step("Логинимся в маркет с вводам кода смс и подтверждением локации.")
    public AuthSteps loginUserCorrect(String userPhone,String code) throws InterruptedException {
        driver = openPageSteps.stepOpenPageMarketGetDriver("");
        Thread.sleep(1000);
        mainPage.LogInBtn.shouldBe(visible);
        mainPage.LogInBtn.hover().click();
        if(authPage.TermsAgreementBlock.isDisplayed()){
            Actions action = new Actions(driver);
            action.moveToElement(authPage.TermsAgreementCheckbox).click().build().perform();
        }
        authPage.LoginField.setValue(userPhone);
        authPage.GetCodeSms.click();
        authPage.SmsField.setValue(code);
        authPage.ConfirmBtn.click();
        try {
            authPage.LocationPage.shouldBe(visible);
            authPage.ConfirmCity.click();
        } catch (com.codeborne.selenide.ex.ElementNotFound e) {

        }
        return this;
    }

    @Step("Логинимся в маркет с вводам кода смс.")
    public AuthSteps loginUserCorrectWithoutConfirmLocation(String userPhone,String code) throws InterruptedException {
        driver = openPageSteps.stepOpenPageMarketGetDriver("");
        Thread.sleep(1000);
        mainPage.LogInBtn.shouldBe(visible);
        mainPage.LogInBtn.hover().click();
        if(authPage.TermsAgreementBlock.isDisplayed()){
            Actions action = new Actions(driver);
            action.moveToElement(authPage.TermsAgreementCheckbox).click().build().perform();
        }
        authPage.LoginField.setValue(userPhone);
        authPage.GetCodeSms.click();
        authPage.SmsField.setValue(code);
        authPage.ConfirmBtn.click();

        return this;
    }

    @Step("Заполняем поле Телефон некорректными значениями")
    public AuthSteps InputCorrectNumber(String phone) {
        clickLoginBtn();
        authPage.LoginField.setValue(phone);
        authPage.GetCodeSms.click();
        return this;
    }

    @Step("Заполняем поля номер и код корректными данными")
    public AuthSteps InputNumberAndCode(String phone,String code) {
        clickLoginBtn();
        authPage.LoginField.setValue(phone);
        setCheckboxAgreement();
        authPage.GetCodeSms.click();
        authPage.SmsField.setValue(code);
        authPage.ConfirmBtn.click();
        return this;
    }

    @Step("Проверяем сообщение о блокировке при авторизации")
    public AuthSteps checkAuthMsg(String msg){
        Assert.assertEquals(authPage.AlertText.getText().trim(),msg,"Ошибка должна отображаться на форме авторизации.");
        return this;
    }
    @Step("Проверяем что форма ВК отображается")
    public AuthSteps checkVkFormExists(){
        authPage.VKForm.shouldBe(visible);
        return this;
    }
    @Step("Проверяем что форма Gmail отображается")
    public AuthSteps checkGmailFormExists(){
        authPage.GmailForm.shouldBe(visible);
        return this;
    }

    @Step("Проверяем что дропдаун для номера телефона содержит 3 страны")
    public AuthSteps checkPhoneCountry(String [] expectedTexts){

        for (int i = 0; i < expectedTexts.length; i++) {
            Assert.assertEquals(authPage.SelectCountry.get(i).getText().trim(),expectedTexts[i],"Страна должна отображаться в дропдауне.");
        }
        return this;
    }
    @Step("Нажимаем на чекбокс согласие с правилами магазина")
    public AuthSteps setCheckboxAgreement(){
        if(authPage.TermsAgreementBlock.isDisplayed()){
            Actions action = new Actions(getWebDriver());
            action.moveToElement(authPage.TermsAgreementCheckbox).click().build().perform();
        }
        return this;
    }

    @Step("Входим через соц сеть ВК")
    public AuthSteps clickAuthViaVk(){
        clickLoginBtn();
        authPage.VKBtn.click();
        return this;
    }
    @Step("Нажимаем на кнопку логина")
    public AuthSteps clickLoginBtn(){
        mainPage.LogInBtn.hover().click();
        return this;
    }
    @Step("Нажимаем на селектор выбора формата телефона")
    public AuthSteps clickCountrySelector(){
        authPage.CountrySelector.hover().click();
        authPage.CountrySelectorList.shouldBe(visible);
        return this;
    }

    @Step("Входим через соц сеть Gmail")
    public AuthSteps clickAuthViaGmail(){
        clickLoginBtn();
        authPage.GmailBtn.click();
        return this;
    }

    @Step("Заполнение поля Номер телефона.")
    public AuthSteps inputLoginField(String phone){
        authPage.LoginField.setValue(phone);
        return this;
    }

    @Step("Нажатие на кнопку Получить код.")
    public AuthSteps clickGetSmsBtn(){
        authPage.GetCodeSms.click();
        return this;
    }

    @Step("Нажатие на кнопку подтверждения.")
    public AuthSteps clickConfirmBtn(){
        //authPage.ConfirmBtn.shouldBe(visible);
        authPage.ConfirmBtn.click();
        return this;
    }

    @Step("Проверка ошибки под полем Phone")
    public AuthSteps checkErrorFieldPhone(String expected){
        Assert.assertEquals(authPage.PhoneErrorMsg.getText(),expected,"Ошибка должна отображаться под полем.");
        return this;
    }
    @Step("Проверка ошибки под полем Sms")
    public AuthSteps checkErrorFieldSms(String expected){
        Assert.assertEquals(authPage.SmsErrorMsg.getText(),expected,"Ошибка должна отображаться под полем.");
        return this;
    }

    @Step("Проверяем текст в поле Code")
    public AuthSteps checkCheckFieldCode(String expected){
        Assert.assertEquals(authPage.SmsField.getText(),expected,"Поле должно быть пустым");
        return this;
    }
    @Step("Проверяем текст в поле Phone")
    public AuthSteps checkFieldPhone(String expected){
        Assert.assertEquals(authPage.LoginField.getText(),expected,"Поле должно быть пустым.");
        return this;
    }

}
