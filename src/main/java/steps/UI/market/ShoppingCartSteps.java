package steps.UI.market;

import env.Base_Container;
import io.qameta.allure.Step;
import org.testng.Assert;
import testPageLocator.market.mainPage.MainLocators;
import testPageLocator.market.productsPage.ProductsLocators;

import static com.codeborne.selenide.Condition.*;
import static io.qameta.allure.Allure.step;
import static junit.framework.TestCase.assertEquals;

public class ShoppingCartSteps extends Base_Container {
    ProductsLocators productsPage;
    MainLocators mainPage;

    @Step("Добавляем товар в корзину по нажатию на кнопку Купить на товаре.")
    public ShoppingCartSteps addProductInShoppingCart(String productName)  {

        productsPage.ProductCard(productName).hover();
        productsPage.AddCartProductBtn(productName).hover().click();
//        productsPage.AddCartProductBtn(productName).should(disappear);

        return this;
    }
    @Step("Добавляем товар в корзину через карточку товара.")
    public ShoppingCartSteps addProductInShoppingCartInProduct(String productName) throws InterruptedException {
        Thread.sleep(1000);
        productsPage.ProductCard(productName).scrollTo();
        productsPage.ProductCard(productName).hover().click();
        Thread.sleep(1000);
        productsPage.BuyBtn.scrollTo().hover().click();
        Thread.sleep(2000);
        mainPage.cartLoader.shouldBe(disappear);
        return this;
    }

    @Step("Нажимаем на иконку корзины.")
    public ShoppingCartSteps clickCartBtn() throws InterruptedException {
        mainPage.CartBtn.click();
        Thread.sleep(1000);
        //mainPage.pageLoadingBar.shouldBe(disappear);
        //mainPage.cartLoader.shouldBe(appear);
        mainPage.cartLoader.shouldBe(disappear);
        return this;
    }
    @Step("Нажимаем на кнопку в корзине в карточке товара.")
    public ShoppingCartSteps clickInCartBtn() throws InterruptedException {
        Thread.sleep(1000);
        productsPage.InCartBtn.click();
        Thread.sleep(1000);
       // mainPage.cartLoader.shouldBe(appear);
        mainPage.cartLoader.shouldBe(disappear);
        return this;
    }

    @Step("Проверяем количество товаров в корзине.")
    public ShoppingCartSteps checkCountProductsInCart(String expected){
        Assert.assertEquals(productsPage.CountProducts.getText(),expected,"Количество товаров должно быть корреткно.");
        return this;
    }

}
