package steps.UI;

import static com.codeborne.selenide.Condition.*;
import static com.codeborne.selenide.Selenide.$;
import static constants.end_point.AdminPanelEndpoint.*;

import env.Base_Container;
import io.qameta.allure.Step;
import org.aeonbits.owner.ConfigFactory;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import testPageLocator.general.GeneralLocators;
import testPageLocator.market.mainPage.MainLocators;

import static com.codeborne.selenide.Selenide.open;
import static helpers.Helper.waitPageLoaded;
import static io.qameta.allure.Allure.step;

public class OpenPageSteps extends Base_Container {
    GeneralLocators loginAdminPage;
    MainLocators mainLocators;

    public OpenPageSteps loginAdminPage() throws InterruptedException {
        stepOpenPageAdmin(login.path());
        if(loginAdminPage.PageLogin.isDisplayed()) {
            loginAdminPage.EmailField.setValue(adminConfig.getAdminLogin());
            loginAdminPage.PasswordField.setValue(adminConfig.getAdminPassword());
            loginAdminPage.SigInBtn.click();
            Thread.sleep(1000);
        }
        return this;
    }
    // Common Steps
    public void stepOpenPageMerchant(String path){
        step("Открытие страницы " + urlsConfig.getMerchantUrl()+path, () -> open(urlsConfig.getMerchantUrl()+path));
    }

    public void stepOpenPageMarket(String path){
       // open(urlConfig.getMarketUrl()+path);
        step("Открытие страницы " + urlsConfig.getMarketUrl()+path, () -> {
                open(urlsConfig.getMarketUrl()+path);
                waitPageLoaded(getWebDriver(),globalTimeout);
                Thread.sleep(1500);
        });

    }

    @Step("Открытие страницы ")
    public WebDriver stepOpenPageMarketGetDriver(String path){
        open(urlsConfig.getMarketUrl()+path);
        driver = getWebDriver();
        waitPageLoaded(driver,globalTimeout);
        return driver;
    }

    public void stepOpenPageAdmin(String path){
        step("Открытие страницы " + urlsConfig.getAdminUrl()+path, () -> open(urlsConfig.getAdminUrl()+path));
    }

    @Step("Проверка отображения активного меню в личных данных")
    public void stepProfileMenuIsDisplayed(String href){
        Assert.assertTrue(mainLocators.ProfileMenuSelectActive(href).shouldBe(appear).isDisplayed(),"Активное левое меню " + href);
    }


}
