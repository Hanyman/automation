package steps.UI.merchant;

import com.codeborne.selenide.Condition;
import env.Base_Container;
import helpers.Generator;
import io.qameta.allure.Step;
import org.testng.Assert;
import steps.UI.GeneralSteps;
import steps.UI.OpenPageSteps;
import steps.UI.admin.MerchantsTabSteps;
import testPageLocator.merchant.CommonLocators;
import testPageLocator.merchant.RegisterLocators;

import static io.qameta.allure.Allure.step;
import static org.testng.AssertJUnit.assertEquals;

public class CommonMerchantSteps extends Base_Container {

    CommonLocators general;

    @Step("Ожидания открытия страницы мерчанта.")
    public CommonMerchantSteps waitMerchantPage(){
        general.MasPage.shouldBe(Condition.visible);
        return this;
    }

    @Step("Проверка статуса мерчанта - Отключен.")
    public CommonMerchantSteps checkMerchantStatusDisabled(String expected){
        Assert.assertEquals(general.StatusMerchantDisabled.getText().trim(),expected,"Статус мерчанта должен отображаться корректно.");
        return this;
    }
    @Step("Проверка статуса мерчанта - Приостановлен.")
    public CommonMerchantSteps checkMerchantStatus(String expected){
        Assert.assertEquals(general.StatusMerchant.getText().trim(),expected,"Статус мерчанта должен отображаться корректно.");
        return this;
    }

    @Step("Проверка сообщения при изменении статуса на приостановлен.")
    public CommonMerchantSteps checkMerchantMsg(String expected){
        Assert.assertEquals(general.StatusMerchantMsg.getText().trim(),expected,"Статус мерчанта должен отображаться корректно.");
        return this;
    }

    @Step("Проверка статуса мерчанта - корректировка информации.")
    public CommonMerchantSteps checkMerchantStatusCorrectionInfo(String expected){
        Assert.assertEquals(general.CorrectionInfStatus.getText().trim(),expected,"Статус мерчанта должен отображаться корректно.");
        return this;
    }
    @Step("Проверка сообщения при изменении статуса.")
    public CommonMerchantSteps checkMerchantMsgCorrectionInfo(String expected){
        Assert.assertEquals(general.CorrectionInfoMsg.getText().trim(),expected,"Сообщение у мерчанта должен отображаться корректно.");
        return this;
    }

}
