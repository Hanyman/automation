package steps.UI.merchant;

import com.jayway.jsonpath.JsonPath;
import constants.data.*;
import env.Base_Container;
import helpers.Generator;
import steps.UI.GeneralSteps;
import steps.UI.OpenPageSteps;
import steps.api.merchant.ActivationMerchantApiSteps;
import testPageLocator.admin.MerchantsPage.PageMerchants;
import testPageLocator.merchant.RegisterLocators;

import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URL;

import static com.codeborne.selenide.Condition.disappear;
import static com.codeborne.selenide.Condition.visible;
import static constants.end_point.AdminPanelEndpoint.*;
import static helpers.api.ApiHelper.*;
import static helpers.api.ApiHelper.clearQuery;
import static io.qameta.allure.Allure.step;
import static org.testng.AssertJUnit.*;

public class ActivationSteps extends Base_Container {

    PageMerchants merchantsPage;
    RegisterLocators registerPage;
    Generator generator = new Generator();
    GeneralSteps commonSteps = new GeneralSteps();
    OpenPageSteps openPageSteps = new OpenPageSteps();
    ActivationMerchantApiSteps activationMerchantApiSteps = new ActivationMerchantApiSteps();
    String link;

    public void stepActivateMerchant(String nameCompany,String email) throws InterruptedException, MalformedURLException, UnsupportedEncodingException {

        openPageSteps.loginAdminPage();
        openPageSteps.stepOpenPageAdmin(merchant_list_registration.path());
        stepClickNameOrganization(nameCompany);
        stepClickActivateBtn();
        stepWaitDisappearActivateBtn();
        String link = activationMerchantApiSteps.getLinkEmail(email);
        openPageSteps.stepOpenPageMerchant(link);


    }
    public void stepInputPasswordMerchant(){
        stepSetPasswordMerchant();
    }
    public void stepInputOrganizationInfo(){
        stepInputNdsField();
        stepCheckNotNullNameOrganizationField();
        stepInputLegalAddressField(OrganizationInfoMerchant.LEGAL_ADDRESS);
        //stepClickSuggestionItem();
        stepInputFactAddressField(OrganizationInfoMerchant.FACT_ADDRESS);
       // stepClickSuggestionItem();
        stepInputCeoFirstNameField(generator.getFirstName());
        stepInputCeoLastNameField(generator.getLastName());
        stepInputCeoMiddleNameField(generator.getMiddleName());
        stepCheckNotNullInnField();
        stepInputKppField(OrganizationInfoMerchant.KPP);
        stepInputPaymentAccountField(OrganizationInfoMerchant.PAYMENT_ACCOUNT);
        stepInputBankField(OrganizationInfoMerchant.BANK);
        //stepClickSuggestionItem();

    }

    public void stepUploadFiles() {
        commonSteps.stepCheckLinkFile("Проверяем наличие ссылки на файл Товарный ассортимент",registerPage.ProductsRangeFileLink);
        commonSteps.stepUploadFile("Загружаем файл Свидетельство ИНН",registerPage.InnFile,registerPage.InnFileLink);
        commonSteps.stepUploadFile("Загружаем файл Свидетельство ОГРН",registerPage.OgrnFile,registerPage.OgrnFileLink);
        commonSteps.stepUploadFile("Загружаем файл Лист записи ЕГРЮЛ",registerPage.EgrulFile,registerPage.EgrulFileLink);
        commonSteps.stepUploadFile("Загружаем файл Устав компании с отметками налогового органа",registerPage.CompanyCharterFile,registerPage.CompanyCharterLink);
        commonSteps.stepUploadFile("Решение или приказ о назначении генерального директора",registerPage.OrderCeoFile,registerPage.OrderCeoLink);

    }
    public void stepActivateLink(String email) throws MalformedURLException, UnsupportedEncodingException {
        step("Получаем ссылку для активации мерчанта" , () -> { link = getParamJson(JsonPath.read(requestPost(email),"$..link").toString());});
        String userid = String.valueOf(getParamUrl(new URL(link)).get("user_id"));
        String token = String.valueOf(getParamUrl(new URL(link)).get("token"));
        openPageSteps.stepOpenPageMerchant("/confirm-email?user_id="+clearQuery(userid)+"&token="+clearQuery(token)+"");
    }

    public void stepCheckStatusMerchantInAdmin(String nameCompany,String status){
        openPageSteps.stepOpenPageAdmin(merchant_list_active.path());
        stepCheckStatusMerchantAdmin(nameCompany,status);
    }

    public void stepClickNameMerchantInAdmin(String nameCompany){
        openPageSteps.stepOpenPageAdmin(merchant_list_active.path());
        stepClickNameOrganization(nameCompany);
        //stepClickNameOrganization(nameCompany);
    }

    public void stepSelectStatusMerchantInAdmin(String status){
        //openPageSteps.stepOpenPageAdmin(PathUrlAdmin.merchant_list_active.getPath());
        stepSelectOptionStatus(status);
        stepClickSaveBtn();

    }

    // Common Steps
    public void stepClickNameOrganization(String nameCompany){ step("Нажимаем на название организации" + nameCompany, () -> {merchantsPage.nameCompanyBtn(nameCompany).click();}); }
    public void stepClickActivateBtn(){ step("Нажимаем на кнопку Активировать" , () -> {merchantsPage.ActivateBtn.click();});}
    public void stepWaitDisappearActivateBtn(){ step("Ожидаем, когда кнопка Активировать станет недоступна" , () -> {merchantsPage.ActivateBtn.should(disappear);});}
    public void stepSetPasswordMerchant(){ step("Устанавливаем пароль для мерчанта и сохраняем" , () -> {
        merchantsPage.SetPasswordField.setValue("1qazXSW@");
        merchantsPage.SetPasswordSaveBtn.click();
    });}

    // Steps Info merchant
    public void stepInputNdsField(){ step("Выбор системы налогообложения", () -> {registerPage.TaxSystemSelect.click();registerPage.TaxSystemSelect.selectOption("УСНО");}); }
    public void stepInputLegalAddressField(String value){ step("Заполняем поле Юридический адрес", () -> {registerPage.LegalAddressField.setValue(value);}); }
    public void stepInputFactAddressField(String value){ step("Заполняем поле Фактический адрес", () -> {registerPage.FactAddressField.setValue(value);}); }
    public void stepInputCeoLastNameField(String value){ step("Заполняем поле Генеральный директор — Фамилия", () -> {registerPage.CeoLastNameField.setValue(value);}); }
    public void stepInputCeoFirstNameField(String value){ step("Заполняем поле Генеральный директор — Имя", () -> {registerPage.CeoFirstNameField.setValue(value);}); }
    public void stepInputCeoMiddleNameField(String value){ step("Заполняем поле Генеральный директор — Отчетво", () -> {registerPage.CeoMiddleNameField.setValue(value);}); }
    public void stepInputKppField(String value){ step("Заполняем поле КПП", () -> {registerPage.KppField.setValue(value);}); }
    public void stepInputPaymentAccountField(String value){ step("Заполняем поле Номер банковского счета", () -> {registerPage.PaymentAccountField.setValue(value);}); }
    public void stepInputBankField(String value){ step("Заполняем поле Банк", () -> {registerPage.BankField.setValue(value);}); }
    public void stepInputCorrespondentAccountField(String value){ step("Заполняем поле Номер корреспондентского счета банка", () -> {registerPage.CorrespondentAccountField.setValue(value);}); }
    public void stepInputBankAddressField(String value){ step("Заполняем поле Юридический адрес банка", () -> {registerPage.BankAddressField.setValue(value);}); }
    public void stepInputBankBikField(String value){ step("Заполняем поле Бик банка", () -> {registerPage.BankBikField.setValue(value);}); }

    public void stepClickSuggestionItem(){ step("Нажимаем на пункт подсказки", () -> {registerPage.SuggestionItem.click();}); }
    public void stepClickSendOrderBtn(){ step("Нажимаем на кнопку Отправить заявку", () -> {registerPage.AgreeBtn.click();}); }
    public void stepCheckStatusActivation(){ step("Проверяем статус активации мерчанта", () -> { assertEquals("Статус должен быть корректным","Активация",registerPage.StatusActivation.getText());}); }
    public void stepCheckStatusMerchantAdmin(String nameCompany,String status){ step("Проверяем статус мерчанта в админке", () -> { assertEquals("Статус должен быть корректным", status,merchantsPage.statusCompany(nameCompany).getText());}); }
    public void stepSelectOptionStatus(String option){ step("Изменяем статус мерчанта в админке", () -> { merchantsPage.StatusSelect.shouldBe(visible);merchantsPage.StatusSelect.click(); merchantsPage.StatusSelect.selectOption(option);}); }
    public void stepClickSaveBtn(){ step("Нажимаем на кнопку сохранить", () -> { merchantsPage.SuccessBtn.click();}); }
    public void stepClickRegistrationBtn(){ step("Нажимаем на кнопку Создать мерчанта", () -> {merchantsPage.CreateMerchantBtn.click();});}


    public void stepCheckNotNullNameOrganizationField(){  step("Проверяем, что поле Юридическое наименование организации не пустое" , () -> {
        assertNotNull(registerPage.NameOrganizationField.getValue(),"Поле не должно быть пустым.");  }); }
    public void stepCheckNotNullInnField(){  step("Проверяем, что поле ИНН не пустое" , () -> {
        assertNotNull(registerPage.InnField.getValue(),"Поле не должно быть пустым.");  }); }


}
