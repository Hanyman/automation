package steps.UI.merchant;

import constants.ErrorsMsg;
import constants.SuccessMsg;
import static constants.end_point.MerchantPanelEndpoint.*;
import env.Base_Container;
import helpers.Generator;
import steps.UI.GeneralSteps;
import steps.UI.OpenPageSteps;
import testPageLocator.merchant.RegisterLocators;

import static helpers.Helper.clearInputForReact;
import static helpers.Helper.getClearName;
import static io.qameta.allure.Allure.step;
import static org.testng.AssertJUnit.*;

public class RegistrationSteps extends Base_Container {

    RegisterLocators regPage;

    Generator generator = new Generator();
    String email = generator.getEmail();
    OpenPageSteps openPageSteps = new OpenPageSteps();
    GeneralSteps commonSteps = new GeneralSteps();


    public void createMerchant(){
        stepInputSurnameField(generator.getLastName());
        stepInputNameField(generator.getFirstName());
        stepInputMiddleNameField(generator.getMiddleName());
        stepInputPhoneField(generator.getPhoneNumb());
        stepInputEmailField(email);
        stepInputInnField(generator.getInn());
        stepInputOrganizationNameField(generator.getCompany());
        stepInputAddressField("Moskow");
        commonSteps.stepUploadFile("Загружаем файл",regPage.ProductsFileField,regPage.ProductsFileSuccess);
        stepClickRegistrationBtn();
        stepCheckSuccessRegistration();

    }

    public void createMerchant(String email, String nameOrg){
        openPageSteps.stepOpenPageMerchant(registration.path());
        stepInputSurnameField(generator.getLastName());
        stepInputNameField(generator.getFirstName());
        stepInputMiddleNameField(generator.getMiddleName());
        stepInputPhoneField(generator.getPhoneNumb());
        stepInputEmailField(email);
        stepInputInnField(generator.getInn());
        stepInputOrganizationNameField(nameOrg);
        stepInputAddressField("Moskow");
        commonSteps.stepUploadFile("Загружаем файл",regPage.ProductsFileField,regPage.ProductsFileSuccess);
        stepClickRegistrationBtn();
        stepCheckSuccessRegistration();

    }


    // Common Steps
    public void stepInputSurnameField(String surname){ step("Заполняем поле Фамилия", () -> {regPage.SurnameField.setValue(surname);}); }
    public void stepInputNameField(String name){ step("Заполняем поле Имя", () -> {regPage.NameField.setValue(name);});}
    public void stepInputMiddleNameField(String middlename){ step("Заполняем поле Отчество", () -> {regPage.MiddleNameField.setValue(middlename);});}
    private void stepInputPhoneField(String phone){ step("Заполняем поле Телефон", () -> {regPage.PhoneField.setValue(phone);});}
    private void stepInputEmailField(String email){ step("Заполняем поле E-mail", () -> {regPage.EmailField.setValue(email);});}
    private void stepInputInnField(String inn){ step("Заполняем поле ИНН", () -> {regPage.InnField.setValue(inn);});}
    private void stepInputOrganizationNameField(String organizationname){ step("Заполняем поле Название организации", () -> {regPage.OrganizationField.setValue(organizationname);});}
    private void stepInputAddressField(String address){ step("Заполняем поле Адреса складов отгрузки", () -> {regPage.AddressField.setValue(address);});}
    public void stepClickRegistrationBtn(){ step("Нажимаем на кнопку Зарегистрироваться", () -> {regPage.RegistrationBtn.click();});}
    public void stepClearFieldPhone(){ step("Очищаем поле телефон", () -> { clearInputForReact(regPage.PhoneField); });  }
    public void stepClearFieldEmail(){ step("Очищаем поле Email", () -> { clearInputForReact(regPage.EmailField); });  }
    public void stepClearFieldInn(){ step("Очищаем поле Inn", () -> { clearInputForReact(regPage.InnField); });  }
    public void stepInputFieldNameAndClick(String name){ step("Вводим в поле Телефон произвольные символы", () -> {
            regPage.PhoneField.setValue(name);
            regPage.EmailField.click(); });  }
    public void stepInputFieldEmailAndClick(String name){ step("Вводим в поле Email произвольные символы", () -> {
            regPage.EmailField.setValue(name);
            Thread.sleep(2000);
            stepClickRegistrationBtn();

    });  }
    public void stepInputFieldInnAndClick(String name){  step("Вводим в поле Inn произвольные символы", () -> {
            regPage.InnField.setValue(name);    regPage.PhoneField.click();});  }

    // Steps verification
    public void stepCheckSuccessRegistration(){
        step("Проверяем отображение успешного сообщения "+ SuccessMsg.registration_merchant_msg.getSuccessMsg()+"", () -> {
            assertEquals("Успешное сообщение должно отображаться.", SuccessMsg.registration_merchant_msg.getSuccessMsg(), regPage.RegistrationText.getText());
        });
    }
    public void stepCheckErrorObligatoryFieldSurname(){
        step("Проверяем отображение ошибки "+ErrorsMsg.obligatory_field_merchant.getErrorMsg()+" под полем Фамилия", () -> {
            assertEquals("Ошибка должна отображаться под полем Фамилия.", ErrorsMsg.obligatory_field_merchant.getErrorMsg(), regPage.SurnameErrorMsg.getText());
        });
    }
    public void stepCheckErrorObligatoryFieldName(){
        step("Проверяем отображение ошибки "+ErrorsMsg.obligatory_field_merchant.getErrorMsg()+" под полем Имя", () -> {
            assertEquals("Ошибка должна отображаться под полем Имя.", ErrorsMsg.obligatory_field_merchant.getErrorMsg(), regPage.NameErrorMsg.getText());
        });
    }
    public void stepCheckErrorObligatoryFieldMiddleName(){
        step("Проверяем отображение ошибки "+ErrorsMsg.obligatory_field_merchant.getErrorMsg()+" под полем Отчество", () -> {
            assertEquals("Ошибка должна отображаться под полем Отчество.", ErrorsMsg.obligatory_field_merchant.getErrorMsg(), regPage.MiddleNameErrorMsg.getText());
        });
    }
    public void stepCheckErrorObligatoryFieldPhone(){
        step("Проверяем отображение ошибки "+ErrorsMsg.obligatory_field_merchant.getErrorMsg()+" под полем Телефон", () -> {
            assertEquals("Ошибка должна отображаться под полем Телефон.", ErrorsMsg.obligatory_field_merchant.getErrorMsg(), regPage.PhoneErrorMsg.getText());
        });
    }
    public void stepCheckErrorObligatoryFieldEmail(){
        step("Проверяем отображение ошибки "+ErrorsMsg.obligatory_field_merchant.getErrorMsg()+" под полем Email", () -> {
            assertEquals("Ошибка должна отображаться под полем Email.", ErrorsMsg.obligatory_field_merchant.getErrorMsg(), regPage.EmailErrorMsg.getText());
        });
    }
    public void stepCheckErrorObligatoryFieldInn(){
        step("Проверяем отображение ошибки "+ErrorsMsg.obligatory_field_merchant.getErrorMsg()+" под полем INN", () -> {
            assertEquals("Ошибка должна отображаться под полем INN.", ErrorsMsg.obligatory_field_merchant.getErrorMsg(), regPage.InnErrorMsg.getText());
        });
    }
    public void stepCheckErrorObligatoryFieldOrganization(){
        step("Проверяем отображение ошибки "+ErrorsMsg.obligatory_field_merchant.getErrorMsg()+" под полем Название организации", () -> {
            assertEquals("Ошибка должна отображаться под полем Название организации.", ErrorsMsg.obligatory_field_merchant.getErrorMsg(), regPage.OrganizationErrorMsg.getText());
        });
    }
    public void stepCheckErrorObligatoryFieldAddress(){
        step("Проверяем отображение ошибки "+ErrorsMsg.obligatory_field_merchant.getErrorMsg()+" под полем Адреса складов отгрузки", () -> {
            assertEquals("Ошибка должна отображаться под полем Адреса складов отгрузки.", ErrorsMsg.obligatory_field_merchant.getErrorMsg(), regPage.AddressErrorMsg.getText());
        });
    }
    public void stepCheckErrorObligatoryFieldProductsFile(){
        step("Проверяем отображение ошибки "+ErrorsMsg.obligatory_field_merchant.getErrorMsg()+" под полем Товарный ассортимент", () -> {
            assertEquals("Ошибка должна отображаться под полем Товарный ассортимент.", ErrorsMsg.obligatory_field_merchant.getErrorMsg(),regPage.ProductsFileErrorMsg.getText());
        });
    }
    public void stepCheckErrorMaximumLengthFieldName(){
        step("Проверяем отображение ошибки "+ErrorsMsg.maximum_length__field.getErrorMsg()+" под полем Имя ", () -> {
            assertEquals("Ошибка должна отображаться под полем Имя.", ErrorsMsg.maximum_length__field.getErrorMsg(), regPage.NameErrorMsg.getText());
        });
    }
    public void stepCheckErrorMaximumLengthFieldSurname(){
        step("Проверяем отображение ошибки "+ErrorsMsg.maximum_length__field.getErrorMsg()+" под полем Фамилия ", () -> {
            assertEquals("Ошибка должна отображаться под полем Фамилия.", ErrorsMsg.maximum_length__field.getErrorMsg(), regPage.SurnameErrorMsg.getText());
        });
    }
    public void stepCheckErrorMaximumLengthFieldMiddleName(){
        step("Проверяем отображение ошибки "+ErrorsMsg.maximum_length__field.getErrorMsg()+" под полем Отчество ", () -> {
            assertEquals("Ошибка должна отображаться под полем Отчество.", ErrorsMsg.maximum_length__field.getErrorMsg(), regPage.MiddleNameErrorMsg.getText());
        });
    }

    public void stepCheckErrorIncorrectEmailFieldEmail(){
        step("Проверяем отображение ошибки "+ErrorsMsg.incorrect_email_merchant.getErrorMsg()+" под полем E-mail" , () -> {
            assertEquals("Ошибка должна отображаться.", ErrorsMsg.incorrect_email_merchant.getErrorMsg(), regPage.EmailErrorMsg.getText());
        });
    }
    public void stepCheckErrorExistEmailFieldEmail(){
        step("Проверяем отображение ошибки "+ErrorsMsg.exist_email_merchant.getErrorMsg()+" под полем E-mail" , () -> {
            assertEquals("Ошибка должна отображаться.", ErrorsMsg.exist_email_merchant.getErrorMsg(), regPage.EmailErrorMsg.getText());
        });
    }
    public void stepCheckErrorIncorrectNumberFieldPhone(){
        step("Проверяем отображение ошибки "+ErrorsMsg.invalid_number_merchant.getErrorMsg()+" под полем Телефон" , () -> {
            assertEquals("Ошибка должна отображаться.", ErrorsMsg.invalid_number_merchant.getErrorMsg(), regPage.PhoneErrorMsg.getText());
        });
    }
    public void stepCheckErrorIncorrectFormatFieldPhone(){
        step("Проверяем пустое отображение поля Телефон" , () -> {
            assertEquals("Поле должно быть пустое","", regPage.PhoneErrorMsg.getText());
        });
    }
    public void stepCheckErrorOnlyDigitsFieldInn(){
        step("Проверяем отображение ошибки "+ErrorsMsg.incorrect_inn_merchant.getErrorMsg()+" под полем Inn" , () -> {
            assertEquals("Ошибка должна отображаться.", ErrorsMsg.incorrect_inn_merchant.getErrorMsg(), regPage.InnErrorMsg.getText());
        });
    }
    public void stepCheckErrorIncorrectInnFieldInn(){
        step("Проверяем отображение ошибки "+ErrorsMsg.incorrect_inn_merchant.getErrorMsg()+" под полем Inn" , () -> {
            assertEquals("Ошибка должна отображаться.", ErrorsMsg.incorrect_inn_merchant.getErrorMsg(), regPage.InnErrorMsg.getText());
        });
    }




}
