package steps.UI;

import com.codeborne.selenide.SelenideElement;
import env.Base_Container;
import testPageLocator.general.GeneralLocators;
import java.io.File;

import static com.codeborne.selenide.Condition.disappear;
import static io.qameta.allure.Allure.step;
import static org.testng.AssertJUnit.*;


public class GeneralSteps extends Base_Container {
    GeneralLocators generalLocators;

    public GeneralSteps loginAdminPage(String email, String password){
        generalLocators.EmailField.setValue(email);
        generalLocators.PasswordField.setValue(password);
        generalLocators.SigInBtn.click();
        generalLocators.PageLogin.should(disappear);
        return this;
    }
    // Загрузка файла
    public void stepUploadFile(String name, SelenideElement inputFile, SelenideElement successElement) {
        step(name, () -> {
            inputFile.uploadFile(new File("src/test/resources/files/testFile.xlsx"));
            Thread.sleep(3000);
            assertTrue( "Отображается успешная загрузка файла.",successElement.exists());
        });

    }
    public void stepCheckLinkFile(String name, SelenideElement successElement) {
        step(name, () -> {
            assertTrue( "Отображается успешная загрузка файла.",successElement.exists());
        });

    }

}
