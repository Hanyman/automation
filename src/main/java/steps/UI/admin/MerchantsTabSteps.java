package steps.UI.admin;

import com.codeborne.selenide.SelenideElement;
import constants.ErrorsMsg;
import env.Base_Container;
import io.qameta.allure.Step;
import org.testng.Assert;
import testPageLocator.admin.MerchantsPage.FormCreateNewMerchant;
import testPageLocator.admin.MerchantsPage.PageCardMerchants;
import testPageLocator.admin.MerchantsPage.PageMerchants;

import static com.codeborne.selenide.Condition.disappear;
import static constants.data.OrganizationInfoMerchant.*;
import static io.qameta.allure.Allure.step;
import static org.testng.AssertJUnit.assertEquals;

public class MerchantsTabSteps extends Base_Container {

    FormCreateNewMerchant formCreateNewMerchant;
    PageMerchants pageMerchants;
    PageCardMerchants pageCardMerchants;

    @Step("Заполняем все поля для создания нового мерчанта и нажимаем кнопку Создать")
    public MerchantsTabSteps createMerchant(String inn,String kpp, String orgForm, String orgName, String paymentAccount, String name,
                               String phone, String email, String comMethod, String password, String site){
        formCreateNewMerchant.InnField.setValue(inn);
        formCreateNewMerchant.KppField.setValue(kpp);
        formCreateNewMerchant.OrganizationFormSelect.selectOption(orgForm);
        formCreateNewMerchant.OrganizationField.setValue(orgName);
        formCreateNewMerchant.LegalAddressField.setValue(GENERAL_ADDRESS);
        formCreateNewMerchant.FactAddressField.setValue(GENERAL_ADDRESS);
        formCreateNewMerchant.PaymentAccountField.setValue(paymentAccount);
        formCreateNewMerchant.BankField.setValue(BANK);
        formCreateNewMerchant.SurnameField.setValue("TestSurname_"+ name);
        formCreateNewMerchant.NameField.setValue("TestName_"+ name);
        formCreateNewMerchant.MiddleNameField.setValue("TestMiddleName_"+ name);
        formCreateNewMerchant.PhoneField.setValue(phone);
        formCreateNewMerchant.EmailField.setValue(email);
        formCreateNewMerchant.CommunicationMethodSelect.selectOption(comMethod);
        formCreateNewMerchant.PasswordField.setValue(password);
        formCreateNewMerchant.PasswordConfirmField.setValue(password);
        formCreateNewMerchant.SiteField.setValue(site);
        formCreateNewMerchant.StorageAddressField.setValue(GENERAL_ADDRESS);
        formCreateNewMerchant.SaleField.setValue("Any brands!!!//.#$%");
        formCreateNewMerchant.SaveBtn.click();

        return this;
    }
    @Step("Проверка отображения сообщения под каждым обязательным полем формы создания нового мерчанта.")
    public MerchantsTabSteps checkErrorObligatoryFields(){
        stepCheckErrorObligatoryField(formCreateNewMerchant.InnErrorMsg,"ИНН");
        stepCheckErrorObligatoryField(formCreateNewMerchant.OrganizationErrorMsg,"Название организации");
        stepCheckErrorObligatoryField(formCreateNewMerchant.LegalAddressErrorMsg,"Юридический адресс");
        stepCheckErrorObligatoryField(formCreateNewMerchant.FactAddressErrorMsg,"Фактический адрес");
        stepCheckErrorObligatoryField(formCreateNewMerchant.PaymentAccountErrorMsg,"Номер банковского счета");
        stepCheckErrorObligatoryField(formCreateNewMerchant.BankErrorMsg,"Название банка");
        stepCheckErrorObligatoryField(formCreateNewMerchant.SurnameErrorMsg,"Фамилия");
        stepCheckErrorObligatoryField(formCreateNewMerchant.NameErrorMsg,"Имя");
        stepCheckErrorObligatoryField(formCreateNewMerchant.PhoneErrorMsg,"Номер телефона");
        stepCheckErrorObligatoryField(formCreateNewMerchant.EmailErrorMsg,"Email");
        stepCheckErrorObligatoryField(formCreateNewMerchant.CommunicationMethodErrorMsg,"Способ связи");
        stepCheckErrorObligatoryField(formCreateNewMerchant.PasswordErrorMsg,"Пароль");
        stepCheckErrorObligatoryField(formCreateNewMerchant.PasswordConfirmErrorMsg,"Подтверждение пароля");
        stepCheckErrorObligatoryField(formCreateNewMerchant.StorageAddressErrorMsg,"Адреса складов отгрузки");
        stepCheckErrorObligatoryField(formCreateNewMerchant.SiteErrorMsg,"Сайт компании");
        stepCheckErrorObligatoryField(formCreateNewMerchant.SaleErrorMsg,"Бренды которыми торгуете");
        return this;
    }

    @Step("Нажимаем на кнопку - Создать мерчанта.")
    public MerchantsTabSteps clickCreateMerchantBtn(){
        pageMerchants.CreateMerchantBtn.click();
        return this;
    }

    @Step("Нажимаем на кнопку - Сохранить.")
    public MerchantsTabSteps clickSaveBtn(){
        formCreateNewMerchant.SaveBtn.click();
        return this;
    }

    @Step("Ожидание недоступности иконки загрузки.")
    public MerchantsTabSteps waitDisappearLoader(){
        formCreateNewMerchant.loader.should(disappear);
        return this;
    }
    @Step("Ожидание недоступности формы создания нового мерчанта.")
    public MerchantsTabSteps waitDisappearCreateNewMerchForm(){
        formCreateNewMerchant.CreateNewMerchForm.should(disappear);
        return this;
    }

    @Step("Заполнение поля email.")
    public MerchantsTabSteps inputEmailField(String value){
        formCreateNewMerchant.EmailField.setValue(value);
        return this;
    }
    @Step("Нажимаем кнопку Войти под мерчантом.")
    public MerchantsTabSteps clickLogInAsMerchant(){
        formCreateNewMerchant.LogInAsMerchantBtn.scrollTo().click();
        return this;
    }
    @Step("Проверка ошибки под полем email.")
    public MerchantsTabSteps checkEmailErrorField(String expected){
        Assert.assertEquals(formCreateNewMerchant.EmailErrorMsg.getText(),expected,"Ошибка должна отображаться под полем email.");
        return this;
    }

    @Step("Заполнение поля ИНН.")
    public MerchantsTabSteps inputInnField(String value){
        formCreateNewMerchant.InnField.setValue(value);
        return this;
    }
    @Step("Проверка ошибки под полем ИНН.")
    public MerchantsTabSteps checkInnErrorField(String expected){
        Assert.assertEquals(formCreateNewMerchant.InnErrorMsg.getText(),expected,"Ошибка должна отображаться под полем inn.");
        return this;
    }
    @Step("Заполнение поля kpp.")
    public MerchantsTabSteps inputKppField(String value){
        formCreateNewMerchant.KppField.setValue(value);
        return this;
    }
    @Step("Проверка ошибки под полем KPP.")
    public MerchantsTabSteps checkKppErrorField(String expected){
        Assert.assertEquals(formCreateNewMerchant.KppErrorMsg.getText(),expected,"Ошибка должна отображаться под полем kpp.");
        return this;
    }

    @Step("Заполнение поля BIk.")
    public MerchantsTabSteps inputBikField(String value){
        formCreateNewMerchant.BankBikField.setValue(value);
        return this;
    }
    @Step("Проверка ошибки под полем Bik.")
    public MerchantsTabSteps checkBikErrorField(String expected){
        Assert.assertEquals(formCreateNewMerchant.BankBikErrorMsg.getText(),expected,"Ошибка должна отображаться под полем Bik.");
        return this;
    }

    @Step("Заполнение поля Номер банковского счета.")
    public MerchantsTabSteps inputPaymentAccountField(String value){
        formCreateNewMerchant.PaymentAccountField.setValue(value);
        return this;
    }
    @Step("Проверка ошибки под полем Номер банковского счета.")
    public MerchantsTabSteps checkPaymentAccountErrorField(String expected){
        Assert.assertEquals(formCreateNewMerchant.PaymentAccountErrorMsg.getText(),expected,"Ошибка должна отображаться под полем Номер банковского счета.");
        return this;
    }

    @Step("Заполнение поля Номер корреспондентского счета.")
    public MerchantsTabSteps inputCorrespondentAccountField(String value){
        formCreateNewMerchant.CorrespondentAccountField.setValue(value);
        return this;
    }
    @Step("Проверка ошибки под полем Номер корреспондентского счета.")
    public MerchantsTabSteps checkCorrespondentAccountErrorField(String expected){
        Assert.assertEquals(formCreateNewMerchant.CorrespondentAccountErrorMsg.getText(),expected,"Ошибка должна отображаться под полем Номер корреспондентского счета.");
        return this;
    }
    @Step("Заполнение поля Телефон.")
    public MerchantsTabSteps inputPhoneField(String value){
        formCreateNewMerchant.PhoneField.setValue(value);
        return this;
    }
    @Step("Проверка ошибки под полем Телефон.")
    public MerchantsTabSteps checkPhoneErrorField(String expected){
        Assert.assertEquals(formCreateNewMerchant.PhoneErrorMsg.getText(),expected,"Ошибка должна отображаться под полем Телефон.");
        return this;
    }
    @Step("Заполнение поля Повтор пароля.")
    public MerchantsTabSteps inputPasswordConfirmField(String value){
        formCreateNewMerchant.PasswordConfirmField.setValue(value);
        return this;
    }
    @Step("Проверка ошибки под полем Повтор пароля.")
    public MerchantsTabSteps checkPasswordConfirmErrorField(String expected){
        Assert.assertEquals(formCreateNewMerchant.PasswordConfirmErrorMsg.getText(),expected,"Ошибка должна отображаться под полем Повтор пароля.");
        return this;
    }

    @Step("Проверка наличия организации в списке мерчантов.")
    public MerchantsTabSteps verifyNameMerchantInList(String expectedResult){
        verifyText(pageMerchants.NameMerchantColFirst,expectedResult,"Имя организации должна отображаться в таблице.");
        return this;
    }

    @Step("Проверка статуса организации в списке мерчантов.")
    public MerchantsTabSteps verifyStatusMerchantInList(String expectedResult){
        verifyText(pageMerchants.StatusMerchantColFirst,expectedResult,"Статус организации должнен отображаться в таблице.");
        return this;
    }
    @Step("Проверка залоговка организации в карточки мерчанта.")
    public MerchantsTabSteps verifyTitleMerchantInCard(String expectedResult){
        verifyText(pageCardMerchants.TitleCard,expectedResult,"Имя организации должна отображаться в карточке.");
        return this;
    }

    @Step("Проверка статуса организации в карточке мерчанта.")
    public MerchantsTabSteps verifyStatusMerchantInCard(String expectedResult){
        verifyText(pageCardMerchants.StatusMerchant,expectedResult,"Статус организации должнен отображаться в карточке.");
        return this;
    }

    public void stepCheckErrorObligatoryField(SelenideElement element, String fieldName){
        Assert.assertEquals(element.getText(),ErrorsMsg.obligatory_field_merchant.getErrorMsg(),"Ошибка должна отображаться под полем.");
    }

    public void verifyText(SelenideElement element, String expected, String msg){
        Assert.assertEquals(element.getText(),expected,msg);
    }


}
