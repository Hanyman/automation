package steps.api.merchant;

import env.Base_api;
import io.qameta.allure.Step;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import steps.api.admin.OrdersAdminApiSteps;
import steps.api.market.CartApiSteps;
import steps.api.market.ReturnsApiSteps;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import static constants.end_point.MerchantPanelEndpoint.shipment_list;
import static helpers.api.ApiHelper.clearQuery;
import static io.qameta.allure.Allure.step;
import static io.restassured.RestAssured.given;

public class OrdersMerchApiSteps extends Base_api {
    CommonMerchantApiSteps commonApiSteps = new CommonMerchantApiSteps();
    CartApiSteps cartApiSteps = new CartApiSteps();
    OrdersAdminApiSteps adminOrderApiSteps = new OrdersAdminApiSteps();
    ReturnsApiSteps returnsApiSteps = new ReturnsApiSteps();
    int idOrderMerch;
    String authorizationToken;
    public int getIdOrder(RequestSpecification merchSpec, String idOrder) {
        //1000160-1-01
        step("Получить id заказа в списке заказов мерчанта по номеру заказа" , () -> {
            Response response = given()
                    .spec(merchSpec)
                    .get(shipment_list.path());
            steps.checkResponseWithStatusCodeAndLogView(response, 200);
           idOrderMerch = Integer.parseInt(commonApiSteps.getStrInfoMerchFromHtml(response,"$..iShipments[?(@.number =~ /^"+idOrder+".*/i)].id"));

        });
        return idOrderMerch;
    }

    public String getTokenForOrder(RequestSpecification merchSpec) {

        step("Получить данные по мерчанту из html body" , () -> {

            Response response = given()
                    .spec(merchSpec)
                    .get(shipment_list.path());
            steps.checkResponseWithStatusCodeAndLogView(response, 200);

            authorizationToken =  commonApiSteps.getAuthToken(response);
        });
        return authorizationToken;

    }

    public void changeStatusOrder(RequestSpecification merchSpec, int idOrder, String status, String authToken) {

        step("Изменение статуса заказа Все товары в наличии", () -> {
            Response response = given()
                    .spec(merchSpec)
                    .header("authorization", authToken)
                    .header("accept", ContentType.JSON)
                    .body("{\"status\":"+status+"}")
                    .put(shipment_list.apiChangeStatusOrder(idOrder));

            steps.checkResponseWithStatusCodeAndLogView(response, 200);
        });

    }

    public Map<String, String> addShipmentPackage(RequestSpecification merchSpec, int idOrderMerch, String authToken) {
        Map<String, String> ids = new HashMap<String, String>();
        step("Добавление упаковки", () -> {
           // ReqAddShipmentPackage body = ReqAddShipmentPackage.builder().jsonMemberPackage(JsonMemberPackage.builder().
            //        packageId(1).height(null).weight(10).length(null).width(null).build()).build();
            String body = "{\n" +
                    "\"package\":{\n" +
                    "\"package_id\":1,\n" +
                    "\"height\":null,\n" +
                    "\"width\":null,\n" +
                    "\"length\":null,\n" +
                    "\"weight\":100\n" +
                    "}\n" +
                    "}";
            Response response = given()
                    .spec(merchSpec)
                    .header("authorization", authToken)
                    .header("accept", ContentType.JSON)
                    .body(body)
                    .post(shipment_list.apiAddShipmentPackageOrder(idOrderMerch));

            steps.checkResponseWithStatusCodeAndLogView(response, 200);
            String idPackage = clearQuery(response.jsonPath().getString("shipment.packages[0].id"));
            String idBasket = clearQuery(response.jsonPath().getString("shipment.items.basket_item_id"));

            ids.put("idPackage", idPackage);
            ids.put("idBasket", idBasket);
        });

        return ids;
    }

    public void addProductsToPackage(RequestSpecification merchSpec, int idOrderMerch, int idPackage, int idBasket,int count, String authToken) {

        step("addProductsToPackage", () -> {
            Response response = given()
                    .spec(merchSpec)
                    .header("authorization", authToken)
                    .header("accept", ContentType.JSON)
                    .body("{\"basketItems\":{\""+idBasket+"\":"+count+"}}")
                    .post(shipment_list.apiAddShipmentPackageItems(idOrderMerch, idPackage));

            steps.checkResponseWithStatusCodeAndLogView(response, 200);
        });

    }
    public void getBarcodes(RequestSpecification merchSpec, int idOrderMerch, String authToken) {

        step(" Отправка запроса на получение штрих кодов", () -> {
            Response response = given()
                    .spec(merchSpec)
                    .header("authorization", authToken)
                    .header("accept", ContentType.JSON)
                    .get(shipment_list.apiGetBarcodes(idOrderMerch));

            steps.checkResponseWithStatusCodeAndLogView(response, 200);
        });

    }

    @Step("Создаем новый заказ, оплачиваем и доставляем его(если такого заказа нет в списке)")
    public int createNewOrder(RequestSpecification marketSpec, int idProduct, int count, String paymentMethod, String alwaysNew){
        String auth, numbOrder;
        int idMerchOrder, idOrder,numbOrderForReturn=0;
        Map<String, String> idsOrder;

        ArrayList<Integer> listOrder = returnsApiSteps.getListOrders(marketSpec);

        if(listOrder.size()!=0){
            numbOrderForReturn= listOrder.get(0);
        }
        if(alwaysNew.contains("yes") || listOrder.size()==0){

            cartApiSteps.clearCartApi(marketSpec);
            cartApiSteps.addProductsInCartApi(marketSpec, idProduct, count, generalMerchConfig.getStore());
            String bodyForOrder = cartApiSteps.checkDataCheckout(marketSpec, paymentMethod);
            idsOrder = cartApiSteps.checkoutCommit(marketSpec, bodyForOrder);
            idOrder = Integer.parseInt(idsOrder.get("idOrder"));
            numbOrder = idsOrder.get("numberOrder");
            Map<String, String> ids;
            auth = getTokenForOrder(merchantSpec);
            adminOrderApiSteps.payOrder(idOrder);
            idMerchOrder = getIdOrder(merchantSpec,numbOrder);
            changeStatusOrder(merchantSpec,idMerchOrder, "5", auth);
            ids = addShipmentPackage(merchantSpec,idMerchOrder, auth);
            addProductsToPackage(merchantSpec,idMerchOrder, Integer.parseInt(ids.get("idPackage")), Integer.parseInt(ids.get("idBasket")), count, auth);
            changeStatusOrder(merchantSpec, idMerchOrder, "6", auth);
            getBarcodes(merchantSpec,idMerchOrder, auth);
            adminOrderApiSteps.setStatusOrderDelivered(idOrder,numbOrder);
            numbOrderForReturn =idOrder;
        }
        return numbOrderForReturn;

    }

    @Step("Создаем новый заказ от разных мерчантов, оплачиваем и доставляем его.")
    public Map<String, String> createNewOrderFewMerchants(RequestSpecification marketSpec, int idProductFirst, int idProductSecond, int count, String paymentMethod){
        int idOrder;
        Map<String, String> idsOrder;
        cartApiSteps.clearCartApi(marketSpec);
        cartApiSteps.addProductsInCartApi(marketSpec, idProductFirst, count, generalMerchConfig.getStore());
        cartApiSteps.addProductsInCartApi(marketSpec, idProductSecond, count, secondMerchConfig.getStore());
        String bodyForOrder = cartApiSteps.checkDataCheckout(marketSpec, paymentMethod);
        idsOrder = cartApiSteps.checkoutCommit(marketSpec, bodyForOrder);
        idOrder = Integer.parseInt(idsOrder.get("idOrder"));
        adminOrderApiSteps.payOrder(idOrder);

        return idsOrder;

    }
    @Step("Собираем и отгружаем заказ под мерчантом.")
    public int assemblyOrder(RequestSpecification merchSpec, Map<String, String> idsOrder, int count){
        String auth;
        int idMerchOrder, idOrder,numbOrderForReturn=0;
        idOrder = Integer.parseInt(idsOrder.get("idOrder"));
        Map<String, String> ids;
        auth = getTokenForOrder(merchSpec);
        idMerchOrder = getIdOrder(merchSpec,idsOrder.get("numberOrder"));
        changeStatusOrder(merchSpec,idMerchOrder, "5", auth);
        ids = addShipmentPackage(merchSpec,idMerchOrder, auth);
        addProductsToPackage(merchSpec,idMerchOrder, Integer.parseInt(ids.get("idPackage")), Integer.parseInt(ids.get("idBasket")), count, auth);
        changeStatusOrder(merchSpec,idMerchOrder, "6", auth);
        getBarcodes(merchSpec,idMerchOrder, auth);
        numbOrderForReturn =idOrder;

        return numbOrderForReturn;

    }

}
