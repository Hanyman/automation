package steps.api.merchant;

import api_model.request.merchant.addInfoMerchant.ReqAddInfoMerchant;
import api_model.request.merchant.registration.Assortment;
import api_model.request.merchant.registration.ReqRegistration;
import api_model.response.merchant.checkEmail.RespCheckEmail;
import api_model.response.merchant.common.uploadFile.RespUpload;
import com.jayway.jsonpath.JsonPath;
import env.Base_api;
import helpers.Generator;
import io.restassured.response.Response;
import junit.framework.Assert;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;

import static constants.end_point.AdminPanelEndpoint.merchant_confirm_email_link;
import static constants.end_point.AdminPanelEndpoint.merchant_detail;
import static constants.end_point.MerchantPanelEndpoint.*;
import static constants.merchant.TypeOrganization.IP;
import static constants.merchant.TypeOrganization.OOO;
import static helpers.api.ApiHelper.*;
import static io.qameta.allure.Allure.addLinks;
import static io.qameta.allure.Allure.step;
import static io.restassured.RestAssured.given;

public class ActivationMerchantApiSteps extends Base_api {
    boolean existEmail;
    int idMerchant;
    String link,linkResult, authorizationToken;
    String codeMerchant, legalNameMerchant, innMerchant, createdAtMerchant;
    CommonMerchantApiSteps commonApiSteps = new CommonMerchantApiSteps();
    Generator generator = new Generator();
    // Метод для  регистрации и активации мерчанта с нуля( регистрация, активация, заполнение данных, полный доступ)

    public void registrationAndActivateMerchant(int typeOrg) throws MalformedURLException, UnsupportedEncodingException {
        //registrationMerchant(String.valueOf(typeOrg));
       // setPassword();
       // getMerchant();
       // changeStatusMerchantOnActivation();
       // addInfoMerchant(typeOrg);
       // changeStatusMerchantOnAFullCheck(typeOrg);
     //   getMerchantCheckStatus();
    }

    public boolean checkExistEmail(String email) {
        step("Проверка существования email при регистрации в mas", () -> {
            Response response = given()
                    .spec(merchantSpec)
                    .param("email",email)
                    .get(registration_user_exist.path());

            RespCheckEmail exEmail = steps.checkResponseWithStatusCodeAndLogView(response, 200).as(RespCheckEmail.class);
            existEmail = exEmail.isExists();
        });
        return  existEmail;
    }

    public void registrationMerchant(String organizationForm,String nameCompany,String firstName, String lastName,
                                     String middleNAme, String inn, String phoneNumb, String email) {
        step("Регистрация нового мерчанта в mas", () -> {

            RespUpload file = commonApiSteps.upload("merchantAssortment", FILE_XLSX);
            ReqRegistration body = ReqRegistration.builder().legal_name(nameCompany).first_name(firstName).last_name(lastName).middle_name(middleNAme)
                    .phone(phoneNumb).inn(inn).email(email).can_integration(false).storage_address("Mosk")
                    .assortment(Assortment.builder().id(file.getId()).name(file.getName()).url(file.getUrl()).build())
                    .communication_method("").bank_address("").organization_form(organizationForm).kpp("").sale_info("").bank("").password("").site("").bank_bik("")
                    .payment_account("").fact_address("").legal_address("").password_confirmation("").correspondent_account("").build();

            Response response = given()
                    .spec(merchantSpec)
                    .body(body)
                    .post(registration.path());

            steps.checkResponseWithStatusCodeAndLogView(response, 200);
        });
    }

    public void getMerchant() {
        step("Получить данные по мерчанту из html body" , () -> {

            Response response = given()
                    .spec(merchSpecOnlyCookies)
                    .get("/merchant");
            steps.checkResponseWithStatusCodeAndLogView(response, 200);

            idMerchant = getIdOnHtml(response);
            codeMerchant = getStrInfoMerchFromHtml(response,"$..iMerchant.code");
            legalNameMerchant = getStrInfoMerchFromHtml(response,"$..iMerchant.legal_name");
            innMerchant = getStrInfoMerchFromHtml(response,"$..iMerchant.inn");
            createdAtMerchant = getStrInfoMerchFromHtml(response,"$..iMerchant.created_at");

        });
    }

    public void getMerchantCheckStatus(String status) {
        step("Проверка статуса мерчанта 7-  (Действует)" , () -> {

            Response response = given()
                    .spec(merchSpecOnlyCookies)
                    .get("/merchant");
            steps.checkResponseWithStatusCodeAndLogView(response, 200);

            Assert.assertEquals("Status merchant must be full check",status,getStrInfoMerchFromHtml(response,"$..iMerchant.status"));

        });
    }

    public void changeStatusMerchantOnActivation(String status) {
        step("Изменение статуса мерчанта на - Активация" , () -> {

            Response response = given()
                    .spec(adminSpec)
                    .queryParam("merchant[status]",status)
                    .post(merchant_detail.pathActivateMerchant(idMerchant));

            steps.checkResponseWithStatusCodeAndLogView(response, 204);

        });
    }
    public void changeStatusMerchantOnAFullCheck(int typeOrg, String status) {
        step("Изменение статуса мерчанта на - Действует" , () -> {

            Response response = given()
                    .spec(adminSpec)
                    .queryParam("merchant[organization_form]",typeOrg)
                    .queryParam("merchant[legal_name]",legalNameMerchant)
                    .queryParam("merchant[status]",status)
                    .post(merchant_detail.pathActivateMerchant(idMerchant));

            steps.checkResponseWithStatusCodeAndLogView(response, 204);

        });
    }

    public void addInfoMerchant(int organizationForm) {
        step("Добавление информации по мерчанту ООО + загрузка файлов" , () -> {

            if (organizationForm == OOO.getTypeOrg()) {
                // Добавление пдф файлов к мерчанту, кроме файла товарный ассортимент - он был добавлен при регистрации
                // uploadFileInfoMerchant("Товарный ассортимент","1");
                uploadFileInfoMerchant("Свидетельство ИНН", "2");
                uploadFileInfoMerchant("Свидетельство ОГРН", "3");
                uploadFileInfoMerchant("Лист записи ЕГРЮЛ", "5");
                uploadFileInfoMerchant("Устав компании с отметками налогового органа", "6");
                uploadFileInfoMerchant("Решение или приказ о назначении генерального директора", "7");
            }if(organizationForm == IP.getTypeOrg()){
                // Добавление пдф файлов к мерчанту, кроме файла товарный ассортимент - он был добавлен при регистрации
                // uploadFileInfoMerchant("Товарный ассортимент","1");
                uploadFileInfoMerchant("Свидетельство ИНН", "2");
                uploadFileInfoMerchant("Свидетельство ОГРНИП", "4");
                uploadFileInfoMerchant("Паспорт", "8");

            }

            // Запрос на добавление инфы по мерчанту - отправка на проверку админам
            ReqAddInfoMerchant body = ReqAddInfoMerchant.builder().bank("ПАО Сбербанк").bankAddress("г Москва, ул Вавилова, д 19").bankBik("044525225")
                    .correspondentAccount("30101810400000000225").ceoFirstName(generator.getFirstName()).ceoLastName(generator.getLastName()).ceoMiddleName(generator.getMiddleName())
                    .code(codeMerchant).comment("Тестовое примечание").factAddress("107078, г Москва, Красносельский р-н, пр-кт Академика Сахарова, д 4").id(idMerchant).inn(innMerchant)
                    .kpp(generator.kpp).legalAddress("107078, г Москва, Красносельский р-н, пр-кт Академика Сахарова, д 4")
                    .legalName(legalNameMerchant).organizationForm(organizationForm).paymentAccount(generator.bank_account).priceModerate(true).ratingId(1).build();

            Response response = given()
                    .spec(merchantSpec)
                    .header("authorization", authorizationToken)
                    .body(body)
                    .redirects().follow(false)
                    .put(merchant.pathMerchant(idMerchant));

            steps.checkResponseWithStatusCodeAndLogView(response, 302);

        });
    }

    public void uploadFileInfoMerchant(String nameFile, String typeId) {
        step("Загрузка файла " + nameFile + " для мерчанта -" + idMerchant, () -> {
            RespUpload file = commonApiSteps.upload("merchant"+idMerchant,FILE_PDF, authorizationToken);
            int idFile = file.getId();

            Response response = given()
                    .spec(merchantSpec)
                    .header("authorization", authorizationToken)
                    .queryParam("file_id", idFile)
                    .queryParam("type", typeId)
                    .post(merchant_docs_create.path());

            steps.checkResponseWithStatusCodeAndLogView(response,201 );

        });
    }

    public String getLinkEmail(String emailUser) throws MalformedURLException, UnsupportedEncodingException {
        step("Получение ссылки на установку пароля из email", () -> {
            Response response = given()
                    .baseUri(url_admin)
                    .queryParam("token",TOKEN_EMAIL)
                    .queryParam("email",emailUser)
                    .post(merchant_confirm_email_link.path());

            steps.checkResponseWithStatusCodeAndLogView(response, 200);
            link = getParamJson(JsonPath.read(response.getBody().asString(),"$..link").toString());

        });
        String getUrlLink = getClearUrl(link);
        String userid = String.valueOf(getParamUrl(new URL(getUrlLink)).get("user_id").get(0));
        String token = String.valueOf(getParamUrl(new URL(getUrlLink)).get("token").get(0));
        String resultLink = "/confirm-email?user_id="+userid+"&token="+token+"";
        return resultLink;
    }

    public void setPassword(String email) throws MalformedURLException, UnsupportedEncodingException {
        // Получаем ссылку на активацию пароля из письма
        linkResult = getLinkEmail(email);

        step("Установка пароля для мерчанта", () -> {
            // Переходим по ссылке которая редиректит, в итоге в Location у нас ссылка для след. запроса
            Response response = given()
                    //.baseUri(url_merchant)
                    .spec(merchSpecOnlyCookies)
                    .redirects().follow(false)
                    .get( linkResult);
            steps.checkResponseWithStatusCodeAndLogView(response, 302);
            String headerLocationValue = response.getHeader("Location");
            int idMerch = GetIdMerchant(headerLocationValue);
            // Делаем запрос на получение страницы с установкой пароля, из тела выдираем токен для авторизации всех послед. запросов
            Response response2 = given()
                    .spec(merchSpecOnlyCookies)
                    .get("/merchant/managers/"+idMerch+"/password");
            steps.checkResponseWithStatusCodeAndLogView(response2, 200);

            authorizationToken = "Bearer " + getAuthToken(response2);

            // Непосредственно сам запрос на установку пароля с токеном для авторизации
            Response response3 = given()
                    .spec(merchantSpec)
                    .header("authorization",authorizationToken)
                    .body("{\"id\":"+idMerch+",\"password\":\"12345678\"}")
                    .put(merchant_managers.pathMerchantManagers(idMerch));
            steps.checkResponseWithStatusCodeAndLogView(response3, 204);

        });

    }


    // получение токена из странцы html
    private String getAuthToken(Response response){
        Document doc = Jsoup.parse(response.getBody().asString());
        Element div = doc.getElementById("app");
        String getAttr = div.attr("data-env");
        String authToken = getParamJson(JsonPath.read(getAttr,"$..token").toString());
        return  authToken;
    }

    // Получение id мерчанта (инт)
    private int getIdOnHtml(Response response){
        Document doc = Jsoup.parse(response.getBody().asString());
        Element div = doc.getElementById("app");
        String getAttr = div.attr("data-props");
        String idMerch = getParamJson(JsonPath.read(getAttr,"$..iMerchant.id").toString());
        return Integer.parseInt(clearQuery(idMerch));
    }

    // Получение даных мерчанта из html
    private String getStrInfoMerchFromHtml(Response response, String jsonPath){
        Document doc = Jsoup.parse(response.getBody().asString());
        Element div = doc.getElementById("app");
        String getAttr = div.attr("data-props");
        String info = getParamJson(JsonPath.read(getAttr,jsonPath).toString());
        return clearQuery(info);
    }
}
