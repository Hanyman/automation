package steps.api.merchant;

import api_model.request.merchant.login.LoginMerchant;
import env.Base_api;
import io.qameta.allure.Description;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;

import static constants.end_point.MerchantPanelEndpoint.*;
import static io.qameta.allure.Allure.step;
import static io.restassured.RestAssured.given;

public class LoginMerchantApiSteps extends Base_api {

    public void loginMerchantPanel(RequestSpecification merchSpec, String loginEmail, String password) {
        step("Логинимся под менеджером в mas", () -> {
            LoginMerchant login = LoginMerchant.builder().email(loginEmail).password(password).build();

            Response response = given()
                    .spec(merchSpec)
                    .body(login)
                    .post(login_merchant.path());

            steps.checkResponseWithStatusCodeAndLogView(response, 200);
        });
    }

    @Description("Логаут пользователя из MAS.")
    public void logoutMerchant(RequestSpecification spec) {

            Response response = given()
                    .spec(spec)
                    .body("{}")
                    .post(logout_merchant.path());

            steps.checkResponseWithStatusCodeAndLogView(response, 200);
    }

}
