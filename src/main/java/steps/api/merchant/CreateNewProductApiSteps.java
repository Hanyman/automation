package steps.api.merchant;

import api_model.request.merchant.createNewProduct.createProduct.ReqCreateProduct;
import api_model.request.merchant.createNewProduct.saveImage.ReqSaveImage;
import api_model.request.merchant.createNewProduct.saveOfferPrice.ReqSaveOfferPrice;
import api_model.request.merchant.createNewProduct.saveProduct.ReqSaveProduct;
import api_model.request.merchant.createNewProduct.saveStocks.ReqSaveStocks;
import api_model.request.merchant.createNewProduct.saveStocks.Stocks;
import api_model.response.merchant.common.uploadFile.RespUpload;
import api_model.response.merchant.createNewProduct.createProduct.RespCreateProduct;
import api_model.response.merchant.createNewProduct.saveImage.RespSaveImage;
import api_model.response.merchant.createNewProduct.saveOfferPrice.RespSaveOfferPrice;
import api_model.response.merchant.createNewProduct.saveProduct.RespSaveProduct;
import api_model.response.merchant.createNewProduct.saveStocks.RespSaveStocks;
import com.jayway.jsonpath.JsonPath;
import env.Base_api;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import steps.api.admin.ApproveProductApiSteps;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

import static constants.end_point.MerchantPanelEndpoint.*;
import static io.qameta.allure.Allure.step;
import static io.restassured.RestAssured.given;

public class CreateNewProductApiSteps extends Base_api {
    String idProductCheck, authorizationToken;
    int idImage;
    ApproveProductApiSteps approveProductApiSteps = new ApproveProductApiSteps();
    CommonMerchantApiSteps commonApiSteps = new CommonMerchantApiSteps();
    public void approveProduct(int idProduct){

        approveProductApiSteps.approveProduct(idProduct,idProductCheck);
    }

    public String getTokenForCreateProduct(){
        step("Получить токен для создания товара", () -> {
        // Делаем запрос на получение страницы с созданием товара, и из тела выдираем токен для авторизации всех послед. запросов
        Response response = given()
                .spec(merchantSpec)
                .get(create.path());
        steps.checkResponseWithStatusCodeAndLogView(response, 200);

        authorizationToken =  commonApiSteps.getAuthToken(response);
        });
        return authorizationToken;
    }

    public Map<String, String> createProduct(String code, String nameProduct, String article) {
        Map<String, String> ids = new HashMap<String, String>();
        getTokenForCreateProduct();
        step("Создание товара", () -> {
            String barCode =  createCode(code);
            ReqCreateProduct body = ReqCreateProduct.builder()
                    .name(nameProduct).barcode(barCode).brand_id(1).category_id(3).height("200").is_new(0)
                    .length("50").vendor_code(article).weight("2000").width("100").xml_id(article)
                    .build();

            String response = given()
                    .spec(merchantSpec)
                    .header("authorization", authorizationToken)
                    .header("accept", ContentType.JSON)
                    .body(body)
                    .post(create_product.path()).then().assertThat().statusCode(200)
                    .extract().asString();

           String idProduct= (JsonPath.read(response, "$..product.id")).toString();
           String idOffer= (JsonPath.read(response, "$..offer.id")).toString();
           ids.put("idProduct", getParamJson(idProduct));
           ids.put("idOffer", getParamJson(idOffer));

        });
        return ids;
    }

    public void saveOfferPrice(int idProduct, int idOffer) {
        step("Добавление цены товара", () -> {
            ReqSaveOfferPrice body = ReqSaveOfferPrice.builder()
                   .status(5).offerId(idOffer).price("500")
                   .build();

            Response response = given()
                    .spec(merchantSpec)
                    .header("authorization", authorizationToken)
                    .header("accept", ContentType.JSON)
                    .body(body)
                    .post(catalog_products.pathSaveOfferPrice(String.valueOf(idProduct)));

            steps.checkResponseWithStatusCodeAndLogView(response, 200);
        });
    }


    public void saveProduct(int idProduct) {
        step("Сохранение данных по товару", () -> {
            ReqSaveProduct body = ReqSaveProduct.builder()
                   .explosive(0).fragile(0).daysToReturn("5").gas(0).hasBattery(0).needSpecialCase("нет")
                   .needSpecialStore("нет").packagedHeight("100").packagedLength("200").packagedWeight("2000").packagedWidth("100").build();

            Response response = given()
                    .spec(merchantSpec)
                    .header("authorization", authorizationToken)
                    .header("accept", ContentType.JSON)
                    .body(body)
                    .post(catalog_products.pathSaveProduct(String.valueOf(idProduct)));

            steps.checkResponseWithStatusCodeAndLogView(response, 200);
        });
    }

    public void saveStocks(int idProduct, int idOffer) {
        step("Добавление остатков по складу", () -> {
           // ReqSaveStocks body = ReqSaveStocks.builder()
           //      .offerId(idOffer).stocks(Stocks.builder().jsonMember1("1").build()).build();
            String body ="{\"stocks\":{\""+generalMerchConfig.getStore()+"\":\"10\"},\"offerId\":"+idOffer+"}";
            Response response = given()
                    .spec(merchantSpec)
                    .header("authorization", authorizationToken)
                    .header("accept", ContentType.JSON)
                    .body(body)
                    .post(catalog_products.pathSaveStocks(String.valueOf(idProduct)));

            steps.checkResponseWithStatusCodeAndLogView(response, 200);
        });
    }

    public int upload() {

        step("Загрузка изображения на сервер", () -> {
            String path = getAbsPath("src/test/resources/files/img/1.jpg");

            Response response = given()
                    .spec(fileMerchantSpec)
                    .header("authorization", authorizationToken)
                    .multiPart("file", new File(path), "image/jpeg")
                    .post(upload.pathUpload("catalog"));

           RespUpload respUpload = steps.checkResponseWithStatusCodeAndLogView(response, 200).as(RespUpload.class);
           idImage = respUpload.getId();
        });
        return idImage;
    }

    public void saveImage(int idProduct) {
        int idImg = upload();
        step("Сохранение изображения для товара", () -> {
            ReqSaveImage body = ReqSaveImage.builder().id(idImg).type(3).build();

            Response response = given()
                    .spec(merchantSpec)
                    .header("authorization", authorizationToken)
                    .header("accept", ContentType.JSON)
                    .body(body)
                    .post(catalog_products.pathSaveImage(String.valueOf(idProduct)));

            steps.checkResponseWithStatusCodeAndLogView(response, 200);
        });
    }

    public void saveClaim(int idProduct) {
        step("Отправка товара на модерацию", () -> {

            String response = given()
                    .spec(merchantSpec)
                    .body("{}")
                    .header("authorization", authorizationToken)
                    .header("accept", ContentType.JSON)
                    .post(catalog_products.pathSaveClaim(String.valueOf(idProduct)))
                    .then().assertThat().statusCode(200)
                    .extract().toString();

           //Assert.assertEquals(getParamJson(JsonPath.read(response, "$..saved").toString()), "success", "The parameter saved value must be correct.");
        });
    }

    public void getIdProductCheck(int idProduct) {
        step("Отправка товара на модерацию, получаем id заявки на проверку товара", () -> {

            String response = given()
                    .spec(merchantSpec)
                    .header("authorization", authorizationToken)
                    .get(product_check.path())
                    .then().assertThat().statusCode(200)
                    .extract().asString();

                idProductCheck = getParamJson(JsonPath.read(response, "$..items[?(@.payload.productIds[0] == '"+idProduct+"')].id").toString());

        });

    }

    public String getParamJson(String param)  {
        return param.replaceAll("\\[|\\]", "");

    }

    public static String getAbsPath(String path) {
        File file = new File(path);
        String fullPath = file.getAbsolutePath();
        return fullPath;
    }

    // Generate code EAN-13
    private String createCode(String numbers) {
        int nSumChet = 0;
        int nSumNeChet = 0;
        int result;
        int s=0;

        for (int i = 0; i < numbers.length(); i++) {
            s++;
            int d =  Character.getNumericValue(numbers.charAt(i));
            if (s % 2 == 0) {
                nSumChet += d;
            } else {
                nSumNeChet += d;
            }
        }

        result = nSumNeChet + nSumChet*3 ;
        if(result%10 == 0){
           return numbers + 0;
        }else {
           int res = 10 - result % 10;
            return numbers + res;
        }

    }
}
