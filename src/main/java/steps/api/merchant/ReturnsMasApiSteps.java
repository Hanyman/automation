package steps.api.merchant;

import env.Base_api;
import io.qameta.allure.Step;
import io.restassured.http.ContentType;
import io.restassured.response.Response;

import java.util.HashMap;
import java.util.Map;

import static constants.end_point.MerchantPanelEndpoint.*;
import static constants.end_point.MerchantPanelEndpoint.shipment_list;
import static helpers.api.ApiHelper.clearQuery;
import static io.qameta.allure.Allure.step;
import static io.restassured.RestAssured.given;

public class ReturnsMasApiSteps extends Base_api {

    @Step("Изменить статус возврата - На приемку")
    public void changeReturnStatus( String idReturn) {
        Response response = given()
                .spec(merchantSpec)
                .body("{\"return_status_id\":300}")
                .post(returns.changeReturnStatus(idReturn));
        steps.checkResponseWithStatusCodeAndLogView(response, 200);

    }

    @Step("Выбираем товар и принимаем его")
    public void returnsAcceptItems( String idItems) {
        Response response = given()
                .spec(merchantSpec)
                .body("{\"return_item_ids\":["+idItems+"]}")
                .post(returns_accept_items.path());
        steps.checkResponseWithStatusCodeAndLogView(response, 200);

    }

}
