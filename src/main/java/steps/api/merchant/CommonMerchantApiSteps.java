package steps.api.merchant;

import api_model.response.merchant.common.uploadFile.RespUpload;
import com.jayway.jsonpath.JsonPath;
import com.jayway.jsonpath.DocumentContext;
import env.Base_api;
import io.restassured.response.Response;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

import java.io.File;

import static constants.end_point.MerchantPanelEndpoint.upload;
import static helpers.api.ApiHelper.clearQuery;
import static helpers.api.ApiHelper.getParamJson;
import static io.qameta.allure.Allure.step;
import static io.restassured.RestAssured.given;

public class CommonMerchantApiSteps extends Base_api {
    RespUpload respUpload;

    public RespUpload uploadImg(String destination) {

        step("Загрузка изображения на сервер", () -> {
            String path = getAbsPath("src/test/resources/files/testFile.xlsx");

            Response response = given()
                    .spec(fileMerchantSpec)
                    .multiPart("file", new File(path), "image/jpeg")
                    .post(upload.pathUpload(destination));

            respUpload = steps.checkResponseWithStatusCodeAndLogView(response, 200).as(RespUpload.class);

        });
        return respUpload;
    }


    public RespUpload upload(String destination, String pathFile) {

        step("Загрузка изображения на сервер файлa " + pathFile  , () -> {

            Response response = given()
                    .spec(fileMerchantSpec)
                    .accept(" application/json")
                    .multiPart("file", new File(pathFile),"text/html")
                    .post(upload.pathUpload(destination));

            respUpload = steps.checkResponseWithStatusCodeAndLogView(response, 200).as(RespUpload.class);

        });
        return respUpload;
    }

    public RespUpload upload(String destination, String pathFile, String header) {

        step("Загрузка изображения на сервер файлa " + pathFile  , () -> {

            Response response = given()
                    .spec(fileMerchantSpec)
                    .header("authorization", header)
                    .accept(" application/json")
                    .multiPart("file", new File(pathFile),"text/html")
                    .post(upload.pathUpload(destination));

            respUpload = steps.checkResponseWithStatusCodeAndLogView(response, 200).as(RespUpload.class);

        });
        return respUpload;
    }
    public static String getAbsPath(String path) {
        File file = new File(path);
        String fullPath = file.getAbsolutePath();
        return fullPath;
    }

    // получение токена из странцы html
    public String getAuthToken(Response response){
        Document doc = Jsoup.parse(response.getBody().asString());
        Element div = doc.getElementById("app");
        String getAttr = div.attr("data-env");
        String authToken = getParamJson(JsonPath.read(getAttr,"$..token").toString());
        return  "Bearer " + authToken;
    }

    // Получение даных мерчанта из html
    protected String getStrInfoMerchFromHtml(Response response, String jsonPath){
        Document doc = Jsoup.parse(response.getBody().asString());
        Element div = doc.getElementById("app");
        String getAttr = div.attr("data-props");
        String info = getParamJson(JsonPath.read(getAttr,jsonPath).toString());
        return clearQuery(info);
    }

}
