package steps.api.admin.Customers;

import api_model.request.admin.returns.ReqAddProblems;
import com.google.gson.internal.bind.util.ISO8601Utils;
import com.jayway.jsonpath.JsonPath;
import env.Base_api;
import io.qameta.allure.Step;
import io.restassured.response.Response;

import static constants.end_point.AdminPanelEndpoint.*;
import static helpers.api.ApiHelper.getParamJson;
import static io.restassured.RestAssured.given;

public class CustomersAdminApiSteps extends Base_api {

    @Step("Получить ид клиента по фио")
    public String getListCustomers(String name,String phone) {
        Response response = given()
                .spec(adminSpec)
                .get(customers_filter.path()+"?id=&status=&phone=&full_name="+name+"&gender=0&created_at=&use_period=false&showDeleted=false&role=0&isReferral=0&isOrganization=0&page=");
        steps.checkResponseWithStatusCodeAndLogView(response, 200);
        String idCustomer= getParamJson(JsonPath.read(response.asString(), "$..users[?(@.phone=='"+phone+"')].id").toString());
        return idCustomer;
    }
    @Step("Установить проблемный статус клиенту")
    public void setStatusCustomer(String id, String status, String comment) {
         Response response = given()
                .spec(adminSpec)
                .body("{\"customer\":{\"status\":"+status+",\"comment_status\":\""+comment+"\"}}")
                .put(customers.customersId(id));
        steps.checkResponseWithStatusCodeAndLogView(response, 204);

    }

}
