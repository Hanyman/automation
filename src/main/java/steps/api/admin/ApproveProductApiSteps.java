package steps.api.admin;

import api_model.request.admin.approveProducts.ReqApproveProducts;
import api_model.request.admin.changeStatus.ReqChangeStatus;
import env.Base_api;
import io.restassured.response.Response;

import java.util.ArrayList;
import java.util.List;

import static constants.end_point.AdminPanelEndpoint.claims_product_check;
import static constants.end_point.AdminPanelEndpoint.products_approval;
import static io.qameta.allure.Allure.step;
import static io.restassured.RestAssured.given;

public class ApproveProductApiSteps extends Base_api {

    public void approveProduct(int idProduct, String idProductCheck) {
        List<String> listProduct = new ArrayList<>();
        listProduct.add(String.valueOf(idProduct));
        step("Изменение статуса заявки на проверку товара с Новая на В работе", () -> {
            ReqChangeStatus body = ReqChangeStatus.builder().status(2).build();
            Response response = given()
                    .spec(adminSpec)
                    .body(body)
                    .put(claims_product_check.pathChangeStatus(idProductCheck));

            steps.checkResponseWithStatusCodeAndLogView(response, 200);
        });

        step("Согласование товара", () -> {
            ReqApproveProducts body = ReqApproveProducts.builder().comment("").productIds(listProduct).status("accept").build();
            Response response = given()
                    .spec(adminSpec)
                    .body(body)
                    .put(products_approval.path());

            steps.checkResponseWithStatusCodeAndLogView(response, 200);
        });

        step("Изменение статуса заявки на проверку товара с В Работе на Согласовано", () -> {
            ReqChangeStatus body = ReqChangeStatus.builder().status(3).build();
            Response response = given()
                    .spec(adminSpec)
                    .body(body)
                    .put(claims_product_check.pathChangeStatus(idProductCheck));

            steps.checkResponseWithStatusCodeAndLogView(response, 200);
        });
    }

}
