package steps.api.admin;

import api_model.request.admin.login.LoginAdmin;
import env.Base_api;
import io.restassured.response.Response;

import static constants.end_point.AdminPanelEndpoint.login_admin;
import static io.qameta.allure.Allure.step;
import static io.restassured.RestAssured.given;

public class LoginAdminApiSteps extends Base_api {

    public void loginAdminPanel(String loginEmail, String password) {
        step("Логинимся под администратором в админку", () -> {
            LoginAdmin login = LoginAdmin.builder().email(loginEmail).password(password).build();

            Response response = given()
                    .spec(adminSpec)
                    .body(login)
                    .post(login_admin.path());

            steps.checkResponseWithStatusCodeAndLogView(response, 200);
        });
    }

}
