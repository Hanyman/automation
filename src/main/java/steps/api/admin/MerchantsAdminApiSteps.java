package steps.api.admin;

import com.jayway.jsonpath.JsonPath;
import env.Base_api;
import io.qameta.allure.Step;
import io.restassured.response.Response;
import org.testng.Assert;

import java.util.ArrayList;
import java.util.List;

import static constants.end_point.AdminPanelEndpoint.*;
import static helpers.Helper.getResource;
import static helpers.api.ApiHelper.*;
import static io.qameta.allure.Allure.step;
import static io.restassured.RestAssured.given;

public class MerchantsAdminApiSteps extends Base_api {

    @Step("Получаем статус возврата из админки")
    public String getMerchantId( String nameMerchant) {
        Response response = given()
                .spec(adminSpec)
                .get(merchant_list_page.filterMerchants(nameMerchant));
        steps.checkResponseWithStatusCodeAndLogView(response, 200);
        String idMerchant= getParamJson(JsonPath.read(response.asString(), "$..items[?(@.legal_name=='"+nameMerchant+"')].id").toString());
        return idMerchant;
    }
    @Step("Устанавливаем статус мерчанта")
    public void setStatusMerchant(String idMerchant, String merchantName, String status, String msg) {
        Response response = given()
                .spec(adminSpec)
                .body("{}")
                .post(merchant_detail.filterMerchants(idMerchant, merchantName, status, msg));
        steps.checkResponseWithStatusCodeAndLogView(response, 204);

    }

}
