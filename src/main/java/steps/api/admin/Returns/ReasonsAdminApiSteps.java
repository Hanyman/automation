package steps.api.admin.Returns;

import api_model.request.admin.masterclass.speakers.ReqAddSpeakers;
import api_model.request.admin.masterclass.speakers.Speaker;
import api_model.request.admin.returns.ReqAddReasons;
import com.jayway.jsonpath.JsonPath;
import env.Base_api;
import io.qameta.allure.Step;
import io.restassured.response.Response;

import java.io.File;

import static constants.end_point.AdminPanelEndpoint.*;
import static constants.end_point.MerchantPanelEndpoint.upload;
import static helpers.api.ApiHelper.getAbsPath;
import static helpers.api.ApiHelper.getParamJson;
import static io.restassured.RestAssured.given;

public class ReasonsAdminApiSteps extends Base_api {


    @Step("Добавить новоую причину возврата")
    public int addReason( String name, String clientName, int returnReasonCategoryId, boolean active, boolean isRequiredPhoto,boolean isRequiredComment, String hint) {
        ReqAddReasons body = ReqAddReasons.builder().name(name).clientName(clientName).returnReasonCategoryId(returnReasonCategoryId)
                .isActive(active).isRequiredPhoto(isRequiredPhoto).isRequiredComment(isRequiredComment)
                .isDefault(false).sort(null).hint(hint).build();
        Response response = given()
                .spec(adminSpec)
                .body(body)
                .post(returns_config_reasons.path());
        steps.checkResponseWithStatusCodeAndLogView(response, 200);
        int idReason = getListReasons(name);
        return idReason;
    }

    @Step("Получить ид причины возврата по имени")
    public int getListReasons(String nameReason) {

        Response response = given()
                .spec(adminSpec)
                .get(list_returns_config_reasons.path());
        steps.checkResponseWithStatusCodeAndLogView(response, 200);
        int idReason= Integer.parseInt(getParamJson(JsonPath.read(response.toString(), "$..reasons[?(@.name=='"+nameReason+"')].id").toString()));
        return idReason;
    }
    @Step("Получить клиентское название причины возврата по имени")
    public String getListReasonsClientName(String nameReason) {

        Response response = given()
                .spec(adminSpec)
                .get(list_returns_config_reasons.path());
        steps.checkResponseWithStatusCodeAndLogView(response, 200);
        String ClientNameReason= getParamJson(JsonPath.read(response.toString(), "$..reasons[?(@.name=='"+nameReason+"')].client_name").toString());
        return ClientNameReason;
    }

    @Step("Удалить причину по ид")
    public void deleteReason(int id) {
        Response response = given()
                .spec(adminSpec)
                .delete(returns_config_reasons.path() + "?id="+id+"");
        steps.checkResponseWithStatusCodeAndLogView(response, 200);
    }

}
