package steps.api.admin.Returns;

import api_model.request.admin.returns.ReqAddReasons;
import com.jayway.jsonpath.JsonPath;
import env.Base_api;
import io.qameta.allure.Step;
import io.restassured.response.Response;
import org.testng.Assert;

import static constants.end_point.AdminPanelEndpoint.*;
import static helpers.api.ApiHelper.getClearString;
import static helpers.api.ApiHelper.getParamJson;
import static io.restassured.RestAssured.given;

public class ReturnsAdminApiSteps extends Base_api {


    @Step("Изменить статус возврата на Передан в ЛО")
    public void changeReturnStatus( String idReturn) {
        Response response = given()
                .spec(adminSpec)
                .post(returns.changeReturnStatus(idReturn,"status-to-ds"));
        steps.checkResponseWithStatusCodeAndLogView(response, 200);

    }

    @Step("Изменить статус возврата на Принимается")
    public void changeReturnStatusAccept( String idReturn, String status) {
        Response response = given()
                .spec(adminSpec)
                .body("{\"return_status_id\":"+status+"}")
                .post(returns.changeReturnStatusAccept(idReturn));
        steps.checkResponseWithStatusCodeAndLogView(response, 200);

    }

    @Step("Принять товар в админке вкладка Состав возврата")
    public void acceptReturnItem( String idItem) {
        Response response = given()
                .spec(adminSpec)
                .body("{}")
                .post(returns.acceptItems(idItem));
        steps.checkResponseWithStatusCodeAndLogView(response, 200);

    }

    @Step("Получаем статус возврата из админки")
    public void getReturnStatus( String idReturn, String return_status_msg) {
        Response response = given()
                .spec(adminSpec)
                .get(returns.returnData(idReturn));
        steps.checkResponseWithStatusCodeAndLogView(response, 200);
        String return_status = getParamJson(JsonPath.read(response.asString(), "$..iReturn.returnStatus.name").toString());
        Assert.assertEquals(getClearString(return_status),return_status_msg,"Статус возврата должен соответсвовать текущему этапу возврата.");

    }

    @Step("Получаем статус возврата из админки")
    public void getReturnData( String idReturn) {
        Response response = given()
                .spec(adminSpec)
                .get(returns.returnData(idReturn));
        steps.checkResponseWithStatusCodeAndLogView(response, 200);
        String return_status = getParamJson(JsonPath.read(response.asString(), "$..iReturn.returnStatus.name").toString());

    }

}
