package steps.api.admin.Returns;

import api_model.request.admin.returns.ReqAddProblems;
import com.jayway.jsonpath.JsonPath;
import env.Base_api;
import io.qameta.allure.Step;
import io.restassured.response.Response;

import static constants.end_point.AdminPanelEndpoint.*;
import static helpers.api.ApiHelper.getParamJson;
import static io.restassured.RestAssured.given;

public class ProblemsAdminApiSteps extends Base_api {


    @Step("Добавить новоую проблему возврата")
    public int addProblem( String name, String clientName, boolean active) {
        ReqAddProblems body = ReqAddProblems.builder().name(name).clientName(clientName).isActive(active).build();
        Response response = given()
                .spec(adminSpec)
                .body(body)
                .post(returns_item_problem_reasons.path());
        steps.checkResponseWithStatusCodeAndLogView(response, 200);
        int idReason = getListProblems(name);
        return idReason;
    }

    @Step("Получить ид проблемы возврата по имени")
    public int getListProblems(String nameProblem) {

        Response response = given()
                .spec(adminSpec)
                .get(list_returns_problem.path());
        steps.checkResponseWithStatusCodeAndLogView(response, 200);
        int idProblem= Integer.parseInt(getParamJson(JsonPath.read(response.toString(), "$..[?(@.name=='"+nameProblem+"')].id").toString()));
        return idProblem;
    }

    @Step("Удалить проблему по ид")
    public void deleteProblem(int id) {
        Response response = given()
                .spec(adminSpec)
                .delete(returns_item_problem_reasons.path() + "?ids[]="+id+"");
        steps.checkResponseWithStatusCodeAndLogView(response, 200);
    }

}
