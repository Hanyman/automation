package steps.api.admin;

import com.jayway.jsonpath.JsonPath;
import env.Base_api;
import io.qameta.allure.Step;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import org.w3c.dom.ls.LSOutput;

import java.util.ArrayList;
import java.util.List;

import static constants.end_point.AdminPanelEndpoint.*;
import static constants.end_point.MarketEndpoint.v1_lk_receivers;
import static helpers.Helper.getResource;
import static helpers.api.ApiHelper.*;
import static io.qameta.allure.Allure.step;
import static io.restassured.RestAssured.given;

public class OrdersAdminApiSteps extends Base_api {

    public void payOrder(int idOrder) {

        step("Оплата заказа со стороны админа", () -> {
            Response response = given()
                    .spec(adminSpec)
                    .body("{}")
                    .put(order.apiOrderPay(idOrder));

            steps.checkResponseWithStatusCodeAndLogView(response, 200);
        });

    }

    @Step("Получаем данные из заказа для запроса доставки")
    public String getDataOrder(int id) {

        Response response = given()
                .spec(adminSpec)
                .get(order.cardOrder(id));

        steps.checkResponseWithStatusCodeAndLogView(response, 200);
        return response.asString();
    }

    @Step("Устанавливаем статус заказа - Доставлено получателю")
    public void setStatusOrderDelivered(int idOrder, String numbOrder) {
        String dataOrder = getDataOrder(idOrder);
        ArrayList<Integer> ids= new ArrayList<>();

        List<Integer> idValues = JsonPath.read(dataOrder, "$.history..data[?(@.number=='"+numbOrder+"-1' || @.number=='"+numbOrder+"-2')].id");
        ids.addAll(idValues);

        for(int i=0; i< ids.size(); i++) {

            String path = "?(@.id=='"+ids.get(i)+"')";
            int delivery_id = ids.get(i);
            String pdd = clearQuery(JsonPath.read(dataOrder, "$.history..data["+path+"].pdd").toString());
            String tariff_id = clearQuery(JsonPath.read(dataOrder, "$.history..data["+path+"].tariff_id").toString());
            String point_id = clearQuery(JsonPath.read(dataOrder, "$.history..data["+path+"].point_id").toString());
            String receiver_email = clearQuery(JsonPath.read(dataOrder, "$.receiver_email").toString());
            String receiver_name = clearQuery(JsonPath.read(dataOrder, "$.receiver_name").toString());
            String receiver_phone = clearQuery(JsonPath.read(dataOrder, "$.receiver_phone").toString());
            String delivery_address = getJsonDeliveries(dataOrder, path);
            String body = getJsonInput(delivery_address, pdd, point_id, tariff_id, receiver_name, receiver_email, receiver_phone);
            Response response = given()
                    .spec(adminSpec)
                    .body(body)
                    .put(order.deliveriesOrder(idOrder, delivery_id));
            steps.checkResponseWithStatusCodeAndLogView(response, 200);
       }
    }

    public String getJsonInput(String delivery_address,String pdd,String point_id,String tariff_id,
                               String receiver_name,String receiver_email,String receiver_phone){
        String textBlock = getResource("src/test/resources/apiJson/adminOrderDeliveries.json");
        textBlock = textBlock
                .replace("__delivery_address", delivery_address)
                .replace("__pdd", pdd)
                .replace("__point_id", point_id)
                .replace("__tariff_id", tariff_id)
                .replace("__receiver_name", receiver_name)
                .replace("__receiver_email", receiver_email)
                .replace("__receiver_phone", receiver_phone);
        return textBlock;
    }
    public String getJsonDeliveries(String delivery_address_json,String path){
        String address_string = clearQuery(JsonPath.read(delivery_address_json, "$.deliveries["+path+"]..delivery_address.address_string").toString());
        String country_code = clearQuery(JsonPath.read(delivery_address_json, "$.deliveries["+path+"]..delivery_address.country_code").toString());
        String post_index = clearQuery(JsonPath.read(delivery_address_json, "$.deliveries["+path+"]..delivery_address.post_index").toString());
        String region = clearQuery(JsonPath.read(delivery_address_json, "$.deliveries["+path+"]..delivery_address.region").toString());
        String region_guid = clearQuery(JsonPath.read(delivery_address_json, "$.deliveries["+path+"]..delivery_address.region_guid").toString());
        String city = clearQuery(JsonPath.read(delivery_address_json, "$.deliveries["+path+"]..delivery_address.city").toString());
        String city_guid = clearQuery(JsonPath.read(delivery_address_json, "$.deliveries["+path+"]..delivery_address.city_guid").toString());
        String street = clearQuery(JsonPath.read(delivery_address_json, "$.deliveries["+path+"]..delivery_address.street").toString());
        String house = clearQuery(JsonPath.read(delivery_address_json, "$.deliveries["+path+"]..delivery_address.house").toString());
        String comment = clearQuery(JsonPath.read(delivery_address_json, "$.deliveries["+path+"]..delivery_address.comment").toString());
        String flat = clearQuery(JsonPath.read(delivery_address_json, "$.deliveries["+path+"]..delivery_address.flat").toString());
        String floor = clearQuery(JsonPath.read(delivery_address_json, "$.deliveries["+path+"]..delivery_address.floor").toString());
        String intercom = clearQuery(JsonPath.read(delivery_address_json, "$.deliveries["+path+"]..delivery_address.intercom").toString());
        String porch = clearQuery(JsonPath.read(delivery_address_json, "$.deliveries["+path+"]..delivery_address.porch").toString());

        String textBlock = getResource("src/test/resources/apiJson/deliveries.json");
        textBlock = textBlock
                .replace("__addressstring", address_string)
                .replace("__countrycode", country_code)
                .replace("__postindex", post_index)
                .replace("__region", region)
                .replace("__regguid", region_guid)
                .replace("__city", city)
                .replace("__citguid", city_guid)
                .replace("__street", street)
                .replace("__house", house)
                .replace("__comment", getClearString(comment))
                .replace("__flat", getClearString(flat))
                .replace("__floor", getClearString(floor))
                .replace("__intercom", getClearString(intercom))
                .replace("__porch", getClearString(porch));

        return textBlock;
    }

}
