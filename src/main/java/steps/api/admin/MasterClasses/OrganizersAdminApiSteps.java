package steps.api.admin.MasterClasses;

import env.Base_api;
import io.restassured.response.Response;

import static constants.end_point.AdminPanelEndpoint.order;
import static io.qameta.allure.Allure.step;
import static io.restassured.RestAssured.given;

public class OrganizersAdminApiSteps extends Base_api {

    public void payOrder(int idOrder) {

        step("Оплата заказа со стороны админа", () -> {
            Response response = given()
                    .spec(adminSpec)
                    .body("{}")
                    .put(order.apiOrderPay(idOrder));

            steps.checkResponseWithStatusCodeAndLogView(response, 200);
        });

    }

}
