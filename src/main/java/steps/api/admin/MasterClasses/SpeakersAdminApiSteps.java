package steps.api.admin.MasterClasses;

import api_model.request.admin.masterclass.speakers.ReqAddSpeakers;
import api_model.request.admin.masterclass.speakers.Speaker;
import api_model.response.merchant.common.uploadFile.RespUpload;
import com.jayway.jsonpath.JsonPath;
import env.Base_api;
import io.qameta.allure.Step;
import io.restassured.response.Response;

import java.io.File;

import static constants.end_point.AdminPanelEndpoint.*;
import static constants.end_point.MerchantPanelEndpoint.upload;
import static helpers.api.ApiHelper.getAbsPath;
import static helpers.api.ApiHelper.getParamJson;
import static io.qameta.allure.Allure.step;
import static io.restassured.RestAssured.given;

public class SpeakersAdminApiSteps extends Base_api {


    @Step("Добавить нового спикера или отредактировать существующего")
    public int addSpeaker( String first_name, String last_name, String middle_name, String phone, String email,int profession_id) {
        int fileId = uploadFile();
        //"+7(999) 991-12-32"
        ReqAddSpeakers body = ReqAddSpeakers.builder().id(null).speaker(Speaker.builder()
                .first_name(first_name).last_name(last_name).middle_name(middle_name).phone(phone)
                .email(email).description("Default description").file_id(fileId).global(true).profession_id(profession_id)
                .linkedin(null).facebook(null).instagram(null).build())
                .build();
        Response response = given()
                .spec(adminSpec)
                .body(body)
                .post(public_events_speakers_save.path());
        steps.checkResponseWithStatusCodeAndLogView(response, 200);
        int idSpeaker = getListSpeakers(first_name);
        return idSpeaker;
    }

    @Step("Получить ид спикера по имени")
    public int getListSpeakers(String name) {

        Response response = given()
                .spec(adminSpec)
                .get(public_events_types_page.path()+"?page=1");
        steps.checkResponseWithStatusCodeAndLogView(response, 200);
        int idEventType= Integer.parseInt(getParamJson(JsonPath.read(response.toString(), "$..speakers[?(@.first_name=='"+name+"')].id").toString()));
        return idEventType;
    }

    @Step("Удалить спикера по ид")
    public void deleteSpeaker(int id) {

        Response response = given()
                .spec(adminSpec)
                .body("{\"ids\":["+id+"]}")
                .post(public_events_speakers_delete.path());
        steps.checkResponseWithStatusCodeAndLogView(response, 200);

    }

    @Step("Загрузка изображения на сервер")
    public int uploadFile() {

        String path = getAbsPath("src/test/resources/files/img/speaker.png");

        Response response = given()
                 .spec(fileAdminSpec)
                 .multiPart("file", new File(path), "image/jpeg")
                 .post(upload.pathUpload("?destination=speaker&caption="));

        steps.checkResponseWithStatusCodeAndLogView(response, 200);
        int idImage = Integer.parseInt(getParamJson(JsonPath.read(response.toString(), "$..id").toString()));
        return idImage;
    }
}
