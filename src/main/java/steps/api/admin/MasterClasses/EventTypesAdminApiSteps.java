package steps.api.admin.MasterClasses;

import com.jayway.jsonpath.JsonPath;
import env.Base_api;
import io.qameta.allure.Step;
import io.restassured.response.Response;

import static constants.end_point.AdminPanelEndpoint.*;
import static helpers.api.ApiHelper.getParamJson;
import static io.restassured.RestAssured.given;

public class EventTypesAdminApiSteps extends Base_api {

    @Step("Добавить новый тип событий или отредактировать существующий")
    public void addEventType(int id, String name, String code) {

        Response response = given()
                .spec(adminSpec)
                .body("{\"id\":"+id+"\",\"type\":{\"name\":\""+name+"\",\"code\":\""+code+"\"}}")
                .post(public_events_types_save.path());
        steps.checkResponseWithStatusCodeAndLogView(response, 200);

    }

    @Step("Получить список всех событий")
    public int getListEventType(String name) {

        Response response = given()
                .spec(adminSpec)
                .get(public_events_types_page.path()+"?page=1");
        steps.checkResponseWithStatusCodeAndLogView(response, 200);
        int idEventType= Integer.parseInt(getParamJson(JsonPath.read(response.toString(), "$..types[?(@.name=='"+name+"')].id").toString()));
        return idEventType;
    }

    @Step("Удалить событие по ид")
    public void deleteEventType(int id) {

        Response response = given()
                .spec(adminSpec)
                .body("{\"ids\":["+id+"]}")
                .post(public_events_types_delete.path());
        steps.checkResponseWithStatusCodeAndLogView(response, 200);

    }

}
