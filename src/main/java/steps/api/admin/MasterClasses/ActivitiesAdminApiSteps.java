package steps.api.admin.MasterClasses;

import com.jayway.jsonpath.JsonPath;
import env.Base_api;
import io.qameta.allure.Step;
import io.restassured.response.Response;

import static constants.end_point.AdminPanelEndpoint.customers_activities;
import static helpers.api.ApiHelper.getParamJson;
import static io.qameta.allure.Allure.step;
import static io.restassured.RestAssured.given;

public class ActivitiesAdminApiSteps extends Base_api {

    @Step("Добавить новую активность")
    public int addActivity(String name) {

        Response response = given()
             .spec(adminSpec)
             .body("{\"id\":false,\"name\":\""+name+"\",\"active\":1}")
             .post(customers_activities.path());
        steps.checkResponseWithStatusCodeAndLogView(response, 200);
        int idActivity= Integer.parseInt(getParamJson(JsonPath.read(response.toString(), "$..activities[?(@.name=='"+name+"')].id").toString()));
        return idActivity;

    }

}
