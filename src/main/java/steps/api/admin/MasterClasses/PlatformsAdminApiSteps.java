package steps.api.admin.MasterClasses;

import api_model.request.admin.masterclass.platforms.Place;
import api_model.request.admin.masterclass.platforms.ReqPlatforms;
import com.jayway.jsonpath.JsonPath;
import env.Base_api;
import io.qameta.allure.Step;
import io.restassured.response.Response;

import java.util.ArrayList;

import static constants.end_point.AdminPanelEndpoint.*;
import static helpers.api.ApiHelper.getParamJson;
import static io.restassured.RestAssured.given;

public class PlatformsAdminApiSteps extends Base_api {

    @Step("Добавить новую платформу или отредактировать существующего")
    public int addPlatform( String platform_name) {
        ArrayList<Object > empty = new ArrayList<>();
        ReqPlatforms body = ReqPlatforms.builder().id(null).place(Place.builder().name(platform_name)
                .description("Пройти прямо").address("г Москва, ул Советская, д 9 стр 20").city_name("Москва")
                .global(true).latitude("55.7971442").longitude("37.7461932").media(empty).city_id("0c5b2444-70a0-4932-980c-b4dc0d3f02b5").build()).build();

        Response response = given()
                .spec(adminSpec)
                .body(body)
                .post(public_events_places_save.path());
        steps.checkResponseWithStatusCodeAndLogView(response, 200);
        int idSpeaker = getListPlatforms(platform_name);
        return idSpeaker;
    }

    @Step("Получить ид платформы по имени")
    public int getListPlatforms(String name) {

        Response response = given()
                .spec(adminSpec)
                .get(public_events_places_page.path()+"?page=1");
        steps.checkResponseWithStatusCodeAndLogView(response, 200);
        int idEventType= Integer.parseInt(getParamJson(JsonPath.read(response.toString(), "$..places[?(@.name=='"+name+"')].id").toString()));
        return idEventType;
    }

    @Step("Удалить платформу по ид")
    public void deletePlatform(int id) {

        Response response = given()
                .spec(adminSpec)
                .body("{\"ids\":["+id+"]}")
                .post(public_events_places_delete.path());
        steps.checkResponseWithStatusCodeAndLogView(response, 200);

    }

}
