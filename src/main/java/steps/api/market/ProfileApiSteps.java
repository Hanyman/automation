package steps.api.market;

import api_model.request.market.receivers.ReqReceivers;
import api_model.request.market.profile.ReqProfile;
import api_model.response.market.profile.GetProfileClient;
import com.jayway.jsonpath.JsonPath;
import env.Base_api;
import io.qameta.allure.Step;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;

import static constants.end_point.MarketEndpoint.*;
import static helpers.api.ApiHelper.getParamJson;
import static io.qameta.allure.Allure.step;
import static io.restassured.RestAssured.given;

public class ProfileApiSteps extends Base_api {

    @Step("Получаем данные профиля клиента")
    public String getProfile(RequestSpecification spec) {
        Response response = given()
            .spec(spec)
            .get(v1_lk_profile.path());

        return response.asString();
    }

    @Step("Устанавливаем профиль клиента по умолчанию")
    public void setUpDefaultProfile(RequestSpecification spec) {
        ReqProfile body = ReqProfile.builder().firstName("First").lastName("Last").middleName("Middle").birthday("1995-05-03").gender(1).build();
        Response response = given()
            .spec(spec)
            .body(body)
            .put(profile_personal.path());

        steps.checkResponseWithStatusCodeAndLogView(response, 204);

    }
    /**Добавление, получение, удаление получателей **/
    @Step("Добавление получателя")
    public void createNewReceivers(RequestSpecification spec, String firstName, String lastName, String phone, String email) {
        ReqReceivers body = ReqReceivers.builder().first_name(firstName).last_name(lastName).email(email).phone(phone).post("").build();
        Response response = given()
            .spec(spec)
            .body(body)
            .post(receivers.path());

        steps.checkResponseWithStatusCodeAndLogView(response, 200);


    }
    @Step("Получение списка получателей")
    public String getReceivers(RequestSpecification spec) {

        Response response = given()
            .spec(spec)
            .get(receivers.path());

        steps.checkResponseWithStatusCodeAndLogView(response, 200);

        return response.asString();
    }

    @Step("Удаление получателей всех")
    public void deleteReceivers(RequestSpecification spec) {
        String resp = getReceivers(spec);
        int lengthReceivers = Integer.parseInt(getParamJson(JsonPath.read(resp, "$..receivers.length()").toString()));
        for(int i=0; i<lengthReceivers; i++) {

            int receiver = Integer.parseInt(getParamJson(JsonPath.read(resp, "$..receivers["+i+"].id").toString()));
            Response response = given()
                    .spec(spec)
                    .delete(receivers.pathAndEndpoint(receiver));

            steps.checkResponseWithStatusCodeAndLogView(response, 200);

        }

    }

    @Step("Удаляем все предпочтения по брендам")
    public void removeAllBrands(RequestSpecification spec) {
        String body = "{\"brands\":[]}";
        Response response = given()
            .spec(spec)
            .body(body)
            .put(remove_preferences_brands.path());

        steps.checkResponseWithStatusCodeAndLogView(response, 204);

    }
    @Step("Удаляем все предпочтения по категориям")
    public void removeAllCategories(RequestSpecification spec) {
       String body = "{\"categories\":[]}";
        Response response = given()
            .spec(spec)
            .body(body)
            .put(remove_preferences_category.path());

        steps.checkResponseWithStatusCodeAndLogView(response, 204);

    }

}
