package steps.api.market;

import api_model.response.market.profile.GetProfileClient;
import env.Base_api;
import io.qameta.allure.Step;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;

import static constants.end_point.MarketEndpoint.*;
import static io.restassured.RestAssured.given;
public class CatalogApiSteps extends Base_api {

    @Step("Получаем данные по брендам")
    public String getBrands(RequestSpecification spec) {
        Response response = given()
            .spec(spec)
            .get(v1_lk_preferences_catalog.path());

        steps.checkResponseWithStatusCodeAndLogView(response, 200);


        return response.asString();
    }

    @Step("Получаем данные по категориям")
    public void getCategories(RequestSpecification spec) {
        Response response = given()
            .spec(spec)
            .get(v1_lk_preferences_catalog.path());

        steps.checkResponseWithStatusCodeAndLogView(response, 200).as(GetProfileClient.class);
    }

}
