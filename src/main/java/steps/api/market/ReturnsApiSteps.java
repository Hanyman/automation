package steps.api.market;

import com.jayway.jsonpath.JsonPath;
import env.Base_api;
import io.qameta.allure.Step;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import org.testng.Assert;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import static constants.end_point.MarketEndpoint.*;
import static helpers.Helper.getResource;
import static helpers.api.ApiHelper.*;
import static io.restassured.RestAssured.given;

public class ReturnsApiSteps extends Base_api {
    ProfileApiSteps profileApiSteps = new ProfileApiSteps();
    @Step("Получаем список заказов у которых можно оформить возврат(статус Доставлено)")
    public ArrayList<Integer> getListOrders(RequestSpecification spec) {

        Response response = given()
                .spec(spec)
                .get(v1_lk_order.path());

        steps.checkResponseWithStatusCodeAndLogView(response, 200);
        ArrayList<Integer> listOrders = JsonPath.read(response.asString(), "$..orders[?(@.isset_items_to_return==true)].id");
        return listOrders;
    }

    @Step("Получаем данные по конкретному заказу")
    public ArrayList<Integer> getOrderData(RequestSpecification spec, int orderNumb) {
        ArrayList<Integer> idsItems= new ArrayList<>();
        Response response = given()
                .spec(spec)
                .get(v1_lk_order.pathGetOrder(orderNumb));
        steps.checkResponseWithStatusCodeAndLogView(response, 200);

        List<Integer> basketItem = JsonPath.read(response.asString(), "$..deliveries..products[*].id");
        idsItems.addAll(basketItem);

        return idsItems;
    }
    @Step("Выбираем товар для возврата")
    public String addItemsToReturns(RequestSpecification spec, int orderNumb) {
       ArrayList<Integer> basketItem = getOrderData(spec,orderNumb);
       Response response = given()
               .spec(spec)
               .body("{\"basket_items\":"+basketItem+"}")
               .post(v1_lk_returns_add_items.pathGetOrder(orderNumb));
        steps.checkResponseWithStatusCodeAndLogView(response, 200);

        String returnId = clearQuery(JsonPath.read(response.asString(), "$..return_id").toString());
        return returnId;
    }

    @Step("Получаем информацию по списку товара возврата")
    public ArrayList<Integer> addReturnItemsInfo(RequestSpecification spec, int numbOrder) {
       ArrayList<Integer> idsItems= new ArrayList<>();
       Response response = given()
               .spec(spec)
               .get(v1_lk_returns_current_items.pathGetRetItem(numbOrder));
        steps.checkResponseWithStatusCodeAndLogView(response, 200);
        List<Integer> itemsId = JsonPath.read(response.asString(), "$..items[*].id");
        idsItems.addAll(itemsId);
        return idsItems;
    }
    @Step("Получаем информацию по причинам возврата")
    public String addReasonsInfo(RequestSpecification spec, String itemId) {
       Response response = given()
               .spec(spec)
               .get(v1_lk_returns_reasons.pathGetReturnItem(itemId));
        steps.checkResponseWithStatusCodeAndLogView(response, 200);

        String itemsId = clearQuery(JsonPath.read(response.asString(), "$..items[0].id").toString());
        return itemsId;
    }

    @Step("Загрузка изображения на сервер")
    public String uploadFile() {

        String path = getAbsPath("src/test/resources/files/img/1.jpg");

        Response response = given()
                .spec(fileMarketSpec)
                .multiPart("file", new File(path), "image/jpeg")
                .post(v1_lk_returns_add_photo.path());

        steps.checkResponseWithStatusCodeAndLogView(response, 200);
        String idImage = getParamJson(JsonPath.read(response.asString(), "$..file_id").toString());
        return idImage;
    }

    @Step("Добавляем причину, описание возврата и фото")
    public void addReasonPhotoDescToReturn(RequestSpecification spec, int numbOrder, String itemId, String fileId, int idReason, String desc) {
        Response response = given()
                .spec(spec)
                .body("{\"files\":["+fileId+"],\"return_reason_id\":"+idReason+",\"return_reason_description\":\""+desc+"\"}")
                .post(v1_lk_returns.pathAddReasonToReturn(numbOrder,itemId));
        steps.checkResponseWithStatusCodeAndLogView(response, 200);

    }
    @Step("Получить инфо о возврате")
    public void getReturnInfo(RequestSpecification spec, String returnId, String return_status_msg) {
        Response response = given()
                .spec(spec)
                .get(v1_lk_returns.pathGetReturnItem(returnId));
        steps.checkResponseWithStatusCodeAndLogView(response, 200);
        String return_status = getParamJson(JsonPath.read(response.asString(), "$..return_status.client_name").toString());
        Assert.assertEquals(getClearString(return_status),return_status_msg,"Статус возврата должен соответсвовать текущему этапу возврата.");


    }
    @Step("Получить точки для возврата")
    public String getDeliveriesPoints(RequestSpecification spec)
    {
        Response response = given()
                .spec(spec)
                .get(v1_points_filtered.pathGetDeliveriesPoints("?pointFrom=55.715217397332474;37.47739907299803&pointTo=55.79266399578914;37.763386927001946&packageWeight=100&packageWidth=16&packageLength=33&packageHeight=16"));
        steps.checkResponseWithStatusCodeAndLogView(response, 200);

        String point = getParamJson(JsonPath.read(response.asString(), "$..data[0]..id").toString());
        return point;
    }
    @Step("Получить адрес точки для возврата")
    public String getAddressDeliveryPoint(RequestSpecification spec, String point)
    {
        Response response = given()
                .spec(spec)
                .get(v1_points.path()+point);
        steps.checkResponseWithStatusCodeAndLogView(response, 200);

        String addressPoint = getParamJson(JsonPath.read(response.asString(), "$..data..address").toString());
        return addressPoint;
    }

    @Step("Получить стоимость по выбранной точке возврата")
    public String calculateDelivery(RequestSpecification spec,String address,String returnId, String point)
    {
        Response response = given()
                .spec(spec)
                .post(v1_lk_returns.pathCalculatePoint(returnId,point));
        steps.checkResponseWithStatusCodeAndLogView(response, 200);

        String delivery_service_id = getParamJson(JsonPath.read(response.asString(), "$..data..delivery_service_id").toString());
        String delivery_cost = getParamJson(JsonPath.read(response.asString(), "$..data..delivery_cost").toString());
        String tariff_id = getParamJson(JsonPath.read(response.asString(), "$..data..tariff_id").toString());
        return getUpdateDeliveryJson(spec,delivery_service_id,address,delivery_cost,tariff_id,point);
    }
    @Step("Обновить доставку")
    public String updateDelivery(RequestSpecification spec, String returnId)
    {
        String point = getDeliveriesPoints(spec);
        String address = getAddressDeliveryPoint(spec,point);
        String dataDelivery = calculateDelivery(spec,address,returnId,point);

        Response response = given()
                .spec(spec)
                .body(dataDelivery)
                .post(v1_lk_returns.pathUpdateDelivery(returnId));
        steps.checkResponseWithStatusCodeAndLogView(response, 200);

        String addressPoint = getParamJson(JsonPath.read(response.asString(), "$..data..address").toString());
        return addressPoint;
    }
    @Step("Отправка запроса на возврат")
    public void sendReturn(RequestSpecification spec, String returnId)
    {
        Response response = given()
                .spec(spec)
                .post(v1_lk_returns.pathSendReturn(returnId));
        steps.checkResponseWithStatusCodeAndLogView(response, 200);
    }

    public String getUpdateDeliveryJson(RequestSpecification spec, String delivery_service_id, String address, String delivery_cost,String tariff_id,String point_id){
        String resp = profileApiSteps.getProfile(spec);
        String last_name = getParamJson(JsonPath.read(resp, "$..last_name").toString());
        String first_name = getParamJson(JsonPath.read(resp, "$..first_name").toString());
        String name = getClearString(last_name) + " " + getClearString(first_name);
        String phone = getParamJson(JsonPath.read(resp, "$..phone").toString());

        String textBlock = getResource("src/test/resources/apiJson/returns/updateDelivery.json");
        textBlock = textBlock
                .replace("__delivery_service_id", delivery_service_id)
                .replace("__delivery_cost", delivery_cost)
                .replace("__return_from_address", address)
                .replace("__delivery_tariff_id", tariff_id)
                .replace("__sender_name", name)
                .replace("__sender_phone", phone)
                .replace("__point_id", point_id);
        return textBlock;
    }

}
