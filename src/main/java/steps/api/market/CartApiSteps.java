package steps.api.market;

import api_model.request.market.cart.ReqCartItem;
import api_model.response.market.cart.RespCartItem;
import api_model.response.market.checkoutCommit.RespCheckoutCommit;
import com.jayway.jsonpath.JsonPath;
import env.Base_api;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.Map;

import static constants.end_point.MarketEndpoint.*;
import static constants.end_point.MerchantPanelEndpoint.delete_cart_all;
import static helpers.api.ApiHelper.clearQuery;
import static io.qameta.allure.Allure.step;
import static io.restassured.RestAssured.given;

public class CartApiSteps extends Base_api {
    int idProduct = 1;
    String bodyForOrder;
    ReceiversApiSteps receiversApiSteps = new ReceiversApiSteps();

    public void addProductsInCartApi(RequestSpecification spec,int id,int count, int storeId) {
        step("Добавляем товар в корзину", () -> {
            ReqCartItem body = ReqCartItem.builder().count(count).offerId(id).referrerCode("").storeId(storeId).build();

            Response response = given()
                    .spec(spec)
                    .body(body)
                    .post(v1_cart_item.path());
            steps.checkResponseWithStatusCodeAndLogView(response, 200);
        });
    }

    public Map<String, String> checkoutCommit(RequestSpecification spec,String body) {
        Map<String, String> ids = new HashMap<String, String>();
        step("Делаем запрос на оформления заказа", () -> {

            Response response = given()
                    .spec(spec)
                    .body(body)
                    .post(v1_checkout_commit.path());
            RespCheckoutCommit resp = steps.checkResponseWithStatusCodeAndLogView(response, 200).as(RespCheckoutCommit.class);

            int idOrder = resp.getOrderId();
            String numberOrder = resp.getOrderNumber();
            ids.put("idOrder", String.valueOf(idOrder));
            ids.put("numberOrder", numberOrder);

        });
        return ids;
    }


    public String checkDataCheckout(RequestSpecification spec, String paymentMethod) {
        step("Получаем все данные по товару и доставке ", () -> {

            String response = given()
                    .spec(spec)
                    .get(v1_checkout_data.path()).then()
                    .assertThat().statusCode(200)
                    .extract()
                    .asString();

            String recipients = clearQuery(JsonPath.read(response, "$..recipients").toString());
            String addresses = clearQuery(JsonPath.read(response, "$..addresses").toString());
            String inputDeliveryType = clearQuery(JsonPath.read(response, "$..input.deliveryType").toString());
            String inputAddresses = clearQuery(JsonPath.read(response, "$..input.address").toString());
            String receiver = receiversApiSteps.getReceiversForCheckout(spec);
            String input = getJsonInput(paymentMethod, receiver,inputAddresses,inputDeliveryType);
            bodyForOrder = getJsonCheckout(recipients,addresses,input);


        });
        return bodyForOrder;
    }

    public void clearCartApi(RequestSpecification spec) {
        step("Удаляем все данные из корзины юзера", () -> {

            Response response = given()
                    .spec(spec)
                    .basePath(delete_cart_all.path())
                    .delete();
            steps.checkResponseWithStatusCodeAndLogView(response, 200);
        });
    }
    public String getJsonCheckout(String recipients, String addresses, String input){
        String textBlock = getResource("src/test/resources/apiJson/commitData.json");
        textBlock = textBlock
                .replace("__recipients", recipients)
                .replace("__addresses", addresses)
                .replace("__input", input);
        return textBlock;
    }
    public String getJsonInput(String paymentMethodID, String recipient, String address, String deliveryType){
        String textBlock = getResource("src/test/resources/apiJson/inputBlockForCheckout.json");
        textBlock = textBlock
                .replace("__paymentMethodID", paymentMethodID)
                .replace("__recipient", recipient)
                .replace("__address", address)
                .replace("__deliveryType", deliveryType);
        return textBlock;
    }


    private String getResource(String name) {
        try {
            return new String(Files.readAllBytes(Paths.get(name)));
        } catch (IOException ex) {
        }
        return "";
    }

}
