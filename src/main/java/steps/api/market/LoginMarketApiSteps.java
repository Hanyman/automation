package steps.api.market;

import api_model.request.market.LoginClient;
import env.Base_api;
import io.qameta.allure.Step;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;

import javax.annotation.CheckReturnValue;

import static constants.end_point.MarketEndpoint.*;
import static io.qameta.allure.Allure.step;
import static io.restassured.RestAssured.given;
import static org.testng.AssertJUnit.assertEquals;

public class LoginMarketApiSteps extends Base_api {

    @CheckReturnValue
    @Step("Логинимся под клиентом в витрину")
    public void loginClient(RequestSpecification spec,String phone, String password) {

        step("Выполняем запрос на получение смс", () -> {
            LoginClient bodySendSms = LoginClient.builder().phone(phone).build();
            // Запрос смс
            Response responseSendSms = given()
                    .spec(spec)
                    .body(bodySendSms)
                    .post(v1_auth_loginbyphone_sendsms.path());
            steps.checkResponseWithStatusCodeAndLogView(responseSendSms, 200);
        });

        step("Выполняем запрос с помощью смс и номера", () -> {
            LoginClient bodyCheckCode = LoginClient.builder().phone(phone).code(password).build();
            // Запрос с номером и смс
            Response responseCheckCode = given()
                    .spec(spec)
                    .body(bodyCheckCode)
                    .post(v1_auth_loginbyphone_checkcode.path());
            steps.checkResponseWithStatusCodeAndLogView(responseCheckCode, 204);
        });

        // Проверка авторизации true
        step("Првоерка корректной авторизации", () -> {
            JsonPath response = given()
                    .spec(spec)
                    .get(v1_auth_is_login.path()).then().assertThat().statusCode(200)
                    .extract().jsonPath();

            assertEquals("The parameter value must be correct.", "true", response.getString("is_login"));
        });
    }

    @CheckReturnValue
    @Step("Делаем логаут клиента")
    public void logoutClient(RequestSpecification spec) {

        step("Выполняем запрос на logout", () -> {
            // Запрос смс
            Response response = given()
                    .spec(spec)
                    .post(v1_auth_logout.path());
            steps.checkResponseWithStatusCodeAndLogView(response, 204);
        });
    }
    @CheckReturnValue
    @Step("Делаем логаут клиента")
    public void logoutClientWithoutVerifyStatusCode(RequestSpecification spec) {

            given()
                    .spec(spec)
                    .post(v1_auth_logout.path());
    }

}
