package steps.api.market;

import api_model.request.market.address.Address;
import api_model.request.market.address.ReqAddAddress;
import api_model.response.market.profile.GetProfileClient;
import com.jayway.jsonpath.JsonPath;
import env.Base_api;
import io.qameta.allure.Step;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;

import static constants.end_point.MarketEndpoint.*;
import static helpers.Helper.getResource;
import static helpers.api.ApiHelper.getClearString;
import static helpers.api.ApiHelper.getParamJson;
import static io.restassured.RestAssured.given;

public class AddressApiSteps extends Base_api {

    @Step("Устанавливаем дефолтный аддресс")
    public int setDefaultAddress(RequestSpecification spec) {
        String body = getAddressJson("null","RU","г Москва","0c5b2444-70a0-4932-980c-b4dc0d3f02b5",
                "null","null","ул Советская","д 9","стр 24","null","Для курьера коммент","55.7963781","37.7460268","105187");
        Response response = given()
            .spec(spec)
            .body(body)
            .put(v1_lk_address.path());

        steps.checkResponseWithStatusCodeAndLogView(response, 201);
        int idAddress = Integer.parseInt(getParamJson(JsonPath.read(response.asString(), "$..id").toString()));
        setDefaultAddresses(spec, idAddress);
        return idAddress;
    }
    @Step("Устанавливаем аддресс")
    public int setAddress(RequestSpecification spec, String street, String house,String block, String geo_lat, String geo_lon, String post_index) {
        ReqAddAddress body = ReqAddAddress.builder().address(Address.builder()
                .id(null).country_code("RU").region("г Москва").region_guid("0c5b2444-70a0-4932-980c-b4dc0d3f02b5").area(null).area_guid(null)
                .city("г Москва").city_guid("0c5b2444-70a0-4932-980c-b4dc0d3f02b5").street(street).house(house).block(block)
                .intercom(null).comment(null).geo_lat(geo_lat).geo_lon(geo_lon).porch("")
                .flat("").floor("").post_index(post_index).build()).build();
        Response response = given()
            .spec(spec)
            .body(body)
            .put(v1_lk_address.path());

        steps.checkResponseWithStatusCodeAndLogView(response, 201);
        int idAddress = Integer.parseInt(getParamJson(JsonPath.read(response.asString(), "$..id").toString()));
        //setDefaultAddresses(spec, idAddress);
        return idAddress;
    }

    @Step("Получаем список адресов")
    public String getListAddresses(RequestSpecification spec) {

        Response response = given()
            .spec(spec)
            .get(v1_lk_address.path());

        steps.checkResponseWithStatusCodeAndLogView(response, 200);
        return response.asString();
    }

    @Step("Установить дефолтный адресс")
    public void setDefaultAddresses(RequestSpecification spec, int idAddress) {

        Response response = given()
            .spec(spec)
            .put(v1_lk_address.pathAddressDefault(idAddress));

        steps.checkResponseWithStatusCodeAndLogView(response, 204);
    }

    @Step("Удаление всех адресов")
    public void deleteAddresses(RequestSpecification spec) {
        String resp = getListAddresses(spec);
        int lengthReceivers = Integer.parseInt(getParamJson(JsonPath.read(resp, "$..addresses.length()").toString()));
        for(int i=0; i<lengthReceivers; i++) {
            int receiver = Integer.parseInt(getParamJson(JsonPath.read(resp, "$..addresses["+i+"].id").toString()));
            Response response = given()
                    .spec(spec)
                    .delete(v1_lk_address.pathAndEndpoint(receiver));

            steps.checkResponseWithStatusCodeAndLogView(response, 204);

        }

    }

    public String getAddressJson(String id,String countryCode,String city,String guid,String area, String areaGuid,String street,String house
    ,String block, String intercom, String comment, String geoLat,String geoLon, String index){

        String textBlock = getResource("src/test/resources/apiJson/addressMarket.json");
        textBlock = textBlock
                .replace("__id", id)
                .replace("__country_code", countryCode)
                .replace("__region", city)
                .replace("__rguid", guid)
                .replace("__area", area)
                .replace("__aguid", areaGuid)
                .replace("__city", city)
                .replace("__cguid", guid)
                .replace("__street", street)
                .replace("__house", house)
                .replace("__block", block)
                .replace("__intercom", intercom)
                .replace("__comment", comment)
                .replace("__glat", geoLat)
                .replace("__glon", geoLon)
                .replace("__porch", "")
                .replace("__floor", "")
                .replace("__flat", "")
                .replace("__postindex", index);
        return textBlock;
    }
}
