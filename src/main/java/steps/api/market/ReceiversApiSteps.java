package steps.api.market;

import api_model.request.market.address.Address;
import api_model.request.market.address.ReqAddAddress;
import api_model.request.market.receivers.ReqReceivers;
import com.jayway.jsonpath.JsonPath;
import env.Base_api;
import io.qameta.allure.Step;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

import static constants.end_point.MarketEndpoint.*;
import static helpers.api.ApiHelper.getClearString;
import static helpers.api.ApiHelper.getParamJson;
import static io.restassured.RestAssured.given;

public class ReceiversApiSteps extends Base_api {
    ProfileApiSteps profileApiSteps = new ProfileApiSteps();
    @Step("Получаем список получателей")
    public String getListReceivers(RequestSpecification spec) {

        Response response = given()
            .spec(spec)
            .get(v1_lk_receivers.path());

        steps.checkResponseWithStatusCodeAndLogView(response, 200);
        return response.asString();
    }

    @Step("Добавляем получателя")
    public String addNewReceivers(RequestSpecification spec, String firstName,String lastName,String email, String phone) {

        ReqReceivers body = ReqReceivers.builder().first_name(firstName).last_name(lastName).email(email).phone(phone).post("").build();
        Response response = given()
                .spec(spec)
                .body(body)
                .post(receivers.path());

        steps.checkResponseWithStatusCodeAndLogView(response, 200);
        return response.asString();
    }

    @Step("Удаляем всех получаетелй и создаем нового для чекаута")
    public String getReceiversForCheckout(RequestSpecification spec) {
        profileApiSteps.deleteReceivers(spec);
        String resp = getListReceivers(spec);
        int lengthReceivers = Integer.parseInt(getParamJson(JsonPath.read(resp, "$..receivers.length()").toString()));
        if(lengthReceivers==0){
            addNewReceivers(spec,"FirstName", "Surname","testemail@mail.com","+79999999123");
        }
        String respList = getListReceivers(spec);
        String idReceiver = getParamJson(JsonPath.read(respList, "$..receivers[0].id").toString());
        String first_name = getParamJson(JsonPath.read(respList, "$..receivers[0].first_name").toString());
        String last_name = getParamJson(JsonPath.read(respList, "$..receivers[0].last_name").toString());
        String phone = getParamJson(JsonPath.read(respList, "$..receivers[0].phone").toString());
        String email = getParamJson(JsonPath.read(respList, "$..receivers[0].email").toString());
        //String isFile = getParamJson(JsonPath.read(respList, "$..receivers[0].is_file").toString());
        String name = getClearString(first_name) + " " + getClearString(last_name);
        return getReceiverJson(idReceiver,name,phone,email);

    }

    public String getReceiverJson(String id, String name, String phone,String email){
        String textBlock = getResource("src/test/resources/apiJson/receiverForCheckout.json");
        textBlock = textBlock
                .replace("__id", id)
                .replace("__name", name)
                .replace("__phone", phone)
                .replace("__email", email);
        return textBlock;
    }

    private String getResource(String name) {
        try {
            return new String(Files.readAllBytes(Paths.get(name)));
        } catch (IOException ex) {
        }
        return "";
    }
}
