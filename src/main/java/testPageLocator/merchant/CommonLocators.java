package testPageLocator.merchant;

import com.codeborne.selenide.SelenideElement;
import org.openqa.selenium.By;

import static com.codeborne.selenide.Selectors.byAttribute;
import static com.codeborne.selenide.Selectors.byXpath;
import static com.codeborne.selenide.Selenide.$;

public class CommonLocators {

    public final static SelenideElement
            MasPage = $(byXpath("//div[@id='g-mas-pageContent']")),
            StatusMerchant = $(byXpath("//span[contains(@class,'badge')]")),
            StatusMerchantMsg = $(byXpath("//div[@class='shadow']//div[@class='text-danger']")),

            /**Для проверки статус корректировка инфы**.**/
            CorrectionInfStatus = $(byXpath("//div[contains(@class,'alert')]/div[1]")),
            CorrectionInfoMsg = $(byXpath("//div[contains(@class,'alert')]/div[2]")),
            StatusMerchantDisabled = $(byXpath("//div[@class='page-login']//b"));



}
