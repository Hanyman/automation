package testPageLocator.merchant;

import com.codeborne.selenide.SelenideElement;
import org.openqa.selenium.By;

import static com.codeborne.selenide.Selectors.byAttribute;
import static com.codeborne.selenide.Selectors.byXpath;
import static com.codeborne.selenide.Selenide.$;

public class RegisterLocators {
    /**
     * Locators for /registration
     **/
    public final static SelenideElement
            RegistrationPage = $(".page-registration"),
            SurnameField = $(byAttribute("data-test", "last_name")),
            SurnameErrorMsg = $(byAttribute("data-test", "last_name_alert")),
            NameField = $(byAttribute("data-test", "first_name")),
            NameErrorMsg = $(byAttribute("data-test", "first_name_alert")),
            MiddleNameField = $(byAttribute("data-test", "middle_name")),
            MiddleNameErrorMsg = $(byAttribute("data-test", "middle_name_alert")),
            PhoneField = $(byXpath("//*[@data-test='phone']//input")),
            PhoneErrorMsg = $(byAttribute("data-test", "phone_alert")),
            EmailField = $(byAttribute("data-test", "email")),
            EmailErrorMsg = $(byAttribute("data-test", "email_alert")),
            InnField = $(byAttribute("data-test", "inn")),
            InnErrorMsg = $(byAttribute("data-test", "inn_alert")),
            OrganizationField = $(byAttribute("data-test", "legal_name")),
            OrganizationErrorMsg = $(byAttribute("data-test", "legal_name_alert")),
            AddressField = $(byAttribute("data-test", "storage_address")),
            AddressErrorMsg = $(byAttribute("data-test", "storage_address_alert")),
            ProductsFileField = $(byAttribute("data-test", "file_assortment")),
            ProductsFileErrorMsg = $(byAttribute("data-test", "file_assortment_alert")),
            ProductsFileProgressBar = $(By.xpath("//*[@role='progressbar']")),
            ProductsFileSuccess = $(".alert-success"),
            RegistrationBtn = $(By.xpath("//*[@type='submit']")),
            RegistrationText = $(By.xpath("//*[@class='page-registration']/div")),

            /**Locators page set password **/
            PasswordField = $(byAttribute("type", "password")),
            SaveBtn = $(byAttribute("type", "submit"));

    /** Locators info organizations **/
    public final static SelenideElement
            TaxSystemSelect = $(byXpath("//select[@data-test='tax_system']")),
            NameOrganizationField = $(byAttribute("data-test", "legal_name")),
            LegalAddressField = $(byAttribute("data-test", "legal_address")),
            FactAddressField = $(byAttribute("data-test", "fact_address")),
            CeoLastNameField = $(byAttribute("data-test", "ceo_last_name")),
            CeoFirstNameField = $(byAttribute("data-test", "ceo_first_name")),
            CeoMiddleNameField = $(byAttribute("data-test", "ceo_middle_name")),
            KppField = $(byAttribute("data-test", "kpp")),
            PaymentAccountField = $(byAttribute("data-test", "payment_account")),
            BankField = $(byAttribute("data-test", "bank")),
            CorrespondentAccountField = $(byAttribute("data-test", "correspondent_account")),
            BankAddressField = $(byAttribute("data-test", "bank_address")),
            BankBikField = $(byAttribute("data-test", "bank_bik")),
            SuggestionItem = $(By.xpath("//*[@class='suggestions-suggestions']//*[contains(@class,'suggestions-selected')]"));

    /** Locators info organizations Files**/
    public final static SelenideElement
            ProductsRangeFile = $(byAttribute("data-test", "file_input_1")),
            ProductsRangeFileLink = $(byAttribute("data-test", "file_link_1")),
            InnFile = $(byAttribute("data-test", "file_input_2")),
            InnFileLink = $(byAttribute("data-test", "file_link_2")),
            OgrnFile = $(byAttribute("data-test", "file_input_3")),
            OgrnFileLink = $(byAttribute("data-test", "file_link_3")),
            EgrulFile = $(byAttribute("data-test", "file_input_5")),
            EgrulFileLink = $(byAttribute("data-test", "file_link_5")),
            CompanyCharterFile = $(byAttribute("data-test", "file_input_6")),
            CompanyCharterLink = $(byAttribute("data-test", "file_link_6")),
            OrderCeoFile = $(byAttribute("data-test", "file_input_7")),
            OrderCeoLink = $(byAttribute("data-test", "file_link_7"));

    public final static SelenideElement
            AgreeBtn = $(byAttribute("data-test", "agree")),
            StatusActivation = $(".badge-success");

}
