package testPageLocator.general;

import com.codeborne.selenide.SelenideElement;
import org.openqa.selenium.By;

import static com.codeborne.selenide.Selenide.$;

public class GeneralLocators {
    /** Locator for page admin /login /**/
    public final static SelenideElement
            EmailField = $(By.id("email")),
            PasswordField = $(By.id("password")),
            SigInBtn = $(".btn-primary"),
            PageLogin = $(".page-login");

}
