package testPageLocator.market.orderPage;

import com.codeborne.selenide.Selenide;
import com.codeborne.selenide.SelenideElement;
import org.openqa.selenium.By;


import static com.codeborne.selenide.Selectors.*;
import static com.codeborne.selenide.Selectors.byXpath;
import static com.codeborne.selenide.Selenide.$;

public class CheckoutOrderLocators {
        /**
         * ORDER CARD
         **/
        public final static SelenideElement
                OrderCardTop= $(byClassName("order-card__top")),
                OrderCardLast= $(byXpath("(//li[@class='orders__item'])[1]")),
                OrderDeliveryDate= $(byXpath("//p[@class='order__delivery-date']"));

        /**
         * GENERAL
         **/
        public final static SelenideElement
                LoaderCheckoutInCart=  $(byXpath("//div[@class='loader-dots']")),
                LoaderSpinnerEndCheckout=  $(byXpath("//*[contains(@class,'loader-spinner')]")),
                CheckoutBodyPending=  $(".checkout__body_pending"),
                CheckoutBtn=  $(byXpath("//button[contains(@class,'checkout__confirm-btn')]"));
        /**
         * RECIPIENT
         **/
        public final static SelenideElement
                CheckoutBodyPending22=  $(".checkout__body_pending");
        /**
         * METHOD OF OBTAINING
         **/
        public final static SelenideElement
                ExpressDelivery=  $(byXpath("(//ul[contains(@class,'receive-methods')]/li)[1]")),
                Pickup=  $(byXpath("(//ul[contains(@class,'receive-methods')]/li)[2]")),
                PickupBlock=  $(byXpath("//div[contains(@class,'receive-pickup')]")),
                PickupSelectBtn=  $(byXpath("//div[@class='receive-pickup__empty-pickup']/button")),
                PickupModal=  $(byXpath("//div[@class='modal modal-address address-pickup-modal']")),
                PickupPointItemFirst=  $(byXpath("(//ul[@class='checkout-option pickup-points']/li[contains(@class,'pickup-points__item')])[1]")),
                PickupModalAddress=  $(byXpath("(//div[@class='pickup-point']//h3[contains(@class,'pickup-point__info-main-title')])[last()]")),
                PickupModalSelectBtn=  $(byXpath("//button[@class='button address-pickup-modal__point-btn button--sm']"));


        /**
         * Locators for add order in market
         **/
        public final static SelenideElement
                CheckoutCartBtn=  $(byXpath("//button[contains(@class,'cart__checkout-link')]")),
                AddressBlock =  $(".address-edit__bottom"),
                AddressField =  $(byAttribute("data-test", "checkout-delivery-address")),
                IndexField =  $(byAttribute("data-test", "checkout-address-index")),
                ContinueBtn =  $(byAttribute("data-test", "checkout-delivery-next")),
                AddressItem = $(byXpath("//*[@class='address-selection__list']/li[1]")),
                SuccessOrder = $(byXpath("//*[contains(@class,'thank-you__title')]")),
                NumberOrder = $(".thank-you__side-order-num"),

                /** Доставка курьером блок **/
                ApartmentField = $(byAttribute("data-test", "checkout-address-flat")),
                EntranceField = $(byAttribute("data-test", "checkout-address-porch")),
                FloorField = $(byAttribute("data-test", "checkout-address-floor")),
                IntercomField = $(byAttribute("data-test", "checkout-address-intercom")),
                CommentsField = $(byAttribute("data-test", "checkout-address-comment")),
                NameField = $(byAttribute("data-test", "checkout-recipient-name")),
                PhoneField = $(byAttribute("data-test", "checkout-recipient-phone")),
                CheckoutNextBtn = $(".checkout__next"),


                /** Страница с данными по заказу **/
                DeliveryBlock = $(".checkout-main"),
                NextOrderBtn = $(byXpath("//*[contains(@class,'button checkout__accept-button')]")),

                /** Страница оплаты **/
                PaymentBlock = $(".payment-confirmation-container__body"),
                BackToStoreBtn = $(byXpath("//span[contains(@class,'BackShopLink')]")),
                BackToStoreAuthBtn = $(".payment-success__back-control"),
                PayBtn = $(".payment-submit__forward-button"),
                PayCardNumberField = $(byId("cardNumber")),
                PayCardMonthField = $(byAttribute("name", "skr_month")),
                PayCardYearField = $(byAttribute("name", "skr_year")),
                PayCardCvcField = $(byAttribute("name", "skr_cardCvc")),
                SuccessPaymentField = $(".title_last_yes"),
                CardAuthenticationBlock = $(".panel panel-primary"),
                CardAuthenticationCvc = $(byId("cvc")),
                CardAuthenticationConfirmBtn = $(byId("submitcvc")),

                /** Страница заказов **/
                LastOrderCard = $(byXpath("//*[@class='order-card'])[1]")),
                CancelOrderBtn = $(byXpath("//button[@class='button order__action button--simple button--full']")),
                LoaderSpinner = $(byXpath("//div[@class='modal__body']//div[@class='loader-spinner loader-spinner--simple']")),
                SendBtn = $(byClassName("selection-form__submit")),
                OrderStatus = $(byXpath("//ul[@class='product-tags']//li")),

                /** Чекаут блок подтверждения **/
                CheckoutConfirmation = $(byXpath("(//input[@id='checkout-confirmation-condition'])[1]")),
                CheckoutConfirmationBlock= $(byXpath("//div[@class='checkout-confirmation__condition-check']")),

                /** Блок после оплаты **/
                ViewOrderBtn = $(byXpath("//a[@class='button thank-you__button']")),
                ExitPaymentProcessBtn = $(byXpath("//span[contains(text(),'Exit the payment process')]")),


                RecipientMsg = $(byXpath("//div[@class='personal-info']//div[@class='personal-info__wrapper']/div")),
                AddressMsg = $(byXpath("//div[@class='personal-info']//div[@class='personal-info__wrapper']/div")),
                CheckoutConfirmationMsg = $(byXpath("//div[@class='checkout-confirmation__condition-check']//div[contains(@class,'agreement-error')]")),
                AddRecipientCheckout = $(byXpath("//button[contains(@class,'checkout-recipient__info-btn button--default')]")),
                EditRecipientCheckout = $(byXpath("//li[contains(@class,'checkout-recipient__item')]//button[contains(@class,'checkout-option-item__right-link')]")),
                AddAddressCheckout = $(byXpath("(//div[@class='checkout__products']//button[@class='button button--default'])[1]")),
                AddDateCheckout = $(byXpath("(//div[@class='checkout__products']//button[@class='button button--default'])[2]")),

                /** Получатели блок**/
                RecipientName = $(byXpath("(//div[@class='personal-info']//li[contains(@class,'checkout-recipient__item')]//p)[1]")),
                RecipientPhone = $(byXpath("(//div[@class='personal-info']//li[contains(@class,'checkout-recipient__item')]//p)[2]")),
                RecipientEmail = $(byXpath("(//div[@class='personal-info']//li[contains(@class,'checkout-recipient__item')]//p)[3]")),
                /** Адрес доставки блок**/
                DeliveryAddress = $(byXpath("//div[@class='checkout__products']//ul[contains(@class,'delivery-address')]//div[@class='checkout-option-item__left']"));

        public static SelenideElement PersonalInfoRecipient(String name){
                return Selenide.$(By.xpath("//div[@class='personal-info']//ul[contains(@class,'checkout-recipient')]//p[contains(text(),'"+name+"')]"));
        }
        public static SelenideElement PersonalInfoRecipientIsSelected(String name){
                return Selenide.$(By.xpath("//div[@class='personal-info']//ul[contains(@class,'checkout-recipient')]//p[contains(text(),'"+name+"')]//ancestor::li"));
        }
        public static SelenideElement statusSuccessOrder(String numberOrder){
                return Selenide.$(By.xpath("//*[@class='order-card__title'][contains(text(),'"+numberOrder+"')]//ancestor::a//*[@class='product-tags__item product-tags__item--yellow']"));
        }
        public static SelenideElement statusUnpaymentOrder(String numberOrder){
                return Selenide.$(By.xpath("//*[@class='order-card__title'][contains(text(),'"+numberOrder+"')]//ancestor::a//*[@class='product-tags__item']"));
        }
        public static SelenideElement statusCancelOrder(String numberOrder){
                return Selenide.$(By.xpath("//*[@class='order-card__title'][contains(text(),'"+numberOrder+"')]//ancestor::a//*[contains(@class,'product-tags__item product-tags__item--light-red')]"));
        }
        public static SelenideElement cardOrder(String numberOrder){
                return Selenide.$(By.xpath("//h2[contains(@class,'order-card__title')][contains(text(),'"+numberOrder+"')]//ancestor::a"));
        }

        public static SelenideElement checkoutPayment(String payment){
                return Selenide.$(By.xpath("//ul[contains(@class,'checkout-payment')]//li[contains(@class,'checkout-option-item')]//div[contains(text(),'"+payment+"')]"));
        }


}
