package testPageLocator.market.mainPage;

import com.codeborne.selenide.Selenide;
import com.codeborne.selenide.SelenideElement;
import org.openqa.selenium.By;

import static com.codeborne.selenide.Selectors.*;
import static com.codeborne.selenide.Selectors.byXpath;
import static com.codeborne.selenide.Selenide.$;

public class MainLocators {
    /**
     * Locators main page market
     **/
    public final static SelenideElement
            LogInBtn = $(byAttribute("data-test", "log-in")),
            LogOutBtn = $(byAttribute("data-test", "profile-menu-logout")),
            ProductsMenu =  $(byXpath("//li[@class='menu__item']//*[contains(text(),'Каталог')]")),
            Cart =  $(byXpath("//*[@class='layout-with-bar cart']")),
            SearchField =  $(byXpath("//*[contains(@class,'search__input')]")),
            CartBtn =  $(byXpath("//*[contains(@class,'header-user__cart')]//*[@class='header-user__cart-count']")),
            UserMenu =  $(byXpath("//*[contains(@class,'header-user__account')]")),
            ProfileBtn = $(byXpath("//a[contains(@class,'header-user__client')]")),
            LogoutBtn = $(byAttribute("data-test", "profile-menu-logout")),
            ProductsArea = $(byXpath("//*[@class='search__overlay']")),
            OrdersBtn = $(byAttribute("data-test", "profile-menu-orders")),
            pageLoadingBar = $(By.xpath("//div[@class='nuxt-progress']")),
            cartLoader = $(By.xpath("//div[@class='loader-dots cart__loader']"));
    public static SelenideElement  ProfileMenuSelectActive(String href){
        return Selenide.$(byXpath("//a[contains(@href,'"+href+"')]//ancestor::li[contains(@class,'navigation-panel__section-item_active')]"));
    }

}
