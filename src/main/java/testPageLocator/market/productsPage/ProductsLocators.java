package testPageLocator.market.productsPage;

import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.Selenide;
import com.codeborne.selenide.SelenideElement;
import org.openqa.selenium.By;

import static com.codeborne.selenide.Selectors.byClassName;
import static com.codeborne.selenide.Selectors.byXpath;
import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.$$;

public class ProductsLocators {
    /**
     * Locators for card product in search
     **/
    public final static SelenideElement
            AddCartBtn = $(byXpath("(//*[contains(@class,'product-card__add-to-cart')])[1]")),
            PlusBtn = $(".quantity-counter__btn-plus"),
            MinusBtn = $(".quantity-counter__btn-minus"),
            DeleteProductBtn = $(".cart__product-remove"),
            CartEmpty = $(".cart__empty-wrapper"),
            CountProductsCartBtn = $(".quantity-counter__val"),
            CountProducts = $(".header-user__cart-count"),
            GridProductsList = $(byXpath("//*[@class='product-grid catalog-page__products']")),
            LoaderProductsList = $(byXpath("//div[@class='loader-dots']")),
            SearchProductsList = $(byXpath("//*[contains(@class,'search-result__products')]"));

    public static SelenideElement  AddCartProductBtn(String nameProduct){
        return Selenide.$(byXpath("//h3[contains(@class,'product-card__name')][contains(text(),'"+nameProduct+"')]//ancestor::li//button[contains(@class,'product-card__btn')]"));
    }
    public static SelenideElement  ProductCard(String nameProduct){
        return Selenide.$(byXpath("//*[contains(@class,'product-card__name')][contains(text(),'"+nameProduct+"')]//ancestor::article[@class='product-card']"));
    }

    public static ElementsCollection
            NamesProducts = $$(By.xpath("//*[@class='product-grid__list']//h3[contains(@class,'product-card__name')]"));
    public static ElementsCollection
            SearchResultProducts = $$(By.xpath("//*[contains(@class,'search-result__products')]//div[@class='search-products__item']"));

    /**PRODUCT CARD**/
    public final static SelenideElement
            ProductPage = $(byXpath("//*[contains(@class,'product-page')]")),
            BuyBtn = $(byXpath("//button[contains(@class,'sku-add-cart__button')]")),
            InCartBtn = $(byXpath("//a[contains(@class,'sku-in-cart__button')]"));


}
