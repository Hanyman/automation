package testPageLocator.market;

import com.codeborne.selenide.Selenide;
import com.codeborne.selenide.SelenideElement;
import org.openqa.selenium.By;

import static com.codeborne.selenide.Selectors.*;
import static com.codeborne.selenide.Selenide.$;

public class ReturnsPageLocators {

        /**
         * RECIPIENT
         **/
        public final static SelenideElement
                OrderReturnButton=  $(byXpath("//button[contains(@class,'order-return__button')]")),
                ReasonsDropdown=  $(byXpath("//div[@class='return-update-item__controls']//div[@class='vs__selected-options']")),
                ProblemDescriptionTextarea=  $(byXpath("//div[@class='return-update-item__controls']//textarea")),
                inputFile=  $(byXpath("//input[@type='file']")),
                dropzoneProgress=  $(byXpath("//div[@class='dropzone__progress']/span[@class='dropzone__upload']")),
                UpdateReturnBtn=  $(byXpath("//button[contains(@class,'update-return__button')]")),
                CreateReturnBtn=  $(byXpath("//button[contains(@class,'create-return__button')]")),
                ConfirmFinishReturnBtn=  $(byXpath("//button[contains(@class,'return-confirm__finish')]")),
                ExpressDeliveryBtn=  $(byXpath("(//div[@class='return-delivery-methods__content']//div[@class='tabs__nav-item'])[1]")),
                CreateReturnForm=  $(byXpath("//section[@id='create-return-form']")),
                ReturnTimeslotsBlock=  $(byXpath("//div[@class='return-timeslots']")),
                OrderReturnSection=  $(byXpath("//section[@class='order-return']")),
                ReasonErrorMsg=  $(byXpath("//div[@class='return-update-item__controls']//p[@class='custom-select__message']")),
                ReturnLinkEdit=  $(byXpath("//a[@class='update-return__link-edit']")),
                AlertText=  $(byXpath("//p[@class='alert__text alert__title']"));
        /**
         * METHOD OF OBTAINING
         **/
        public final static SelenideElement
                AddressInput=  $(byXpath("//div[@class='return-delivery-form']//div[@class='return-address-edit__search']//input")),
                AddressSuggestionItem=  $(byXpath("//div[@class='return-delivery-form']//li[@class='return-address-edit__suggestion-item']"));


        public static SelenideElement ReturnOrderBtn(String numberOrder){
                return Selenide.$(By.xpath("//h2[contains(@class,'order-card__title')][contains(text(),'"+numberOrder+"')]//ancestor::article//a[contains(text(),'Вернуть товары')]"));
        }
        public static SelenideElement ReturnCheck(String numbProduct){
                return Selenide.$(By.xpath("//input[@id='return-check-"+numbProduct+"']"));
        }
        public static SelenideElement ReasonsDropdownItem(String reason){
                return Selenide.$(By.xpath("//div[@class='return-update-item__controls']//ul/li[contains(text(),'"+reason+"')]"));
        }

}
