package testPageLocator.market.authPage;
import com.codeborne.selenide.*;
import org.openqa.selenium.By;

import static com.codeborne.selenide.Selectors.*;
import static com.codeborne.selenide.Selectors.byAttribute;
import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.$$;

public class AuthLocators {
    /**
     * Locators for auth users in market
     **/
    public final static SelenideElement
            MainPage = $(".sign-up--main"),
            SignUpBtn = $(byXpath("//*[@data-test='registration']")),
            LoginField = $(byXpath("//input[@class='input-tel__input']")),
            TermsAgreementCheckbox = $(byXpath("//input[@id='termsAgreement']")),
            TermsAgreementBlock= $(byXpath("//div[@class='sign-up__form-agreement-body']")),
            GetCodeSms = $(byXpath("//button[@data-test='get-code']")),
            SmsField = $(byXpath("//input[@data-test='fill-code']")),
            ConfirmBtn = $(byXpath("//button[@data-test='confirmation']")),
            PhoneErrorMsg = $(".tel-input__error"),
            SmsErrorMsg = $(".input-mask__message"),
            AlertText=  $(byXpath("//div[contains(@class,'alert__text')]/p"));

    /**
     * Locators for social network
     **/
    public final static SelenideElement
            FacebookBtn = $(byXpath("(//*[contains(@class,'button--facebook')])[last()]")),
            GmailBtn = $(byXpath("(//*[@class='sign-in-buttons']//button)[1]")),
            VKBtn = $(byXpath("(//*[@class='sign-in-buttons']//button)[2]")),
            VKForm = $(By.id("oauth_wrap_content")),
            FacebookForm = $(By.id("facebook")),
            GmailForm = $(By.xpath("//*[@id='initialView']"));

    /**
     * Locators for choose city in market after auth user
     **/
    public final static SelenideElement
            LocationPage = $(".location"),
            ConfirmCity = $(byAttribute("data-test", "city-true")),
            CountrySelector = $(By.xpath("//div[@class='select-country-container']//div[@class='country-selector__toggle']")),
            CountrySelectorList = $(By.xpath("//div[@class='country-selector__list']")),
            RejectCity = $(byAttribute("data-test", "city-false"));

    public static ElementsCollection
            SelectCountry = $$(By.xpath("//div[@class='select-country-container']//div[@class='dots-text']"));

}
