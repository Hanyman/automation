package testPageLocator.market.profilePage;

import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selectors.byXpath;
import static com.codeborne.selenide.Selenide.$;

public class MessagesLocators {
    public final static SelenideElement

        AddNewMessage = $(byXpath("//div[@class='profile-messages__controls']/button")),
        TitleMessage = $(byXpath("//div[contains(@class,'message-item__title-item')]")),
        BodyMessage = $(byXpath("//div[contains(@class,'message-item__message')]")),
        SubjectMessageField = $(byXpath("//div[contains(@class,'profile-messages__input profile-messages__subject')]//input")),
        BodyMessageField = $(byXpath("//div[contains(@class,'profile-messages__input profile-messages__message')]//textarea")),
        AcceptBtn = $(byXpath("//button[contains(@class,'profile-messages__accept-button')]")),
        CloseMessageBtn = $(byXpath("//button[contains(@class,'profile-messages__close-button')]")),
        AlertMsg = $(byXpath("//p[contains(@class,'profile-messages__alert-text')]"));
}
