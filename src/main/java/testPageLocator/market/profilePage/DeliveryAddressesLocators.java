package testPageLocator.market.profilePage;

import com.codeborne.selenide.Selenide;
import com.codeborne.selenide.SelenideElement;
import org.openqa.selenium.By;

import static com.codeborne.selenide.Selectors.byAttribute;
import static com.codeborne.selenide.Selectors.byXpath;
import static com.codeborne.selenide.Selenide.$;

public class DeliveryAddressesLocators {
    public final static SelenideElement

        AddNewAddress = $(byXpath("//button[contains(@class,'personal-form__submit button--full')]")),
        ModalAddressMap = $(By.className("modal-address__map")),

        /**Address delivery**/
        AddressField = $(byXpath("//label[contains(text(),'Введите адрес')]//ancestor::div[1]//input")),
        AddressSuggestionsDropDown = $(byXpath("//div[@class='suggestions']")),
        IndexField = $(byXpath("//label[contains(text(),'Индекс')]//ancestor::div[1]//input")),
        StreetField = $(byXpath("//label[contains(text(),'Улица')]//ancestor::div[2]//input[@type='text']")),
        HomeField = $(byXpath("//label[contains(text(),'корпус')]//ancestor::div[2]//input[@type='text']")),
        ConstructionField = $(byXpath("//label[contains(text(),'Строение')]//ancestor::div[1]//input[@type='text']")),
        ApartmentField = $(byXpath("//label[contains(text(),'Офис')]//ancestor::div[1]//input[@type='text']")),
        FloorField = $(byXpath("//label[contains(text(),'Этаж')]//ancestor::div[1]//input[@type='number']")),
        EntranceField = $(byXpath("//label[contains(text(),'Подъезд')]//ancestor::div[1]//input[@type='number']")),
        IntercomField = $(byXpath("//label[contains(text(),'Домофон')]//ancestor::div[1]//input[@type='text']")),
        CommentDorDeliveryField = $(byXpath("//textarea[@data-test='checkout-address-comment']")),
        DeliveryBtn = $(byXpath("//button[contains(@class,'personal-form__submit')]")),

        /**Address delivery**/
        AddressCardTitle = $(byXpath("//div[@class='address-card addresses-cards__item']/p[@class='address-card__title']")),
        AddressCardTitleLast = $(byXpath("(//div[@class='addresses-cards']//p[@class='address-card__title'])[last()]")),
        /**Page addresses**/
        AddAddressBtn = $(byXpath("//button[contains(@class,'addresses__add-button')]"))

                ;

    public static SelenideElement  AddressCard(String address){
        return Selenide.$(byXpath("//div[@class='addresses-cards']//p[@class='address-card__title'][contains(text(),'"+address+"')]"));
    }
    public static SelenideElement  AddressCardSelected(String address){
        return Selenide.$(byXpath("//div[@class='addresses-cards']//p[@class='address-card__title'][contains(text(),'"+address+"')]//ancestor::div[1]//div[@class='address-card__right-selected']"));
    }
    public static SelenideElement  AddressCardEditBtn(String address){
        return Selenide.$(byXpath("//div[@class='addresses-cards']//p[@class='address-card__title'][contains(text(),'"+address+"')]//ancestor::div[1]//*[contains(@class,'address-card__right-control-edit')]"));
    }
    public static SelenideElement  AddressCardDeleteBtn(String address){
        return Selenide.$(byXpath("//div[@class='addresses-cards']//p[@class='address-card__title'][contains(text(),'"+address+"')]//ancestor::div[1]//*[contains(@class,'address-card__right-control-close')]"));
    }
    public static SelenideElement  suggestionsAddress(){
        return Selenide.$(byXpath("(//div[@class='suggestions']//li[@class='item'])[1]"));
    }
    public static SelenideElement  SuggestionsHomeNumber(){
        return Selenide.$(byXpath("(//div[@class='suggestions']//li[@class='item'])[1]"));
    }
}
