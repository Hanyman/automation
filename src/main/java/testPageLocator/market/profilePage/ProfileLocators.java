package testPageLocator.market.profilePage;

import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selectors.*;
import static com.codeborne.selenide.Selectors.byXpath;
import static com.codeborne.selenide.Selenide.$;

public class ProfileLocators {
    /**
     * Locators fors user menu- personal data in market
     **/
    public final static SelenideElement
            NameField = $(byAttribute("data-test", "profile-full-name")),
            NameErrorMsg = $(byXpath("//*[@data-test='profile-full-name']//ancestor::div[2]//*[contains(@class,'input__message')]")),
            PhoneField = $(byXpath("//input[@class='input-tel__input']")),
            PhoneErrorMsg = $(byXpath("//*[@data-test='profile-phone']//ancestor::div[1]//*[contains(@class,'input__message')]")),
            EmailField = $(byAttribute("data-test", "profile-email")),
            EmailErrorMsg = $(byXpath("//*[@data-test='profile-email']//ancestor::div[2]//*[contains(@class,'input__message')]")),
            DateField = $(byXpath("//*[@data-test='profile-datepicker']//input")),
            ConfirmBtn =  $(".profile__form-confirm-button"),
            ProfileReadyBtn =  $(byXpath("//*[contains(@class,'personal-info__centered')]/button")),
            AlertField = $(byXpath("//*[contains(@class,'alert--success')]"));
}
