package testPageLocator.market.profilePage;

import com.codeborne.selenide.Selenide;
import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selectors.byXpath;
import static com.codeborne.selenide.Selenide.$;

public class PreferencesLocators {
    /**
     * Locators fors user menu- personal data in market
     **/
    public final static SelenideElement

            /**Block Brands**/
            PreferencesAddModal = $(byXpath("//div[@class='preferences-add-modal']")),
            AddBrandsMiddleBtn = $(byXpath("(//button[@class='button'])[1]")),
            AddBrandsRightBtn = $(byXpath("(//button[contains(@class,'preferences__add-button')])[1]")),
            DeleteAllBrandsBtn = $(byXpath("(//button[contains(@class,'preferences__remove-button')])[1]")),
            SearchBrandsField = $(byXpath("(//input[@class='preferences-add-modal__search-input'])[1]")),
            CloseModalBrandsBtn = $(byXpath("(//button[@class='preferences-add-modal__close-button'])[1]")),
            AddBrandsBtnModal = $(byXpath("(//button[contains(@class,'preferences-add-modal__accept-button')])[1]")),
            PreferencesBrandModal = $(byXpath("(//div[@class='profile-wrapper']//*[@class='modal'])[1]")),


            /**Block Categories**/
            PreferencesAddModalCategories = $(byXpath("(//div[@class='preferences-add-modal'])[2]")),
            AddCategoriesMiddleBtn = $(byXpath("(//button[@class='button'])[2]")),
            AddCategoriesRightBtn = $(byXpath("(//button[contains(@class,'preferences__add-button')])[2]")),
            DeleteAllCategoriesBtn = $(byXpath("(//button[contains(@class,'preferences__remove-button')])[2]")),
            SearchCategoriesField = $(byXpath("(//input[@class='preferences-add-modal__search-input'])[2]")),
            CloseModalCategoriesBtn = $(byXpath("(//button[@class='preferences-add-modal__close-button'])[2]")),
            AddCategoriesBtnModal = $(byXpath("(//button[contains(@class,'preferences-add-modal__accept-button')])[2]")),
            PreferencesCategoriesModal = $(byXpath("(//div[@class='profile-wrapper']//*[@class='modal'])[2]"))
            ;

    public static SelenideElement  BrandCheckbox(String brand){
        return Selenide.$(byXpath("//div[@class='preferences-add-modal']//input[contains(@id,'brand')][contains(@name,'"+brand+"')]//ancestor::div[1]/label"));
    }

    public static SelenideElement  CategoryCheckbox(String brand){
        return Selenide.$(byXpath("//div[@class='preferences-add-modal']//input[contains(@id,'category')][@name='"+brand+"']//ancestor::div[1]/label"));
    }
    public static SelenideElement  CategoryFewCheckbox(String brand){
        return Selenide.$(byXpath("//div[@class='preferences-add-modal']//input[contains(@id,'category')][contains(@name,'"+brand+"')]//ancestor::div[1]/label"));
    }
    public static SelenideElement PreferenceItem(String value){
        return Selenide.$(byXpath("//div[@class='preferences__elements-item']//*[contains(text(),'"+value+"')]"));
    }
    public static SelenideElement PreferenceItem(){
        return Selenide.$(byXpath("//div[@class='preferences__elements-item']/div"));
    }
    public static SelenideElement PreferenceItemRemoveBtn(String value){
        return Selenide.$(byXpath("//div[@class='preferences__elements-item']//*[contains(text(),'"+value+"')]//ancestor::div[1]//*[@class='preferences__elements-remove-button']"));
    }
}
