package testPageLocator.market.profilePage;

import com.codeborne.selenide.Selenide;
import com.codeborne.selenide.SelenideElement;
import org.openqa.selenium.By;

import static com.codeborne.selenide.Selectors.byAttribute;
import static com.codeborne.selenide.Selectors.byXpath;
import static com.codeborne.selenide.Selenide.$;

public class ContactsLocators {
    public final static SelenideElement

        AddNewRecipient = $(byXpath("//button[contains(@class,'button--primary')]")),
        RecipientNameField = $(byAttribute("data-test","recipient-name")),
        RecipientNameFieldMsg = $(byXpath("//input[@data-test='recipient-name']//ancestor::div[2]//p[@class='input__message']")),
        RecipientMailField = $(byAttribute("data-test","recipient-mail")),
        RecipientPhoneField = $(byXpath("//div[contains(@class,'recipient-modal')]//input[@class='input-tel__input']")),
        RecipientPhoneFieldMsg = $(byXpath("//div[@data-test='recipient-phone']//div[@class='tel-input__error']")),
        RecipientAddBtn = $(byXpath("//div[contains(@class,'recipient-modal')]//button[contains(@class,'recipient-modal__footer-btn')]")),
        RecipientCloseBtn = $(byXpath("//div[contains(@class,'recipient-modal')]//button[contains(@class,'modal__close')]")),
        RecipientFioRow = $(byXpath("(//tr[@class='custom-table__row']//td[@data-th='ФИО']/div)[last()]")),
        RecipientEmailRow = $(byXpath("(//tr[@class='custom-table__row']//td[@data-th='E-mail']/div)[last()]")),
        RecipientPhoneRow = $(byXpath("(//tr[@class='custom-table__row']//td[@data-th='Телефон']/div)[last()]")),
        RecipientEditBtn = $(byXpath("(//div[contains(@class,'custom-table-edit__item')])[1]")),
        RecipientDeleteBtn = $(byXpath("(//div[contains(@class,'custom-table-edit__item')])[2]")),
        EmptyRecipient = $(byXpath("//td[@class='custom-table__column']"));

    public static SelenideElement EditRecipientRowBtn(String fio){
        return Selenide.$(By.xpath("//tr[@class='custom-table__row']//td[@data-th='ФИО']/div[contains(text(),'"+fio+"')]//ancestor::tr//td[@class='custom-table__column custom-table-edit']"));
    }

}

