package testPageLocator.market.searchAndFilters;

import com.codeborne.selenide.Selenide;
import com.codeborne.selenide.SelenideElement;
import org.openqa.selenium.By;

import static com.codeborne.selenide.Selectors.*;
import static com.codeborne.selenide.Selenide.$;

public class SearchFilterLocators {
    /**
     * Locators for page search in market
     **/
    public final static SelenideElement

        /** Локаторы поиска **/
        EmptyProduct =  $(byClassName("search-empty")),
        CategoriesField =  $(By.xpath("//a[contains(@class,'categories-list__link')][contains(text(),'Книги')]"));

    public static SelenideElement  helpSearchBtn(String nameProduct){
        return Selenide.$(byXpath("//*[contains(@class,'search__suggestion--product')]//*[contains(text(),'"+nameProduct+"')]"));
    }
    public static SelenideElement  productName(String nameProduct){
        return Selenide.$(byXpath("//*[@class='product-card']//*[contains(text(),'"+nameProduct+"')]"));
    }
    public static SelenideElement  ProductCardInSearch(String nameProduct){
        return Selenide.$(byXpath("//*[contains(@class,'search-result__products')]//div[contains(text(),'"+nameProduct+"')]"));
    }
}
