package testPageLocator.admin.MerchantsPage;

import com.codeborne.selenide.Selenide;
import com.codeborne.selenide.SelenideElement;
import org.openqa.selenium.By;

import static com.codeborne.selenide.Selenide.$;

public class PageMerchants {
     /**
      * Locators for merchant/list/registration
      **/
     public void NameCompanyLink(final String text) {
          Selenide.$(By.xpath("//*[@class='table']//*[contains(text(),'" + text + "')]"));
     }

     public final static SelenideElement
             ActivateBtn = $(".btn-outline-success"),
             CreateMerchantBtn = $(".btn-success"),

             SuccessBtn = $(".btn-success"),
             StatusSelect = $(By.xpath("//*[contains(text(),'Статус')]//ancestor::tr//select")),
             SetPasswordField = $(By.xpath("//*[@type='password']")),
             SetPasswordSaveBtn = $(By.xpath("//*[@type='submit']"));


     public static SelenideElement nameCompanyBtn(String nameCompany){
          return Selenide.$(By.xpath("//*[@class='table']//*[contains(text(),'" + nameCompany + "')]"));
     }

     public static SelenideElement statusCompany(String nameCompany){
          return Selenide.$(By.xpath("//*[@class='table']//*[contains(text(),'" + nameCompany + "')]//ancestor::tr//*[contains(@class,'badge')]"));
     }


     /** TABLE MERCHANTS **/
     public final static SelenideElement
             NameMerchantColFirst = $(By.xpath("(//tr/td/a)[1]")),
             StatusMerchantColFirst = $(By.xpath("(//tr/td/span)[1]"));



}
