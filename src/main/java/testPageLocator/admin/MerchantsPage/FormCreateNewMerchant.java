package testPageLocator.admin.MerchantsPage;

import com.codeborne.selenide.SelenideElement;
import org.openqa.selenium.By;

import static com.codeborne.selenide.Selectors.*;
import static com.codeborne.selenide.Selenide.$;

public class FormCreateNewMerchant {
     /**
      * Locators for merchant/list/active/ FORM CREATE NEW MERCHANT
      **/
     public final static SelenideElement
             LogInAsMerchantBtn = $(byXpath("//a[contains(@class,'btn-warning')]")),
             InnField = $(byAttribute("data-test", "inn")),
             InnErrorMsg = $(byAttribute("data-test", "inn_alert")),
             KppField = $(byAttribute("data-test", "kpp")),
             KppErrorMsg = $(byAttribute("data-test", "kpp_alert")),
             OrganizationField = $(byAttribute("data-test", "legal_name")),
             OrganizationErrorMsg = $(byAttribute("data-test", "legal_name_alert")),
             LegalAddressField = $(byAttribute("data-test", "legal_address")),
             LegalAddressErrorMsg = $(byXpath("//input[@data-test='fact_address']//ancestor::div[1]//span[@class='invalid-feedback']")),
             FactAddressField = $(byAttribute("data-test", "fact_address")),
             FactAddressErrorMsg = $(byXpath("//input[@data-test='fact_address']//ancestor::div[1]//span[@class='invalid-feedback']")),
             PaymentAccountField = $(byAttribute("data-test", "payment_account")),
             PaymentAccountErrorMsg = $(byAttribute("data-test", "payment_account_alert")),
             BankField = $(byAttribute("data-test", "bank")),
             BankErrorMsg = $(byXpath("//input[@data-test='bank']//ancestor::div[1]//span[@class='invalid-feedback']")),
             BankBikField = $(byAttribute("data-test", "bank_bik")),
             BankBikErrorMsg = $(byAttribute("data-test", "bank_bik_alert")),
             CorrespondentAccountField = $(byAttribute("data-test", "correspondent_account")),
             CorrespondentAccountErrorMsg = $(byAttribute("data-test", "correspondent_account_alert")),
             BankAddressField = $(byAttribute("data-test", "bank_address")),
             BankAddressErrorMsg = $(byAttribute("data-test", "bank_address_alert")),
             SurnameField = $(byAttribute("data-test", "last_name")),
             SurnameErrorMsg = $(byAttribute("data-test", "last_name_alert")),
             NameField = $(byAttribute("data-test", "first_name")),
             NameErrorMsg = $(byAttribute("data-test", "first_name_alert")),
             MiddleNameField = $(byAttribute("data-test", "middle_name")),
             MiddleNameErrorMsg = $(byAttribute("data-test", "middle_name_alert")),
             PhoneField = $(byXpath("//*[@data-test='phone']//input")),
             PhoneErrorMsg = $(byAttribute("data-test", "phone_alert")),
             EmailField = $(byAttribute("data-test", "email")),
             EmailErrorMsg = $(byAttribute("data-test", "email_alert")),
             CommunicationMethodSelect = $(byXpath("//div[@data-test='communication_method']/select")),
             CommunicationMethodErrorMsg = $(byXpath("//select[@data-test='communication_method']//ancestor::div[1]//span[@class='invalid-feedback']")),
             PasswordField = $(byAttribute("data-test", "password")),
             PasswordErrorMsg = $(byAttribute("data-test", "password_alert")),
             PasswordConfirmField = $(byAttribute("data-test", "password_confirmation")),
             PasswordConfirmErrorMsg = $(byAttribute("data-test", "password_confirmation_alert")),
             StorageAddressField = $(byAttribute("data-test", "storage_address")),
             StorageAddressErrorMsg = $(byAttribute("data-test", "storage_address_alert")),
             SiteField = $(byAttribute("data-test", "site")),
             SiteErrorMsg = $(byAttribute("data-test", "site_alert")),
             SaleField = $(byAttribute("data-test", "sale_info")),
             SaleErrorMsg = $(byAttribute("data-test", "sale_info_alert")),
             SaveBtn = $(byAttribute("data-test", "submit")),
             CancelBtn = $(byAttribute("data-test", "cancel")),
             CloseCrossBtn = $(byXpath("//*[@aria-label='Close']")),
             CreateNewMerchForm = $(byId("modalIdCreateMerchant___BV_modal_content_")),
             OrganizationFormSelect = $(byXpath("//div[@data-test='organization_form']/select")),
             loader = $(By.xpath("loader"));

}
