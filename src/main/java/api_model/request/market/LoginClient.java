package api_model.request.market;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class LoginClient {
	private String code;
	private String phone;
}
