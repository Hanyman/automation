package api_model.request.market.cart;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

@Data
@Builder
public class ReqCartItem{

	@JsonProperty("count")
	private int count;

	@JsonProperty("offerId")
	private int offerId;

	@JsonProperty("referrerCode")
	private String referrerCode;

	@JsonProperty("storeId")
	private int storeId;
}