package api_model.request.market.address;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

@Data
@Builder
public class Address{

	@JsonProperty("area")
	private Object area;

	@JsonProperty("area_guid")
	private Object area_guid;

	@JsonProperty("city")
	private String city;

	@JsonProperty("porch")
	private String porch;

	@JsonProperty("post_index")
	private String post_index;

	@JsonProperty("house")
	private String house;

	@JsonProperty("country_code")
	private String country_code;

	@JsonProperty("geo_lat")
	private String geo_lat;

	@JsonProperty("street")
	private String street;

	@JsonProperty("flat")
	private String flat;

	@JsonProperty("city_guid")
	private String city_guid;

	@JsonProperty("geo_lon")
	private String geo_lon;

	@JsonProperty("block")
	private String block;

	@JsonProperty("intercom")
	private String intercom;

	@JsonProperty("comment")
	private String comment;

	@JsonProperty("id")
	private Object id;

	@JsonProperty("region")
	private String region;

	@JsonProperty("floor")
	private String floor;

	@JsonProperty("region_guid")
	private String region_guid;
}