package api_model.request.market.address;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

@Data
@Builder
public class ReqAddAddress{

	@JsonProperty("address")
	private Address address;
}