package api_model.request.market.receivers;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Builder;

@Builder
public class ReqReceivers{

	@JsonProperty("post")
	private String post;

	@JsonProperty("phone")
	private String phone;

	@JsonProperty("last_name")
	private String last_name;

	@JsonProperty("first_name")
	private String first_name;

	@JsonProperty("email")
	private String email;

	public String getPost(){
		return post;
	}

	public String getPhone(){
		return phone;
	}

	public String getLastName(){
		return last_name;
	}

	public String getFirstName(){
		return first_name;
	}

	public String getEmail(){
		return email;
	}
}