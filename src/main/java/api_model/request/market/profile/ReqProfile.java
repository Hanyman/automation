package api_model.request.market.profile;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class ReqProfile{

	@JsonProperty("birthday")
	private String birthday;

	@JsonProperty("gender")
	private int gender;

	@JsonProperty("last_name")
	private String lastName;

	@JsonProperty("middle_name")
	private String middleName;

	@JsonProperty("first_name")
	private String firstName;
}