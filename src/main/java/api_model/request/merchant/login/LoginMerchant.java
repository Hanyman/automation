package api_model.request.merchant.login;

import lombok.*;

@Data
@Builder
public class LoginMerchant {
	private String password;
	private String email;
}
