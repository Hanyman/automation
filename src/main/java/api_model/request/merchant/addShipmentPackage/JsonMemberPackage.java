package api_model.request.merchant.addShipmentPackage;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

@Data
@Builder
public class JsonMemberPackage{

	@JsonProperty("width")
	private Object width;

	@JsonProperty("length")
	private Object length;

	@JsonProperty("weight")
	private int weight;

	@JsonProperty("package_id")
	private int packageId;

	@JsonProperty("height")
	private Object height;
}