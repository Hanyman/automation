package api_model.request.merchant.addShipmentPackage;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

@Data
@Builder
public class ReqAddShipmentPackage{

	@JsonProperty("package")
	private JsonMemberPackage jsonMemberPackage;
}