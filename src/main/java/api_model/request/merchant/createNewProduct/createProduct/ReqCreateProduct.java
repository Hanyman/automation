package api_model.request.merchant.createNewProduct.createProduct;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

@Data
@Builder
public class ReqCreateProduct {

	@JsonProperty("vendor_code")
	private String vendor_code;

	@JsonProperty("category_id")
	private int category_id;

	@JsonProperty("is_new")
	private int is_new;

	@JsonProperty("name")
	private String name;

	@JsonProperty("width")
	private String width;

	@JsonProperty("length")
	private String length;

	@JsonProperty("weight")
	private String weight;

	@JsonProperty("xml_id")
	private String xml_id;

	@JsonProperty("barcode")
	private String barcode;

	@JsonProperty("brand_id")
	private int brand_id;

	@JsonProperty("height")
	private String height;
}