package api_model.request.merchant.createNewProduct.saveStocks;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

@Data
@Builder
public class ReqSaveStocks{

	@JsonProperty("offerId")
	private int offerId;

	@JsonProperty("stocks")
	private Stocks stocks;
}