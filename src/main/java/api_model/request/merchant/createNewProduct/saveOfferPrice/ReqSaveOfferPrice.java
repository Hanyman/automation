package api_model.request.merchant.createNewProduct.saveOfferPrice;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

@Data
@Builder
public class ReqSaveOfferPrice{

	@JsonProperty("price")
	private String price;

	@JsonProperty("offerId")
	private int offerId;

	@JsonProperty("status")
	private int status;
}