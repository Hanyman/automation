package api_model.request.merchant.createNewProduct.saveImage;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

@Data
@Builder
public class ReqSaveImage {

	@JsonProperty("id")
	private int id;

	@JsonProperty("type")
	private int type;
}