package api_model.request.merchant.createNewProduct.saveProduct;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

@Data
@Builder
public class ReqSaveProduct {

	@JsonProperty("explosive")
	private int explosive;

	@JsonProperty("packaged_width")
	private String packagedWidth;

	@JsonProperty("has_battery")
	private int hasBattery;

	@JsonProperty("days_to_return")
	private String daysToReturn;

	@JsonProperty("need_special_store")
	private String needSpecialStore;

	@JsonProperty("packaged_weight")
	private String packagedWeight;

	@JsonProperty("gas")
	private int gas;

	@JsonProperty("need_special_case")
	private String needSpecialCase;

	@JsonProperty("fragile")
	private int fragile;

	@JsonProperty("packaged_length")
	private String packagedLength;

	@JsonProperty("packaged_height")
	private String packagedHeight;

	public int getExplosive(){
		return explosive;
	}

	public String getPackagedWidth(){
		return packagedWidth;
	}

	public int getHasBattery(){
		return hasBattery;
	}

	public String getDaysToReturn(){
		return daysToReturn;
	}

	public String getNeedSpecialStore(){
		return needSpecialStore;
	}

	public String getPackagedWeight(){
		return packagedWeight;
	}

	public int getGas(){
		return gas;
	}

	public String getNeedSpecialCase(){
		return needSpecialCase;
	}

	public int getFragile(){
		return fragile;
	}

	public String getPackagedLength(){
		return packagedLength;
	}

	public String getPackagedHeight(){
		return packagedHeight;
	}
}