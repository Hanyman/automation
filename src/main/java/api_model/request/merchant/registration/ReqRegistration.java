package api_model.request.merchant.registration;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

@Data
@Builder
public class ReqRegistration{

	@JsonProperty("password_confirmation")
	private String password_confirmation;

	@JsonProperty("assortment")
	private Assortment assortment;

	@JsonProperty("communication_method")
	private String communication_method;

	@JsonProperty("storage_address")
	private String storage_address;

	@JsonProperty("inn")
	private String inn;

	@JsonProperty("bank_address")
	private String bank_address;

	@JsonProperty("organization_form")
	private String organization_form;

	@JsonProperty("last_name")
	private String last_name;

	@JsonProperty("kpp")
	private String kpp;

	@JsonProperty("middle_name")
	private String middle_name;

	@JsonProperty("can_integration")
	private boolean can_integration;

	@JsonProperty("sale_info")
	private String sale_info;

	@JsonProperty("bank")
	private String bank;

	@JsonProperty("password")
	private String password;

	@JsonProperty("site")
	private String site;

	@JsonProperty("payment_account")
	private String payment_account;

	@JsonProperty("legal_address")
	private String legal_address;

	@JsonProperty("bank_bik")
	private String bank_bik;

	@JsonProperty("phone")
	private String phone;

	@JsonProperty("correspondent_account")
	private String correspondent_account;

	@JsonProperty("legal_name")
	private String legal_name;

	@JsonProperty("first_name")
	private String first_name;

	@JsonProperty("fact_address")
	private String fact_address;

	@JsonProperty("email")
	private String email;
}