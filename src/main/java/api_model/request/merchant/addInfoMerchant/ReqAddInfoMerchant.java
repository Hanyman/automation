package api_model.request.merchant.addInfoMerchant;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

@Data
@Builder
public class ReqAddInfoMerchant{

	@JsonProperty("code")
	private String code;

	@JsonProperty("city")
	private Object city;

	@JsonProperty("ceo_last_name")
	private String ceoLastName;

	@JsonProperty("bank_address")
	private String bankAddress;

	@JsonProperty("created_at")
	private String createdAt;

	@JsonProperty("sale_info")
	private Object saleInfo;

	@JsonProperty("bank")
	private String bank;

	@JsonProperty("price_moderate")
	private boolean priceModerate;

	@JsonProperty("legal_address")
	private String legalAddress;

	@JsonProperty("updated_at")
	private String updatedAt;

	@JsonProperty("manager_id")
	private Object managerId;

	@JsonProperty("contract_number")
	private Object contractNumber;

	@JsonProperty("correspondent_account")
	private String correspondentAccount;

	@JsonProperty("legal_name")
	private String legalName;

	@JsonProperty("id")
	private int id;

	@JsonProperty("fact_address")
	private String factAddress;

	@JsonProperty("vat_info")
	private Object vatInfo;

	@JsonProperty("commercial_info")
	private Object commercialInfo;

	@JsonProperty("storage_address")
	private String storageAddress;

	@JsonProperty("inn")
	private String inn;

	@JsonProperty("organization_form")
	private int organizationForm;

	@JsonProperty("kpp")
	private String kpp;

	@JsonProperty("ceo_middle_name")
	private String ceoMiddleName;

	@JsonProperty("can_integration")
	private Object canIntegration;

	@JsonProperty("contract_at")
	private Object contractAt;

	@JsonProperty("ceo_first_name")
	private String ceoFirstName;

	@JsonProperty("site")
	private Object site;

	@JsonProperty("cancel_reason")
	private Object cancelReason;

	@JsonProperty("payment_account")
	private String paymentAccount;

	@JsonProperty("bank_bik")
	private String bankBik;

	@JsonProperty("rating_id")
	private Object ratingId;

	@JsonProperty("comment")
	private Object comment;

	@JsonProperty("status_at")
	private String statusAt;

	@JsonProperty("status")
	private int status;
}