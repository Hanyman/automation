package api_model.request.merchant.stores;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class Address{
	private Object area;
	private Object area_guid;
	private String address_string;
	private String city;
	private String porch;
	private String post_index;
	private String house;
	private String country_code;
	private String street;
	private String flat;
	private String city_guid;
	private String block;
	private String intercom;
	private String comment;
	private String region;
	private String floor;
	private String region_guid;
}
