package api_model.request.merchant.stores;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class CreateStoreMas{
	private Address address;
	private int return_accepting_type_id;
	private String name;
	private String xml_id;
}
