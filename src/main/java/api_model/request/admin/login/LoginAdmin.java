package api_model.request.admin.login;

import lombok.*;

@Data
@Builder
public class LoginAdmin{
	private String password;
	private String email;
}
