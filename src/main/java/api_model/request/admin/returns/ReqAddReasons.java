package api_model.request.admin.returns;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

@Data
@Builder
public class ReqAddReasons {

	@JsonProperty("is_active")
	private boolean isActive;

	@JsonProperty("is_required_comment")
	private boolean isRequiredComment;

	@JsonProperty("return_reason_category_id")
	private int returnReasonCategoryId;

	@JsonProperty("hint")
	private String hint;

	@JsonProperty("name")
	private String name;

	@JsonProperty("sort")
	private Object sort;

	@JsonProperty("is_default")
	private boolean isDefault;

	@JsonProperty("client_name")
	private String clientName;

	@JsonProperty("is_required_photo")
	private boolean isRequiredPhoto;
}