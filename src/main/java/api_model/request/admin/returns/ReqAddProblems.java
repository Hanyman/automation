package api_model.request.admin.returns;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

@Data
@Builder
public class ReqAddProblems {

	@JsonProperty("is_active")
	private boolean isActive;

	@JsonProperty("name")
	private String name;

	@JsonProperty("client_name")
	private String clientName;
}