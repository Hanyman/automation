package api_model.request.admin.approveProducts;

import java.util.List;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

@Data
@Builder
public class ReqApproveProducts{

	@JsonProperty("productIds")
	private List<String> productIds;

	@JsonProperty("comment")
	private String comment;

	@JsonProperty("status")
	private String status;
}