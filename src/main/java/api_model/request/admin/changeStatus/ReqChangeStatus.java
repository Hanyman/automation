package api_model.request.admin.changeStatus;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

@Data
@Builder
public class ReqChangeStatus{

	@JsonProperty("status")
	private int status;
}