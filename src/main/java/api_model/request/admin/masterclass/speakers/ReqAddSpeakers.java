package api_model.request.admin.masterclass.speakers;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

@Data
@Builder
public class ReqAddSpeakers{

	@JsonProperty("speaker")
	private Speaker speaker;

	@JsonProperty("id")
	private Object id;
}