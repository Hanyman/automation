package api_model.request.admin.masterclass.speakers;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

@Data
@Builder
public class Speaker{

	@JsonProperty("phone")
	private String phone;

	@JsonProperty("profession_id")
	private int profession_id;

	@JsonProperty("facebook")
	private Object facebook;

	@JsonProperty("file_id")
	private int file_id;

	@JsonProperty("last_name")
	private String last_name;

	@JsonProperty("description")
	private String description;

	@JsonProperty("global")
	private boolean global;

	@JsonProperty("instagram")
	private Object instagram;

	@JsonProperty("linkedin")
	private Object linkedin;

	@JsonProperty("middle_name")
	private String middle_name;

	@JsonProperty("first_name")
	private String first_name;

	@JsonProperty("email")
	private String email;
}