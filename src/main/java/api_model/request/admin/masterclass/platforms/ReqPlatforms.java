package api_model.request.admin.masterclass.platforms;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

@Data
@Builder
public class ReqPlatforms{

	@JsonProperty("id")
	private Object id;

	@JsonProperty("place")
	private Place place;
}