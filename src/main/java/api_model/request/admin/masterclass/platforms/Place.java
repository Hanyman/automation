package api_model.request.admin.masterclass.platforms;

import java.util.List;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

@Data
@Builder
public class Place{

	@JsonProperty("city_name")
	private String city_name;

	@JsonProperty("address")
	private String address;

	@JsonProperty("latitude")
	private String latitude;

	@JsonProperty("name")
	private String name;

	@JsonProperty("description")
	private String description;

	@JsonProperty("global")
	private boolean global;

	@JsonProperty("media")
	private List<Object> media;

	@JsonProperty("longitude")
	private String longitude;

	@JsonProperty("city_id")
	private String city_id;
}