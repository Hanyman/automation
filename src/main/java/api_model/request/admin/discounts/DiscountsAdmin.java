package api_model.request.admin.discounts;

import lombok.*;

import java.util.List;
@Data
@Builder
public class DiscountsAdmin{
	private Object end_date;
	private int value_type;
	private List<String> brands;
	private String name;
	private Except except;
	private Object merchant_id;
	private int type;
	private List<Object> conditions;
	private int value;
	private boolean promo_code_only;
	private Object start_date;
	private int status;
}