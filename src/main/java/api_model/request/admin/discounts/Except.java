package api_model.request.admin.discounts;

import lombok.*;

import java.util.List;
@Data
@Builder
public class Except{
	private List<Object> offers;
}