package api_model.response.market.profile;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class Passport{

	@JsonProperty("no")
	private Object no;

	@JsonProperty("patronymic")
	private Object patronymic;

	@JsonProperty("issue_date")
	private Object issueDate;

	@JsonProperty("serial")
	private Object serial;

	@JsonProperty("surname")
	private Object surname;

	@JsonProperty("name")
	private Object name;

	@JsonProperty("id")
	private Object id;

	@JsonProperty("customer_id")
	private int customerId;
}