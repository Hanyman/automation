package api_model.response.market.profile;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class ActivitiesAllItem{

	@JsonProperty("name")
	private String name;

	@JsonProperty("id")
	private int id;
}