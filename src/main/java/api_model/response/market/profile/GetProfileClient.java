package api_model.response.market.profile;

import java.util.List;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class GetProfileClient{

	@JsonProperty("birthday")
	private Object birthday;

	@JsonProperty("gender")
	private int gender;

	@JsonProperty("can_edit_referral_code")
	private boolean canEditReferralCode;

	@JsonProperty("referral_personal_discount")
	private Object referralPersonalDiscount;

	@JsonProperty("legal_info_bank")
	private Object legalInfoBank;

	@JsonProperty("passport")
	private Passport passport;

	@JsonProperty("portfolio")
	private List<Object> portfolio;

	@JsonProperty("first_name")
	private String firstName;

	@JsonProperty("legal_info_bik")
	private Object legalInfoBik;

	@JsonProperty("payment_city")
	private Object paymentCity;

	@JsonProperty("email")
	private Object email;

	@JsonProperty("has_password")
	private boolean hasPassword;

	@JsonProperty("legal_info_company_name")
	private Object legalInfoCompanyName;

	@JsonProperty("social")
	private List<Object> social;

	@JsonProperty("last_name")
	private String lastName;

	@JsonProperty("avatar")
	private Object avatar;

	@JsonProperty("middle_name")
	private Object middleName;

	@JsonProperty("legal_info_bank_correspondent_account")
	private Object legalInfoBankCorrespondentAccount;

	@JsonProperty("certificates")
	private List<Object> certificates;

	@JsonProperty("legal_info_company_address")
	private Object legalInfoCompanyAddress;

	@JsonProperty("phone")
	private String phone;

	@JsonProperty("activities")
	private List<Object> activities;

	@JsonProperty("activitiesAll")
	private List<ActivitiesAllItem> activitiesAll;

	@JsonProperty("legal_info_payment_account")
	private Object legalInfoPaymentAccount;

	@JsonProperty("legal_info_inn")
	private Object legalInfoInn;
}