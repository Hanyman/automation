package api_model.response.market.cart;

import com.fasterxml.jackson.annotation.JsonProperty;

public class ItemsItem{

	@JsonProperty("p")
	private P P;

	@JsonProperty("count")
	private int count;

	@JsonProperty("id")
	private int id;

	@JsonProperty("type")
	private String type;
}