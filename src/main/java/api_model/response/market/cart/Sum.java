package api_model.response.market.cart;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Sum{

	@JsonProperty("currency")
	private String currency;

	@JsonProperty("value")
	private int value;
}