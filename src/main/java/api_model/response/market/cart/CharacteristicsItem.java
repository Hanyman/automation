package api_model.response.market.cart;

import java.util.List;
import com.fasterxml.jackson.annotation.JsonProperty;

public class CharacteristicsItem{

	@JsonProperty("name")
	private String name;

	@JsonProperty("value")
	private List<String> value;
}