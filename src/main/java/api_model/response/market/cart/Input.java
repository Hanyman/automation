package api_model.response.market.cart;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Input{

	@JsonProperty("certificates")
	private Object certificates;

	@JsonProperty("bonus")
	private int bonus;

	@JsonProperty("promoCode")
	private Object promoCode;
}