package api_model.response.market.cart;

import java.util.List;
import com.fasterxml.jackson.annotation.JsonProperty;

public class Summary{

	@JsonProperty("total")
	private Total total;

	@JsonProperty("maxBonus")
	private int maxBonus;

	@JsonProperty("discounts")
	private List<Object> discounts;

	@JsonProperty("bonusGet")
	private int bonusGet;

	@JsonProperty("promoCodes")
	private List<Object> promoCodes;

	@JsonProperty("sum")
	private Sum sum;
}