package api_model.response.market.cart;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Stock{

	@JsonProperty("qty")
	private int qty;

	@JsonProperty("storeId")
	private int storeId;
}