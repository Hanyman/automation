package api_model.response.market.cart;

import java.util.List;
import com.fasterxml.jackson.annotation.JsonProperty;

public class P{

	@JsonProperty("image")
	private Image image;

	@JsonProperty("characteristics")
	private List<CharacteristicsItem> characteristics;

	@JsonProperty("code")
	private String code;

	@JsonProperty("productId")
	private int productId;

	@JsonProperty("bonus")
	private int bonus;

	@JsonProperty("rating")
	private int rating;

	@JsonProperty("bundleId")
	private int bundleId;

	@JsonProperty("length")
	private int length;

	@JsonProperty("variantGroups")
	private Object variantGroups;

	@JsonProperty("weight")
	private int weight;

	@JsonProperty("active")
	private boolean active;

	@JsonProperty("type")
	private String type;

	@JsonProperty("categoryCodes")
	private List<String> categoryCodes;

	@JsonProperty("price")
	private Price price;

	@JsonProperty("name")
	private String name;

	@JsonProperty("width")
	private int width;

	@JsonProperty("id")
	private int id;

	@JsonProperty("stock")
	private Stock stock;

	@JsonProperty("height")
	private int height;
}