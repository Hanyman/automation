package api_model.response.market.cart;

import com.fasterxml.jackson.annotation.JsonProperty;

public class RespCartItem{

	@JsonProperty("product")
	private Product product;
}