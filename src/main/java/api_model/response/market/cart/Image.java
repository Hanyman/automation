package api_model.response.market.cart;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Image{

	@JsonProperty("sourceExt")
	private String sourceExt;

	@JsonProperty("id")
	private int id;
}