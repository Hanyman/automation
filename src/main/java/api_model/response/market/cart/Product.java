package api_model.response.market.cart;

import java.util.List;
import com.fasterxml.jackson.annotation.JsonProperty;

public class Product{

	@JsonProperty("summary")
	private Summary summary;

	@JsonProperty("input")
	private Input input;

	@JsonProperty("id")
	private int id;

	@JsonProperty("type")
	private String type;

	@JsonProperty("items")
	private List<ItemsItem> items;
}