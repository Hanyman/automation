package api_model.response.market.checkoutCommit;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class ItemsItem{

	@JsonProperty("qnt")
	private int qnt;

	@JsonProperty("price")
	private int price;

	@JsonProperty("id")
	private int id;
}