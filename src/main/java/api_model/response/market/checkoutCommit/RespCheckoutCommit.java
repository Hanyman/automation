package api_model.response.market.checkoutCommit;

import java.util.List;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class RespCheckoutCommit{

	@JsonProperty("orderNumber")
	private String orderNumber;

	@JsonProperty("orderId")
	private int orderId;

	@JsonProperty("paymentUrl")
	private String paymentUrl;

	@JsonProperty("items")
	private List<ItemsItem> items;
}