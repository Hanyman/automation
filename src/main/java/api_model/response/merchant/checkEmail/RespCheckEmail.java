package api_model.response.merchant.checkEmail;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class RespCheckEmail{

	@JsonProperty("exists")
	private boolean exists;
}