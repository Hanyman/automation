package api_model.response.merchant.createNewProduct.saveStocks;

import com.fasterxml.jackson.annotation.JsonProperty;

public class StocksItem{

	@JsonProperty("store_id")
	private int storeId;

	@JsonProperty("updated_at")
	private String updatedAt;

	@JsonProperty("delivery_period")
	private int deliveryPeriod;

	@JsonProperty("product_id")
	private int productId;

	@JsonProperty("qty")
	private int qty;

	@JsonProperty("created_at")
	private String createdAt;

	@JsonProperty("id")
	private int id;

	@JsonProperty("offer_id")
	private int offerId;
}