package api_model.response.merchant.createNewProduct.saveStocks;

import java.util.List;
import com.fasterxml.jackson.annotation.JsonProperty;

public class RespSaveStocks{

	@JsonProperty("totalQty")
	private int totalQty;

	@JsonProperty("stocks")
	private List<StocksItem> stocks;
}