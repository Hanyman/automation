package api_model.response.merchant.createNewProduct.saveOfferPrice;

import java.util.List;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

@Data
public class RespSaveOfferPrice{

	@JsonProperty("offer")
	private Offer offer;

	@JsonProperty("discounts")
	private List<Object> discounts;

	@JsonProperty("price")
	private Price price;
}