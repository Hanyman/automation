package api_model.response.merchant.createNewProduct.saveOfferPrice;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

@Data
public class Price{

	@JsonProperty("cost")
	private String cost;

	@JsonProperty("discounts")
	private Object discounts;

	@JsonProperty("price")
	private String price;

	@JsonProperty("bonus")
	private int bonus;

	@JsonProperty("offer_id")
	private int offerId;
}