package api_model.response.merchant.createNewProduct.saveImage;

import com.fasterxml.jackson.annotation.JsonProperty;

public class File{

	@JsonProperty("size")
	private int size;

	@JsonProperty("original_name")
	private String originalName;

	@JsonProperty("id")
	private int id;

	@JsonProperty("absolute_url")
	private String absoluteUrl;

	@JsonProperty("url")
	private String url;
}