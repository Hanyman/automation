package api_model.response.merchant.createNewProduct.saveImage;

import com.fasterxml.jackson.annotation.JsonProperty;

public class ImagesItem{

	@JsonProperty("file")
	private File file;

	@JsonProperty("productId")
	private int productId;

	@JsonProperty("id")
	private int id;

	@JsonProperty("type")
	private int type;

	@JsonProperty("url")
	private String url;
}