package api_model.response.merchant.createNewProduct.saveImage;

import java.util.List;
import com.fasterxml.jackson.annotation.JsonProperty;

public class RespSaveImage{

	@JsonProperty("images")
	private List<ImagesItem> images;
}