package api_model.response.merchant.createNewProduct.saveProduct;

import java.util.List;
import com.fasterxml.jackson.annotation.JsonProperty;

public class RespSaveProduct{

	@JsonProperty("availableProperties")
	private List<Object> availableProperties;

	@JsonProperty("offer")
	private Offer offer;

	@JsonProperty("product")
	private Product product;

	@JsonProperty("discounts")
	private List<Object> discounts;

	@JsonProperty("price")
	private Price price;

	@JsonProperty("propertyValues")
	private List<Object> propertyValues;

	@JsonProperty("directoryValues")
	private List<Object> directoryValues;
}