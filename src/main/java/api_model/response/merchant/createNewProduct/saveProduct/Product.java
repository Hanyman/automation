package api_model.response.merchant.createNewProduct.saveProduct;

import java.util.List;
import com.fasterxml.jackson.annotation.JsonProperty;

public class Product{

	@JsonProperty("explosive")
	private boolean explosive;

	@JsonProperty("how_to")
	private Object howTo;

	@JsonProperty("vendor_code")
	private String vendorCode;

	@JsonProperty("code")
	private String code;

	@JsonProperty("how_to_video")
	private Object howToVideo;

	@JsonProperty("public_event_ids")
	private Object publicEventIds;

	@JsonProperty("production")
	private int production;

	@JsonProperty("variant_group_id")
	private Object variantGroupId;

	@JsonProperty("rating")
	private Object rating;

	@JsonProperty("description")
	private Object description;

	@JsonProperty("created_at")
	private String createdAt;

	@JsonProperty("need_special_case")
	private String needSpecialCase;

	@JsonProperty("reviews_qty")
	private int reviewsQty;

	@JsonProperty("description_video")
	private Object descriptionVideo;

	@JsonProperty("tips")
	private List<Object> tips;

	@JsonProperty("packaged_width")
	private int packagedWidth;

	@JsonProperty("is_origin")
	private boolean isOrigin;

	@JsonProperty("archive_date")
	private Object archiveDate;

	@JsonProperty("days_to_return")
	private int daysToReturn;

	@JsonProperty("category_id")
	private int categoryId;

	@JsonProperty("updated_at")
	private String updatedAt;

	@JsonProperty("instruction_file_id")
	private Object instructionFileId;

	@JsonProperty("need_special_store")
	private String needSpecialStore;

	@JsonProperty("gas")
	private boolean gas;

	@JsonProperty("owner_merchant_id")
	private int ownerMerchantId;

	@JsonProperty("id")
	private int id;

	@JsonProperty("barcode")
	private String barcode;

	@JsonProperty("height")
	private int height;

	@JsonProperty("origin_product_id")
	private Object originProductId;

	@JsonProperty("has_battery")
	private boolean hasBattery;

	@JsonProperty("approval_status")
	private int approvalStatus;

	@JsonProperty("version_usage_type_id")
	private Object versionUsageTypeId;

	@JsonProperty("packaged_weight")
	private int packagedWeight;

	@JsonProperty("length")
	private int length;

	@JsonProperty("weight")
	private int weight;

	@JsonProperty("archive")
	private int archive;

	@JsonProperty("packaged_height")
	private int packagedHeight;

	@JsonProperty("brand_id")
	private int brandId;

	@JsonProperty("production_comment")
	private Object productionComment;

	@JsonProperty("is_single_offer")
	private boolean isSingleOffer;

	@JsonProperty("name")
	private String name;

	@JsonProperty("archive_comment")
	private Object archiveComment;

	@JsonProperty("width")
	private int width;

	@JsonProperty("fragile")
	private boolean fragile;

	@JsonProperty("packaged_length")
	private int packagedLength;

	@JsonProperty("approval_status_comment")
	private Object approvalStatusComment;

	@JsonProperty("properties")
	private List<Object> properties;
}