package api_model.response.merchant.createNewProduct.createProduct;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class AvailablePropertiesItem{

	@JsonProperty("code")
	private String code;

	@JsonProperty("is_inherited")
	private boolean isInherited;

	@JsonProperty("is_color")
	private boolean isColor;

	@JsonProperty("created_at")
	private String createdAt;

	@JsonProperty("description")
	private Object description;

	@JsonProperty("base_attribute_id")
	private Object baseAttributeId;

	@JsonProperty("display_name")
	private String displayName;

	@JsonProperty("type")
	private String type;

	@JsonProperty("baseAttribute")
	private Object baseAttribute;

	@JsonProperty("is_multiple")
	private boolean isMultiple;

	@JsonProperty("is_base")
	private boolean isBase;

	@JsonProperty("updated_at")
	private String updatedAt;

	@JsonProperty("is_required")
	private boolean isRequired;

	@JsonProperty("name")
	private String name;

	@JsonProperty("measurement_unit")
	private Object measurementUnit;

	@JsonProperty("is_filterable")
	private boolean isFilterable;

	@JsonProperty("id")
	private int id;
}