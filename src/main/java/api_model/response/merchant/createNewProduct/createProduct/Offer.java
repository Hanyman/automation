package api_model.response.merchant.createNewProduct.createProduct;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class Offer{

	@JsonProperty("entity_type")
	private int entityType;

	@JsonProperty("ticket_type_id")
	private Object ticketTypeId;

	@JsonProperty("updated_at")
	private String updatedAt;

	@JsonProperty("product_id")
	private int productId;

	@JsonProperty("sale_at")
	private Object saleAt;

	@JsonProperty("sale_status")
	private int saleStatus;

	@JsonProperty("created_at")
	private String createdAt;

	@JsonProperty("manual_sort")
	private int manualSort;

	@JsonProperty("certificate_nominal_id")
	private Object certificateNominalId;

	@JsonProperty("id")
	private int id;

	@JsonProperty("merchant_id")
	private int merchantId;

	@JsonProperty("xml_id")
	private String xmlId;
}