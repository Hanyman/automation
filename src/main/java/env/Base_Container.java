package env;

import com.codeborne.selenide.Configuration;
import com.codeborne.selenide.Selenide;
import com.codeborne.selenide.WebDriverRunner;
import configs.UrlsConfig;
import org.aeonbits.owner.ConfigFactory;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.LocalFileDetector;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeTest;

import java.net.MalformedURLException;
import java.net.URI;
import java.util.HashMap;
import java.util.Map;

import static com.codeborne.selenide.WebDriverRunner.setWebDriver;

/** Main class for initialization browser, set configurations, kill browser **/
public class Base_Container extends Base_api{
    static public WebDriver driver;
    static final WebDriverConfig config = ConfigFactory.create(WebDriverConfig.class, System.getProperties());
    public static final UrlsConfig urlsConfig = ConfigFactory.create(UrlsConfig.class, System.getProperties());
    public int globalTimeout = 20;
    @BeforeTest
    public void setups() throws MalformedURLException {

        getConfiguration(config.getRunType());
        Selenide.open(urlsConfig.getMarketUrl());
    }

    @AfterClass
    public void tearDownClass(){
       // clearBrowser();
        Selenide.closeWebDriver();
    }

    @BeforeClass
    public void setUpClass() throws MalformedURLException {
        getConfiguration(config.getRunType());
    }

    public WebDriver getWebDriver(){
        return Selenide.webdriver().driver().getWebDriver();
    }
    // function for get configurations local(browser) or remote(selenoid)
    public static void getConfiguration(String runType) throws MalformedURLException {
        switch (runType){
            case("local"):
                Configuration.browser = config.getWebBrowser();
                Configuration.baseUrl = urlsConfig.getMarketUrl();
                Configuration.browserSize = config.getBrowserSize();
                Configuration.timeout = config.getBrowserTimeout();
                Configuration.pageLoadTimeout = config.getBrowserTimeout();
                Configuration.headless = config.getHeadlessMode();

                break;

            case("remote"):
                Configuration.timeout = config.getBrowserTimeout();

                DesiredCapabilities capabilities = new DesiredCapabilities();

                capabilities.setBrowserName(config.getWebBrowser());
                capabilities.setVersion(config.getBrowserVersion());

                Map<String, Object> selenoidOptions = new HashMap<>();
                selenoidOptions.put("enableVNC", true);
                selenoidOptions.put("enableVideo", true);

                capabilities.setCapability("selenoid:options", selenoidOptions);
                Configuration.browserCapabilities = capabilities;

                RemoteWebDriver webDriver = new RemoteWebDriver(URI.create(config.getRemoteUrl()).toURL(),capabilities);
                webDriver.setFileDetector(new LocalFileDetector());
                setWebDriver(webDriver);
                WebDriverRunner.getWebDriver().manage().window().maximize();

                break;

        }

    }
}
