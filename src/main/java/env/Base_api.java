package env;

import constants.ConstantApi;
import constants.data.*;
import helpers.api.ApiHelper;
import io.qameta.allure.Step;
import io.restassured.RestAssured;
import io.restassured.parsing.Parser;
import io.restassured.specification.RequestSpecification;
import org.testng.annotations.BeforeSuite;
import steps.api.admin.LoginAdminApiSteps;
import steps.api.market.LoginMarketApiSteps;
import steps.api.merchant.LoginMerchantApiSteps;

import static constants.end_point.MarketEndpoint.v1_categories;
import static helpers.api.ApiHelper.*;
import static helpers.api.Spec_api.*;

public class Base_api extends ConstantApi {

    @BeforeSuite
    public void setup() {

        url_admin = urlsConfig.getAdminUrl();
        url_merchant = urlsConfig.getMerchantUrl();
        url_market = urlsConfig.getMarketUrl();

        RestAssured.defaultParser = Parser.JSON;

        getTokensAuthMerchant(generalMerchConfig.getMerchantLogin(),generalMerchConfig.getMerchantPassword());
        getTokensAdmin();
       // getTokensMarket();

    }

    protected void getTokensMerchant(){
        cookies_merchant = getTokensLogin(default_spec(urlsConfig.getMerchantUrl()),MAS_SESSION);
        xsrf_token_merchant = getToken(cookies_merchant,XSRF_TOKEN);
        session_merchant = getToken(cookies_merchant,MAS_SESSION);
        merchantSpec = spec_with_token(url_merchant, xsrf_token_merchant,MAS_SESSION, session_merchant);
        merchSpecOnlyCookies = spec_with_token_withoutHeader(url_merchant, xsrf_token_merchant,MAS_SESSION, session_merchant);
        fileMerchantSpec = spec_upload_file(url_merchant, xsrf_token_merchant, MAS_SESSION, session_merchant);

    }

    protected void getTokensAuthMerchant(String login, String password){
        LoginMerchantApiSteps loginMerchantApiSteps = new LoginMerchantApiSteps();
        cookies_merchant = getTokensLogin(default_spec(urlsConfig.getMerchantUrl()),MAS_SESSION);
        xsrf_token_merchant = getToken(cookies_merchant,XSRF_TOKEN);
        session_merchant = getToken(cookies_merchant,MAS_SESSION);
        merchantSpec = spec_with_token(url_merchant, xsrf_token_merchant, MAS_SESSION, session_merchant);
        merchSpecOnlyCookies = spec_with_token_withoutHeader(url_merchant, xsrf_token_merchant,MAS_SESSION, session_merchant);
        fileMerchantSpec = spec_upload_file(url_merchant, xsrf_token_merchant, MAS_SESSION, session_merchant);
        loginMerchantApiSteps.loginMerchantPanel(merchantSpec,login,password);

    }

    protected RequestSpecification getTokensMerchant(String login, String password){
        RequestSpecification merchantSpec;
        LoginMerchantApiSteps loginMerchantApiSteps = new LoginMerchantApiSteps();
        cookies_merchant = getTokensLogin(default_spec(urlsConfig.getMerchantUrl()),MAS_SESSION);
        xsrf_token_merchant = getToken(cookies_merchant,XSRF_TOKEN);
        session_merchant = getToken(cookies_merchant,MAS_SESSION);
        merchantSpec = spec_with_token(url_merchant, xsrf_token_merchant, MAS_SESSION, session_merchant);
        merchSpecOnlyCookies = spec_with_token_withoutHeader(url_merchant, xsrf_token_merchant,MAS_SESSION, session_merchant);
        fileMerchantSpec = spec_upload_file(url_merchant, xsrf_token_merchant, MAS_SESSION, session_merchant);
        loginMerchantApiSteps.loginMerchantPanel(merchantSpec,login,password);
        return merchantSpec;
    }

    protected RequestSpecification getTokensMarket(String phone){

        RequestSpecification marketDefault, marketSpec;
        LoginMarketApiSteps marketApiSteps = new LoginMarketApiSteps();
        cookies_market = getTokenMarket(default_spec(urlsConfig.getMarketUrl()+v1_categories.path()),APIMS_SESSION);
        apims_session_market = getToken(cookies_market,APIMS_SESSION);
        marketDefault = spec_client_with_token(url_market, apims_session_market, APIMS_SESSION);
        marketApiSteps.loginClient(marketDefault,phone,Phone.SMS_CODE);
        marketSpec = spec_client_with_token_market(url_market, apims_session_market, APIMS_SESSION);
        fileMarketSpec = spec_upload_file_market(url_market, apims_session_market, APIMS_SESSION);

        return marketSpec;
    }

    protected void getTokensAdmin(){
        LoginAdminApiSteps adminApiSteps = new LoginAdminApiSteps();
        cookies_admin = getTokensLogin(default_spec(url_admin),LARAVEL_SESSION);
        xsrf_token_admin = getToken(cookies_admin,XSRF_TOKEN);
        session_admin = getToken(cookies_admin,LARAVEL_SESSION);
        adminSpec = spec_with_token(url_admin,xsrf_token_admin,LARAVEL_SESSION,session_admin);
        fileAdminSpec = spec_upload_file(url_admin, xsrf_token_admin, LARAVEL_SESSION, session_admin);
        adminApiSteps.loginAdminPanel(adminConfig.getAdminLogin(),adminConfig.getAdminPassword());

    }
    @Step("Делаем логаут мерчанта.")
    protected void logoutMerchantPanel(RequestSpecification spec){
        LoginMerchantApiSteps loginMerchantApiSteps = new LoginMerchantApiSteps();
        loginMerchantApiSteps.logoutMerchant(spec);
    }

}