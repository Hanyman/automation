package API;

import api_model.request.merchant.createNewProduct.createProduct.ReqCreateProduct;
import api_model.request.merchant.createNewProduct.saveOfferPrice.ReqSaveOfferPrice;
import api_model.request.merchant.createNewProduct.saveStocks.ReqSaveStocks;
import api_model.request.merchant.createNewProduct.saveStocks.Stocks;
import env.Base_api;
import helpers.Generator;
import io.qameta.allure.Description;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import junit.framework.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import steps.api.merchant.CommonMerchantApiSteps;
import steps.api.merchant.CreateNewProductApiSteps;

import java.util.HashMap;
import java.util.Map;

import static constants.end_point.MerchantPanelEndpoint.catalog_products;
import static constants.end_point.MerchantPanelEndpoint.create_product;
import static helpers.api.ApiHelper.clearQuery;
import static io.qameta.allure.Allure.step;
import static io.restassured.RestAssured.given;

public class TestApi_CreateNewProduct extends Base_api {
    CreateNewProductApiSteps createNewProductApiSteps = new CreateNewProductApiSteps();
    CommonMerchantApiSteps commonApiSteps = new CommonMerchantApiSteps();
    Generator generator;
    Map<String, String> idsProduct = new HashMap<String, String>();
    int idProductCommon, idOfferCommon;
    String authToken;

    @BeforeClass
    private void createCommonProduct(){
        getTokensAuthMerchant(generalMerchConfig.getMerchantLogin(),generalMerchConfig.getMerchantPassword());
        generator = new Generator();
        String combinationForBarcode = generator.getCombinationForBarcode();
        String productName = generator.getNameProduct();
        String article = generator.getArticle();
        authToken = createNewProductApiSteps.getTokenForCreateProduct();
        idsProduct = createNewProductApiSteps.createProduct(combinationForBarcode, productName,article);
        idProductCommon = Integer.parseInt(idsProduct.get("idProduct"));
        idOfferCommon = Integer.parseInt(idsProduct.get("idOffer"));
    }

    @BeforeMethod
    private void generator(){
        generator = new Generator();

    }

    @Test
    @Description("Создание и аппрув нового товара.")
    public void testCreateNewProduct(){
        int idProduct, idOffer;
        Map<String, String> ids;
        String combinationForBarcode = generator.getCombinationForBarcode();
        String productName = generator.getNameProduct();
        String article = generator.getArticle();


        ids = createNewProductApiSteps.createProduct(combinationForBarcode, productName,article);
        idProduct = Integer.parseInt(ids.get("idProduct"));
        idOffer = Integer.parseInt(ids.get("idOffer"));

        createNewProductApiSteps.saveOfferPrice(idProduct, idOffer);
        createNewProductApiSteps.saveProduct(idProduct);
        createNewProductApiSteps.saveStocks(idProduct, idOffer);
        createNewProductApiSteps.saveImage(idProduct);
        createNewProductApiSteps.saveClaim(idProduct);
        createNewProductApiSteps.getIdProductCheck(idProduct);
        createNewProductApiSteps. approveProduct(idProduct);

    }

    //@Test
    public void testCreateNewProductWhitIncorrectBarcode(){
        String productName = generator.getNameProduct();
        String article = generator.getArticle();

        step("Создание товара с некорректным штрих кодом.", () -> {
            String authorizationToken = createNewProductApiSteps.getTokenForCreateProduct();
            ReqCreateProduct body = ReqCreateProduct.builder()
                    .name(productName).barcode("barCode").brand_id(1).category_id(5).height("200").is_new(0)
                    .length("50").vendor_code(article).weight("2000").width("100").xml_id(article)
                    .build();

            Response response = given()
                    .spec(merchantSpec)
                    .header("authorization", authorizationToken)
                    .header("accept", ContentType.JSON)
                    .body(body)
                    .post(create_product.path());

            steps.checkResponseWithStatusCodeAndLogView(response, 422);
            Assert.assertEquals("Msg must be display.","Невалидный штрихкод",
                    clearQuery(response.jsonPath().getString("errors.barcode")));
        });
    }

    @Test
    public void testCreateNewProductWhitExistBarcode(){
        String productName = generator.getNameProduct();
        String article = generator.getArticle();

        step("Создание товара с существующим штрих кодом.", () -> {
            String authorizationToken = createNewProductApiSteps.getTokenForCreateProduct();
            ReqCreateProduct body = ReqCreateProduct.builder()
                    .name(productName).barcode(BAR_CODE_EXIST).brand_id(1).category_id(3).height("200").is_new(0)
                    .length("50").vendor_code(article).weight("2000").width("100").xml_id(article)
                    .build();

            Response response = given()
                    .spec(merchantSpec)
                    .header("authorization", authorizationToken)
                    .header("accept", ContentType.JSON)
                    .body(body)
                    .post(create_product.path());

            steps.checkResponseWithStatusCodeAndLogView(response, 422);
            Assert.assertEquals("Msg must be display.","Товар с таким ШК уже существует, перейдите в список товаров для его поиска",
                    clearQuery(response.jsonPath().getString("errors.barcode")));
        });
    }

    @Test
    public void testAddingNegativePriceToProduct(){

        step("Добавление отрицательной цены товара", () -> {
            ReqSaveOfferPrice body = ReqSaveOfferPrice.builder()
                    .status(5).offerId(idOfferCommon).price("-5")
                    .build();

            Response response = given()
                    .spec(merchantSpec)
                    .header("authorization", authToken)
                    .header("accept", ContentType.JSON)
                    .body(body)
                    .post(catalog_products.pathSaveOfferPrice(String.valueOf(idProductCommon)));

            steps.checkResponseWithStatusCodeAndLogView(response, 422);
            Assert.assertEquals("Msg must be display.","Цена не может быть отрицательным числом",
                    clearQuery(response.jsonPath().getString("errors.price")));
        });
    }

    @Test
    public void testAddingPriceFromNonIntegerNumberToProduct(){

        step("Добавление нецелого числа цены товара", () -> {
            ReqSaveOfferPrice body = ReqSaveOfferPrice.builder()
                    .status(5).offerId(idOfferCommon).price("5.66")
                    .build();

            Response response = given()
                    .spec(merchantSpec)
                    .header("authorization", authToken)
                    .header("accept", ContentType.JSON)
                    .body(body)
                    .post(catalog_products.pathSaveOfferPrice(String.valueOf(idProductCommon)));

            steps.checkResponseWithStatusCodeAndLogView(response, 422);
            Assert.assertEquals("Msg must be display.","Цена должна быть целым числом",
                    clearQuery(response.jsonPath().getString("errors.price")));
        });
    }

    @Test
    public void testAddingPriceProductWithSymbols(){

        step("Добавление символов в цену товара", () -> {
            ReqSaveOfferPrice body = ReqSaveOfferPrice.builder()
                    .status(5).offerId(idOfferCommon).price("safdsf")
                    .build();

            Response response = given()
                    .spec(merchantSpec)
                    .header("authorization", authToken)
                    .header("accept", ContentType.JSON)
                    .body(body)
                    .post(catalog_products.pathSaveOfferPrice(String.valueOf(idProductCommon)));

            steps.checkResponseWithStatusCodeAndLogView(response, 422);
            Assert.assertEquals("Msg must be display.","Цена должна быть числом, Цена должна быть целым числом",
                    clearQuery(response.jsonPath().getString("errors.price")));
        });
    }

    @Test
    public void testAddingStocksProductWithSymbols(){

        step("Добавление разлиычнх символов в остатки по складу", () -> {
            ReqSaveStocks body = ReqSaveStocks.builder()
                    .offerId(idOfferCommon).stocks(Stocks.builder().jsonMember1("MRET#$").build()).build();

            Response response = given()
                    .spec(merchantSpec)
                    .header("authorization", authToken)
                    .header("accept", ContentType.JSON)
                    .body(body)
                    .post(catalog_products.pathSaveStocks(String.valueOf(idProductCommon)));

            steps.checkResponseWithStatusCodeAndLogView(response, 500);
        });
    }



}
