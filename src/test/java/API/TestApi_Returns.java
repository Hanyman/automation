package API;

import constants.data.*;
import env.Base_api;
import io.qameta.allure.Description;
import io.restassured.specification.RequestSpecification;
import org.testng.annotations.*;
import steps.api.admin.OrdersAdminApiSteps;
import steps.api.admin.Returns.ReturnsAdminApiSteps;
import steps.api.market.CartApiSteps;
import steps.api.market.LoginMarketApiSteps;
import steps.api.market.ReturnsApiSteps;
import steps.api.merchant.OrdersMerchApiSteps;
import steps.api.merchant.ReturnsMasApiSteps;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class TestApi_Returns extends Base_api {
    String auth, numbOrder;
    int idMerchOrder, idOrder,numbOrderForReturn;
    CartApiSteps cartApiSteps = new CartApiSteps();
    OrdersAdminApiSteps adminOrderApiSteps = new OrdersAdminApiSteps();
    OrdersMerchApiSteps ordersMerchApiSteps = new OrdersMerchApiSteps();
    ReturnsAdminApiSteps returnsAdminApiSteps = new ReturnsAdminApiSteps();
    ReturnsMasApiSteps returnsMasApiSteps = new ReturnsMasApiSteps();
    OrdersMerchApiSteps merchOrderApiSteps = new OrdersMerchApiSteps();
    Map<String, String> idsOrder = new HashMap<String, String>();
    LoginMarketApiSteps marketApiSteps = new LoginMarketApiSteps();
    ReturnsApiSteps returnsApiSteps = new ReturnsApiSteps();
    private static RequestSpecification marketSpecReturnMerch, merchGeneral, merchSecond;

    @BeforeMethod
    private void preconditionMethod(){
        marketSpecReturnMerch = getTokensMarket(Phone.FOR_RETURN_DEFAULT);
    }
    @AfterMethod
    private void postconditionMethod(){
        if (marketSpecReturnMerch!=null) {
        marketApiSteps.logoutClient(marketSpecReturnMerch);}
    }

    @AfterClass
    private void postconditionClass(){
        if (merchSecond!=null) {
            logoutMerchantPanel(merchSecond);
            getTokensAuthMerchant(generalMerchConfig.getMerchantLogin(), generalMerchConfig.getMerchantPassword());
        }

    }
    @Test(priority =1)
    @Description("Цепочка Возврат (Клиент - Мерчант).")
    public void testApiReturnOrderClientToMerch(){
        numbOrderForReturn = ordersMerchApiSteps.createNewOrder(marketSpecReturnMerch, generalMerchConfig.getProductFirst(), 1, PaymentMethods.CASH,"no");
        String returnId = returnsApiSteps.addItemsToReturns(marketSpecReturnMerch,numbOrderForReturn);
        ArrayList<Integer> itemIds = returnsApiSteps.addReturnItemsInfo(marketSpecReturnMerch,numbOrderForReturn);
        String itemId =String.valueOf(itemIds.get(0));
        String idImage = returnsApiSteps.uploadFile();
        returnsApiSteps.addReasonPhotoDescToReturn(marketSpecReturnMerch,numbOrderForReturn, itemId,idImage,2,"Text Reason");
        returnsApiSteps.getReturnInfo(marketSpecReturnMerch,returnId,"Формируется возврат");
        returnsApiSteps.updateDelivery(marketSpecReturnMerch,returnId);
        returnsApiSteps.sendReturn(marketSpecReturnMerch,returnId);
        returnsApiSteps.getReturnInfo(marketSpecReturnMerch,returnId,"Оформлен возврат");

        //admin
        returnsAdminApiSteps.changeReturnStatus(returnId);

        //market
        returnsMasApiSteps.changeReturnStatus(returnId);
        returnsMasApiSteps.returnsAcceptItems(itemId);


    }

    @Test(priority =2)
    @Description("Цепочка Возврат (Клиент - ЦСВ).")
    public void testApiReturnOrderClientToCSV(){
        String firstItem, secondItem, imageFirst,imageSecond;
        int idOrder, numberOrder;
        idsOrder = ordersMerchApiSteps.createNewOrderFewMerchants(marketSpecReturnMerch, generalMerchConfig.getProductFirst(),secondMerchConfig.getProductFirst(), 1, PaymentMethods.CASH);
        idOrder = Integer.parseInt(idsOrder.get("idOrder"));
        numberOrder = Integer.parseInt(idsOrder.get("numberOrder"));
        ordersMerchApiSteps.assemblyOrder(merchantSpec,idsOrder,1);

        logoutMerchantPanel(merchantSpec);

        merchSecond = getTokensMerchant(secondMerchConfig.getMerchantLogin(),secondMerchConfig.getMerchantPassword());
        ordersMerchApiSteps.assemblyOrder(merchSecond,idsOrder,1);

        adminOrderApiSteps.setStatusOrderDelivered(Integer.parseInt(idsOrder.get("idOrder")),idsOrder.get("numberOrder"));

        String returnId = returnsApiSteps.addItemsToReturns(marketSpecReturnMerch,idOrder);
        ArrayList<Integer> itemId = returnsApiSteps.addReturnItemsInfo(marketSpecReturnMerch,2555656);
        firstItem =String.valueOf(itemId.get(0));
        secondItem =String.valueOf(itemId.get(1));
        imageFirst = returnsApiSteps.uploadFile();
        imageSecond = returnsApiSteps.uploadFile();
        returnsApiSteps.addReasonPhotoDescToReturn(marketSpecReturnMerch,idOrder, firstItem,imageFirst,2,"Text Reason111");
        returnsApiSteps.addReasonPhotoDescToReturn(marketSpecReturnMerch,idOrder, secondItem,imageSecond,1,"Text Reason222");
        returnsApiSteps.getReturnInfo(marketSpecReturnMerch,returnId,"Формируется возврат");
        returnsApiSteps.updateDelivery(marketSpecReturnMerch,returnId);
        returnsApiSteps.sendReturn(marketSpecReturnMerch,returnId);
        returnsApiSteps.getReturnInfo(marketSpecReturnMerch,returnId,"Оформлен возврат");

        //admin
        returnsAdminApiSteps.changeReturnStatus(returnId);
        returnsAdminApiSteps.changeReturnStatusAccept(returnId,"300");
        returnsAdminApiSteps.acceptReturnItem(firstItem);
        returnsAdminApiSteps.acceptReturnItem(secondItem);
        returnsAdminApiSteps.getReturnStatus(returnId,"Принят");
        returnsAdminApiSteps.changeReturnStatusAccept(returnId,"320");
        returnsAdminApiSteps.getReturnStatus(returnId,"Обработан на ЦСВ");

    }

    //@Test(priority =3,dependsOnMethods = { "testApiReturnOrderClientToCSV" })
    @Test(priority =3,enabled = false)
    @Description("Цепочка Возврат (ЦСВ - Мерчант).")
    public void testApiReturnOrderCSVtoMerch(){


    }



}
