package API;

import api_model.request.merchant.registration.Assortment;
import api_model.request.merchant.registration.ReqRegistration;
import api_model.response.merchant.checkEmail.RespCheckEmail;
import api_model.response.merchant.common.uploadFile.RespUpload;
import env.Base_api;
import helpers.Generator;
import io.qameta.allure.Description;
import io.restassured.response.Response;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import steps.api.market.LoginMarketApiSteps;
import steps.api.merchant.ActivationMerchantApiSteps;
import steps.api.merchant.CommonMerchantApiSteps;

import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;

import static constants.end_point.MerchantPanelEndpoint.registration;
import static constants.end_point.MerchantPanelEndpoint.registration_user_exist;
import static constants.merchant.StatusMerchant.ACTIVATION;
import static constants.merchant.StatusMerchant.ACTIVE;
import static constants.merchant.TypeOrganization.IP;
import static io.qameta.allure.Allure.step;
import static io.restassured.RestAssured.given;


public class TestApi_ActivationMerchant extends Base_api {
    ActivationMerchantApiSteps activationMerchantApiSteps = new ActivationMerchantApiSteps();
    CommonMerchantApiSteps commonApiSteps = new CommonMerchantApiSteps();
    Generator generator;
    LoginMarketApiSteps marketApiSteps = new LoginMarketApiSteps();

    @BeforeMethod
    private void getTokensForMerchant(){
        generator = new Generator();
        getTokensMerchant();
    }

    @AfterMethod
    private void logout(){
        //marketApiSteps.logoutClient();
    }
    @Test
    @Description("Регистрация и активация мерчанта ИП.")
    public void testRegistrationAndActivationMerchantIP() throws MalformedURLException, UnsupportedEncodingException {
        // prepare
        String company = generator.getCompany();
        String firstName = generator.getFirstName();
        String lastName = generator.getLastName();
        String middleName = generator.getMiddleName();
        String phoneNumb = generator.getPhoneNumb();
        String inn = generator.getInn();
        String email = getEmail();
        int typeOrg = IP.getTypeOrg();

        // action
        activationMerchantApiSteps.registrationMerchant(String.valueOf(typeOrg),company,firstName,lastName,middleName,inn,phoneNumb, email);
        activationMerchantApiSteps.setPassword(email);
        activationMerchantApiSteps.getMerchant();
        activationMerchantApiSteps.changeStatusMerchantOnActivation(ACTIVATION.getStatusMerch());
        activationMerchantApiSteps.addInfoMerchant(typeOrg);
        activationMerchantApiSteps.changeStatusMerchantOnAFullCheck(typeOrg, ACTIVE.getStatusMerch());
        activationMerchantApiSteps.getMerchantCheckStatus(ACTIVE.getStatusMerch());
    }

    @Test
    public void testVerifyExistEmail(){

        step("Проверка ошибки при вводе существующего email.", () -> {
            Response response = given()
                    .spec(merchantSpec)
                    .param("email", EXIST_EMAIL)
                    .get(registration_user_exist.path());

            RespCheckEmail exEmail = steps.checkResponseWithStatusCodeAndLogView(response, 200).as(RespCheckEmail.class);
            Assert.assertEquals(exEmail.isExists(),true);
        });
    }

    @Test
    public void testRegistrationWhitExistEmail(){
        // prepare
        String company = generator.getCompany();
        String firstName = generator.getFirstName();
        String lastName = generator.getLastName();
        String middleName = generator.getMiddleName();
        String phoneNumb = generator.getPhoneNumb();
        String inn = generator.getInn();
        String email = "nomakanton@yandex.ru";
        int typeOrg = IP.getTypeOrg();

        step("Проверка регистрации мерчанта с существующим email.", () -> {

            RespUpload file = commonApiSteps.upload("merchantAssortment", FILE_XLSX);
            ReqRegistration body = ReqRegistration.builder().legal_name(company).first_name(firstName).last_name(lastName).middle_name(middleName)
                    .phone(phoneNumb).inn(inn).email(email).can_integration(false).storage_address("Mosk")
                    .assortment(Assortment.builder().id(file.getId()).name(file.getName()).url(file.getUrl()).build())
                    .communication_method("").bank_address("").organization_form(String.valueOf(typeOrg)).kpp("").sale_info("").bank("").password("").site("").bank_bik("")
                    .payment_account("").fact_address("").legal_address("").password_confirmation("").correspondent_account("").build();

            Response response = given()
                    .spec(merchantSpec)
                    .body(body)
                    .post(registration.path());

            steps.checkResponseWithStatusCodeAndLogView(response, 500);
        });
    }
    @Test
    public void testRegistrationWhithoutRequiredField(){
        // prepare
        int typeOrg = IP.getTypeOrg();

        step("Проверка регистрации мерчанта без обязательных полей.", () -> {

            RespUpload file = commonApiSteps.upload("merchantAssortment", FILE_XLSX);
            ReqRegistration body = ReqRegistration.builder().legal_name("").first_name("").last_name("").middle_name("")
                    .phone("").inn("").email("").can_integration(false).storage_address("Mosk")
                    .assortment(Assortment.builder().id(file.getId()).name(file.getName()).url(file.getUrl()).build())
                    .communication_method("").bank_address("").organization_form(String.valueOf(typeOrg)).kpp("").sale_info("").bank("").password("").site("").bank_bik("")
                    .payment_account("").fact_address("").legal_address("").password_confirmation("").correspondent_account("").build();

            Response response = given()
                    .spec(merchantSpec)
                    .body(body)
                    .post(registration.path());

            steps.checkResponseWithStatusCodeAndLogView(response, 400);
        });
    }

    @Test
    public void testRegistrationWhithoutFile(){

        // prepare
        String company = generator.getCompany();
        String firstName = generator.getFirstName();
        String lastName = generator.getLastName();
        String middleName = generator.getMiddleName();
        String phoneNumb = generator.getPhoneNumb();
        String inn = generator.getInn();
        String email = generator.getEmail();
        int typeOrg = IP.getTypeOrg();

            step("Проверка регистрации мерчанта без файла.", () -> {

                ReqRegistration body = ReqRegistration.builder().legal_name(company).first_name(firstName).last_name(lastName).middle_name(middleName)
                        .phone(phoneNumb).inn(inn).email(email).can_integration(false).storage_address("Mosk")
                        .assortment(Assortment.builder().id(0).name("").url("").build())
                        .communication_method("").bank_address("").organization_form(String.valueOf(typeOrg)).kpp("").sale_info("").bank("").password("").site("").bank_bik("")
                        .payment_account("").fact_address("").legal_address("").password_confirmation("").correspondent_account("").build();

                Response response = given()
                    .spec(merchantSpec)
                    .body(body)
                    .post(registration.path());

            steps.checkResponseWithStatusCodeAndLogView(response, 500);
        });
    }


    // Проверка существования email при регистрацц, если такой уже есть то повторный запрос на гереацию нового мыла
    private String getEmail(){
        String email=null;
        int emailCheck=0;
        while(emailCheck< 10){
            email = generator.getEmail();
            if (activationMerchantApiSteps.checkExistEmail(email) == false){
                return email;
            }
            emailCheck++;
        }
        return email;
    }
}
