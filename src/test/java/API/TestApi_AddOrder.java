package API;

import com.jayway.jsonpath.JsonPath;
import constants.data;
import env.Base_api;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import steps.api.admin.OrdersAdminApiSteps;
import steps.api.market.CartApiSteps;
import steps.api.market.LoginMarketApiSteps;
import steps.api.merchant.OrdersMerchApiSteps;

import java.util.HashMap;
import java.util.Map;

import static helpers.api.ApiHelper.clearQuery;
import static helpers.api.ApiHelper.getParamJson;
import static io.restassured.RestAssured.given;

public class TestApi_AddOrder extends Base_api {
    String auth, numbOrder;
    int idMerchOrder, idOrder;
    CartApiSteps cartApiSteps = new CartApiSteps();
    OrdersAdminApiSteps adminOrderApiSteps = new OrdersAdminApiSteps();
    OrdersMerchApiSteps merchOrderApiSteps = new OrdersMerchApiSteps();
    Map<String, String> idsOrder = new HashMap<String, String>();
    LoginMarketApiSteps marketApiSteps = new LoginMarketApiSteps();
    private static RequestSpecification marketSpecification;
    @BeforeMethod
    private void precondition(){
        marketSpecification = getTokensMarket(data.Phone.FOR_ORDER_DEFAULT);
    }
    @AfterMethod
    private void postcondition(){
        marketApiSteps.logoutClient(marketSpecification);
    }

    @Test
    public void testApiPaymentAndOrderProcessing(){

        merchOrderApiSteps.createNewOrder(marketSpecification,generalMerchConfig.getProductFirst(),2,"1","yes");

    }

    protected String getStrInfoFromHtml(Response response, String jsonPath){
        Document doc = Jsoup.parse(response.getBody().asString());
        Element div = doc.getElementById("app");
        String getAttr = div.attr("data-props");
        String info = getParamJson(JsonPath.read(getAttr,jsonPath).toString());
        return clearQuery(info);
    }
}
