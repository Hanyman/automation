package UI.Admin;


import constants.Errors;
import env.Base_Container;
import helpers.Generator;
import helpers.logs.TestListener;
import io.qameta.allure.Description;
import io.qameta.allure.Story;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;
import steps.UI.GeneralSteps;
import steps.UI.OpenPageSteps;
import steps.UI.admin.MerchantsTabSteps;

import static constants.end_point.AdminPanelEndpoint.merchant_list_active;
@Listeners({ TestListener.class })
public class TestCreateNewMerchant extends Base_Container {
    Generator generator = new Generator();
    MerchantsTabSteps merchantTabSteps = new MerchantsTabSteps();
    OpenPageSteps openPageSteps = new OpenPageSteps();
    GeneralSteps generalSteps = new GeneralSteps();

    @BeforeClass
    public void classSetup() {
        openPageSteps.stepOpenPageAdmin("");
        generalSteps.loginAdminPage(adminConfig.getAdminLogin(),adminConfig.getAdminPassword());

    }
    @BeforeMethod
    public void methodSetup() {
        generator = new Generator();
        openPageSteps.stepOpenPageAdmin(merchant_list_active.path());
        merchantTabSteps.clickCreateMerchantBtn();
    }

    @Test
    @Description("Проверка отображения ошибки, при вводе существующего email.")
    public void testCheckErrorExistEmail() {
        merchantTabSteps.inputEmailField(generalMerchConfig.getMerchantLogin())
                .checkEmailErrorField(Errors.EXIST_EMAIL);
    }

    @Test
    @Description("Регистрация мерчанта из админ. панели.")
    public void testCorrectionCreateMerchant() throws InterruptedException {
        String inn = generator.getInn();
        String kpp = generator.getKpp();
        String orgName = generator.getCompany();
        String bankAccount = generator.getBank_account();
        String nameClient = generator.getFirstName()+" " + generator.getLastName();
        String phone = generator.getPhoneNumb();
        String email = generator.getEmail();
        String password = generator.getPassword();
        String site = generator.getDomainName();

        merchantTabSteps.createMerchant(inn,kpp,"ИП",orgName,bankAccount,nameClient,phone,email,"Email",password,site)
                .waitDisappearLoader()
                .waitDisappearCreateNewMerchForm();

        merchantTabSteps.verifyTitleMerchantInCard(orgName)
                .verifyStatusMerchantInCard("Активация");

    }

    @Test
    @Description("Регистрация мерчанта из админ. панели с банковским аккаунтом больше 20 цифр")
    public void testCorrectionCreateMerchant_withPaymentAccountGreat20() throws InterruptedException {
        String inn = generator.getInn();
        String kpp = generator.getKpp();
        String orgName = generator.getCompany();
        String bankAccount = generator.getBank_account_21();
        String nameClient = generator.getFirstName()+" " + generator.getLastName();
        String phone = generator.getPhoneNumb();
        String email = generator.getEmail();
        String password = generator.getPassword();
        String site = generator.getDomainName();

        merchantTabSteps.createMerchant(inn,kpp,"ИП",orgName,bankAccount,nameClient,phone,email,"Email",password,site)
                .waitDisappearLoader()
                .checkPaymentAccountErrorField(Errors.SYMBOLS_MUST_BE_20);;


    }

    @Test
    @Description("Проверка отображения обязательных полей при регистрации.")
    public void testRegistrationMerchantEmptyFields() {
        merchantTabSteps.clickSaveBtn()
                .checkErrorObligatoryFields();
    }

    @Test
    @Description("Проверка ошибки о недостаточном кол-ве символов под полем KPP(меньше 9).")
    public void testCheckInputSymbolsKppField_less9() {
        merchantTabSteps.inputKppField("1233")
                .checkKppErrorField(Errors.SYMBOLS_MUST_BE_9);

    }
    @Test
    @Description("Проверка ошибки о недостаточном кол-ве символов под полем KPP(больше 9).")
    public void testCheckInputSymbolsKppField_great9() {
        merchantTabSteps.inputKppField("1234567890")
                .checkKppErrorField(Errors.SYMBOLS_MUST_BE_9);

    }
    @Test
    @Description("Проверка ошибки о недостаточном кол-ве символов под полем ИНН(меньше 10).")
    public void testCheckInputSymbolsInnField_less10() {
        merchantTabSteps.inputInnField(generator.getInn_9())
                .checkInnErrorField(Errors.SYMBOLS_IS_LESS_10);

    }
    @Test
    @Description("Проверка ошибки о недостаточном кол-ве символов под полем Номер банковского счета(меньше 20).")
    public void testCheckInputSymbolsPaymentAccountField_less20() {
        merchantTabSteps.inputPaymentAccountField(generator.getNumber_15())
                .checkPaymentAccountErrorField(Errors.SYMBOLS_MUST_BE_20);

    }

    @Test
    @Description("Проверка ошибки о недостаточном кол-ве символов под полем Номер корреспондентского счета(меньше 20).")
    public void testCheckInputSymbolsCorrespondentAccountField_less20() {
        merchantTabSteps.inputCorrespondentAccountField(generator.getNumber_15())
                .checkCorrespondentAccountErrorField(Errors.SYMBOLS_MUST_BE_20);

    }
    @Test
    @Description("Проверка ошибки о недостаточном кол-ве символов под полем Номер корреспондентского счета(больше 20).")
    public void testCheckInputSymbolsCorrespondentAccountField_great20() {
        merchantTabSteps.inputCorrespondentAccountField(generator.getBank_account_21())
                .checkCorrespondentAccountErrorField(Errors.SYMBOLS_MUST_BE_20);

    }
    @Test
    @Description("Проверка ошибки о недостаточном кол-ве символов под полем ИНН(между 10 и 12).")
    public void testCheckInputSymbolsInnField_between10and12() {
        merchantTabSteps.inputInnField(generator.getInn_11())
                .checkInnErrorField(Errors.SYMBOLS_BETWEEN_10_AND_12);
    }
    @Test
    @Description("Проверка ошибки о недостаточном кол-ве символов под полем ИНН(больше 12).")
    public void testCheckInputSymbolsInnField_Great12() {
        merchantTabSteps.inputInnField(generator.getInn_13())
                .checkInnErrorField(Errors.SYMBOLS_IS_LESS_12);
    }

    @Test
    @Description("Проверка отображения ошибки под полем Телефон.(меньше 11)")
    public void testInputIncorrectPhoneNumber_less11() {
        merchantTabSteps.inputPhoneField("+7923")
                .checkPhoneErrorField(Errors.INCORRECT_FORMAT_PHONE);

    }
    @Test
    @Description("Проверка отображения ошибки под полем Телефон.(больше 11)")
    public void testInputIncorrectPhoneNumber_great11() {
        merchantTabSteps.inputPhoneField("+792312345432112323")
                .checkPhoneErrorField(Errors.INCORRECT_FORMAT_PHONE);

    }

    @Test
    @Description("Проверка отображения ошибки при вводе невалидного email.")
    public void testInputIncorrectEmail() {
        merchantTabSteps.inputEmailField("123")
                .checkEmailErrorField(Errors.INCORRECT_EMAIL);
    }

    @Test
    @Description("Проверка отображения ошибки при вводе разных паролей в полях пароль и подтверждение пароля.")
    public void testCheckForPasswordMismatch() {
        merchantTabSteps.inputPasswordConfirmField("123")
                .checkPasswordConfirmErrorField(Errors.PASSWORD_DO_NOT_MATCH);
    }


}
