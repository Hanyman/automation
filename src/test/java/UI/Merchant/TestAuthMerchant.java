package UI.Merchant;

import com.codeborne.selenide.Selenide;
import env.Base_Container;
import helpers.logs.TestListener;
import io.qameta.allure.Description;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;
import steps.UI.OpenPageSteps;
import steps.UI.admin.MerchantsTabSteps;
import steps.UI.merchant.CommonMerchantSteps;
import steps.UI.merchant.RegistrationSteps;

import steps.api.admin.*;

import static constants.end_point.AdminPanelEndpoint.merchant_detail;
import static helpers.Helper.switchOnAnotherWindow;

@Listeners({ TestListener.class })
public class TestAuthMerchant extends Base_Container {
    OpenPageSteps openPageSteps = new OpenPageSteps();
    MerchantsTabSteps merchantTabSteps = new MerchantsTabSteps();
    MerchantsAdminApiSteps merchantsAdminApiSteps = new MerchantsAdminApiSteps();

    CommonMerchantSteps commonMerchantSteps = new CommonMerchantSteps();
    String idMerchant;
    String merchantName="Rutherford-Dicki";
    @BeforeMethod
    public void preconditionMethod() {

        idMerchant = merchantsAdminApiSteps.getMerchantId(merchantName);
        merchantsAdminApiSteps.setStatusMerchant(idMerchant,merchantName,"7","");
    }

    @AfterMethod
    public void postconditionMethod() {

        Selenide.clearBrowserCookies();
        Selenide.clearBrowserLocalStorage();
    }

    @Test
    @Description("Проверка изменения статуса мерчанта - Приостановлен.")
    public void testCheckChangeStatusMerchantSuspended() throws InterruptedException {
        String msg = "приостановлен за неуплату";
        openPageSteps.loginAdminPage();
        merchantsAdminApiSteps.setStatusMerchant(idMerchant,merchantName,"8",msg);
        openPageSteps.stepOpenPageAdmin(merchant_detail.path()+"/"+idMerchant);
        merchantTabSteps.clickLogInAsMerchant();
        switchOnAnotherWindow();

        commonMerchantSteps.waitMerchantPage();
        commonMerchantSteps.checkMerchantStatus("Приостановлен");
        commonMerchantSteps.checkMerchantMsg(msg);
    }

    @Test
    @Description("Проверка изменения статуса мерчанта - Полная проверка.")
    public void testCheckChangeStatusMerchantFullCheck() throws InterruptedException {

        openPageSteps.loginAdminPage();
        merchantsAdminApiSteps.setStatusMerchant(idMerchant,merchantName,"6","");
        openPageSteps.stepOpenPageAdmin(merchant_detail.path()+"/"+idMerchant);
        merchantTabSteps.clickLogInAsMerchant();
        switchOnAnotherWindow();

        commonMerchantSteps.waitMerchantPage();
        commonMerchantSteps.checkMerchantStatus("Активация");

    }

    @Test
    @Description("Проверка изменения статуса мерчанта - Корректировка информации.")
    public void testCheckChangeStatusMerchantCorrectionInformation() throws InterruptedException {
        String msg = "Корректировка инфы";
        openPageSteps.loginAdminPage();
        merchantsAdminApiSteps.setStatusMerchant(idMerchant,merchantName,"10",msg);
        openPageSteps.stepOpenPageAdmin(merchant_detail.path()+"/"+idMerchant);
        merchantTabSteps.clickLogInAsMerchant();
        switchOnAnotherWindow();

        commonMerchantSteps.waitMerchantPage();
        commonMerchantSteps.checkMerchantStatusCorrectionInfo("Требуется корректировка информации.");
        commonMerchantSteps.checkMerchantMsgCorrectionInfo(msg);

    }

    @Test
    @Description("Проверка изменения статуса мерчанта - Отключен.")
    public void testCheckChangeStatusMerchantDisabled() throws InterruptedException {
        String msg = "Отключен полностью";
        openPageSteps.loginAdminPage();
        merchantsAdminApiSteps.setStatusMerchant(idMerchant,merchantName,"9",msg);
        openPageSteps.stepOpenPageAdmin(merchant_detail.path()+"/"+idMerchant);
        merchantTabSteps.clickLogInAsMerchant();
        switchOnAnotherWindow();

        commonMerchantSteps.waitMerchantPage();
        commonMerchantSteps.checkMerchantStatusDisabled("Доступ закрыт");

    }


}
