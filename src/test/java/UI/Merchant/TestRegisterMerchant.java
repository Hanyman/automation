package UI.Merchant;

import static constants.end_point.MerchantPanelEndpoint.*;

import env.Base_Container;
import helpers.Generator;
import helpers.logs.TestListener;
import io.qameta.allure.Description;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;
import steps.UI.OpenPageSteps;
import steps.UI.merchant.RegistrationSteps;

import static constants.end_point.AdminPanelEndpoint.*;
@Listeners({ TestListener.class })
public class TestRegisterMerchant extends Base_Container {
    Generator generator = new Generator();
    String longName = generator.getLongName();

    RegistrationSteps registrationSteps = new RegistrationSteps();
    OpenPageSteps openPageSteps = new OpenPageSteps();
//
//    @BeforeMethod
//    public void profileSetup() {
//        openPageSteps.stepOpenPageMerchant(registration.path());
//
//    }


    @Test
    @Description("Reg-12. Проверка отображения ошибки, при вводе существующего email.")
    public void testCheckErrorExistEmail() {
        openPageSteps.stepOpenPageMerchant(registration.path());
        registrationSteps.stepInputFieldEmailAndClick(generalMerchConfig.getMerchantLogin());
        registrationSteps.stepCheckErrorExistEmailFieldEmail();
    }

    @Test
    @Description("Reg-1. Регистрация мерчанта из админ. панели.")
    public void testCorrectionCreateMerchant() throws InterruptedException {
        openPageSteps.stepOpenPageMerchant(registration.path());
        openPageSteps.loginAdminPage();
        openPageSteps.stepOpenPageAdmin(merchant_list_active.path());

    }

    @Test
    @Description("Reg-2. Регистрация мерчанта через форму на сайте.")
    public void testCorrectRegistrationMerchant() {
        openPageSteps.stepOpenPageMerchant(registration.path());
        registrationSteps.createMerchant();
    }

    @Test
    @Description("Reg-3. Проверка отображения обязательных полей при регистрации.")
    public void testRegistrationMerchantEmptyFields() {
        openPageSteps.stepOpenPageMerchant(registration.path());
        registrationSteps.stepClickRegistrationBtn();
        registrationSteps.stepCheckErrorObligatoryFieldSurname();
        registrationSteps.stepCheckErrorObligatoryFieldName();
        registrationSteps.stepCheckErrorObligatoryFieldPhone();
        registrationSteps.stepCheckErrorObligatoryFieldEmail();
        registrationSteps.stepCheckErrorObligatoryFieldInn();
        registrationSteps.stepCheckErrorObligatoryFieldOrganization();

    }

    @Test
    @Description("Reg-4. Проверка лимита ввода символов в поле Имя.")
    public void testCheckExceedingLimitFieldName() {
        openPageSteps.stepOpenPageMerchant(registration.path());
        registrationSteps.stepInputNameField(longName);
        registrationSteps.stepCheckErrorMaximumLengthFieldName();

    }

    @Test
    @Description("Reg-5. Проверка лимита ввода символов в поле Фамилия.")
    public void testCheckExceedingLimitFieldSurname() {
        openPageSteps.stepOpenPageMerchant(registration.path());
        registrationSteps.stepInputSurnameField(longName);
        registrationSteps.stepCheckErrorMaximumLengthFieldSurname();

    }

    @Test
    @Description("Reg-6. Проверка лимита ввода символов в поле Отчество.")
    public void testCheckExceedingLimitFieldMiddleName() {
        openPageSteps.stepOpenPageMerchant(registration.path());
        registrationSteps.stepInputMiddleNameField(longName);
        registrationSteps.stepCheckErrorMaximumLengthFieldMiddleName();

    }

    @Test
    @Description("Reg-7. Проверка отображения ошибки под полем Телефон.")
    public void testInputIncorrectPhoneNumber() {
        openPageSteps.stepOpenPageMerchant(registration.path());
       // registrationSteps.stepClearFieldPhone();
        registrationSteps.stepInputFieldNameAndClick("+71234");
        registrationSteps.stepCheckErrorIncorrectNumberFieldPhone();

    }

    @Test
    @Description("Reg-8. Проверка отображения ошибки при вводе невалидного email.")
    public void testInputIncorrectEmail() {
        openPageSteps.stepOpenPageMerchant(registration.path());
        registrationSteps.stepClearFieldEmail();
        registrationSteps.stepInputFieldEmailAndClick("email");
        registrationSteps.stepCheckErrorIncorrectEmailFieldEmail();
    }

    @Test
    @Description("Reg-9. Проверка ошибки о недостаточном кол-ве символов под полем ИНН.")
    public void testInputHalfINN() {
        openPageSteps.stepOpenPageMerchant(registration.path());
        registrationSteps.stepClearFieldInn();
        registrationSteps.stepInputFieldInnAndClick("12345");
        registrationSteps.stepCheckErrorIncorrectInnFieldInn();
    }


    @Test
    @Description("Reg-10. Проверка ввода некорректных символов в поле ИНН.")
    public void testInputIncorrectINN() {
        openPageSteps.stepOpenPageMerchant(registration.path());
        registrationSteps.stepClearFieldInn();
        registrationSteps.stepInputFieldInnAndClick("#@$EWDRWF");
        registrationSteps.stepCheckErrorOnlyDigitsFieldInn();

    }

    @Test
    @Description("Reg-11. Проверка ввода некорректных символов в поле Телефон.")
    public void testInputIncorrectSymbolPhone() {
        openPageSteps.stepOpenPageMerchant(registration.path());
        registrationSteps.stepClearFieldPhone();
        registrationSteps.stepInputFieldNameAndClick("!@#^&_абвгд)*.");
        registrationSteps.stepCheckErrorIncorrectFormatFieldPhone();

    }


}
