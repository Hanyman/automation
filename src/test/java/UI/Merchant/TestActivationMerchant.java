package UI.Merchant;

import env.Base_Container;
import helpers.Generator;

import helpers.logs.TestListener;
import io.qameta.allure.Description;
import io.qameta.allure.Feature;
import io.qameta.allure.Story;
import org.aeonbits.owner.ConfigFactory;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;
import steps.UI.merchant.ActivationSteps;
import steps.UI.merchant.RegistrationSteps;

import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;

@Feature("Web testing")
@Story("Мерчант. Активация")
@Listeners({ TestListener.class })
public class TestActivationMerchant extends Base_Container {

    Generator generator;
    String email, nameCompany;

    RegistrationSteps registrationSteps = new RegistrationSteps();
    ActivationSteps activationSteps = new ActivationSteps();

    @BeforeMethod
    public void setSetup() {

        generator = new Generator();
        email = generator.getEmail();
        nameCompany = generator.getCompany();
        //RestAssured.requestSpecification = RequestSpec(urlConfig.getAdminUrl()+ PathUrlAdmin.merchant_confirm_email_link.getPath());
        registrationSteps.createMerchant(email,nameCompany);
    }

    @Test
    @Description("Активация мерчанта.")
    public void testCorrectActivationMerchant() throws InterruptedException, UnsupportedEncodingException, MalformedURLException {

        activationSteps.stepActivateMerchant(nameCompany,email);

        activationSteps.stepInputPasswordMerchant();
        activationSteps.stepInputOrganizationInfo();
        activationSteps.stepUploadFiles();
        activationSteps.stepClickSendOrderBtn();
        activationSteps.stepCheckStatusActivation();
        activationSteps.stepCheckStatusMerchantInAdmin(nameCompany,"Полная проверка");
        activationSteps.stepClickNameMerchantInAdmin(nameCompany);
        activationSteps.stepSelectStatusMerchantInAdmin("Действует");
        activationSteps.stepCheckStatusMerchantInAdmin(nameCompany,"Действует");


    }





}
