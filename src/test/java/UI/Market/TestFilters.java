package UI.Market;

import constants.data.*;
import env.Base_Container;
import helpers.logs.TestListener;
import io.qameta.allure.Description;

import lombok.SneakyThrows;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;
import steps.UI.OpenPageSteps;
import steps.UI.market.SearchFiltersSteps;
@Listeners({ TestListener.class })
public class TestFilters extends Base_Container {
    SearchFiltersSteps searchFiltersSteps = new SearchFiltersSteps();
    OpenPageSteps openPageSteps = new OpenPageSteps();

    @SneakyThrows
    @BeforeMethod
    public void openPage() {
        openPageSteps.stepOpenPageMarket("");

    }

    @Test
    @Description("F-1. Фильтрация товара по категориям.")
    public void testSearchProductWithFilterCategories() {
       searchFiltersSteps.clickProductMenu()
                         .clickSwitCategory()
                         .waitAppearCardProduct(Products.PRODUCT_TEST_1)
                         .waitDisappearCardProduct(Products.PRODUCT_TEST_2);
    }


}
