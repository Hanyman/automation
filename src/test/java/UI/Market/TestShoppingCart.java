package UI.Market;

import constants.data.*;
import env.Base_Container;
import helpers.logs.TestListener;
import io.qameta.allure.Description;
import io.qameta.allure.Feature;
import io.qameta.allure.Story;

import org.testng.ITestResult;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;
import steps.UI.OpenPageSteps;
import steps.UI.market.SearchFiltersSteps;
import steps.UI.market.ShoppingCartSteps;

import static helpers.Helper.getTestName;

@Feature("Web testing")
@Story("Маркет. Корзина")
@Listeners({ TestListener.class })
public class TestShoppingCart extends Base_Container {
    String productName;
    SearchFiltersSteps searchFiltersSteps = new SearchFiltersSteps();
    ShoppingCartSteps shoppingCartSteps = new ShoppingCartSteps();
    OpenPageSteps openPageSteps = new OpenPageSteps();

    @BeforeMethod
    public void openPage(ITestResult iTestResult) throws InterruptedException {
        openPageSteps.stepOpenPageMarket("catalog/knigy/"+"?v="+getTestName(iTestResult)+"");
        //productName = searchFiltersSteps.stepGetProduct("catalog/svitshoty/");
    }

    @Test
    @Description("Добавление товара в корзину через кнопку Купить на товаре.")
    public void testAddProductInShoppingCartThroughButtonOnProductCard() throws InterruptedException {

        shoppingCartSteps.addProductInShoppingCart(Products.PRODUCT_TEST_1)
                         .clickCartBtn()
                         .checkCountProductsInCart("1");
    }

    @Test(enabled = false)
    @Description("Добавление товара в корзину через карточку товара.")
    public void testAddProductInShoppingCartThroughProductCard() throws InterruptedException {

        shoppingCartSteps.addProductInShoppingCartInProduct(Products.PRODUCT_TEST_1)
                         //.clickInCartBtn()
                         .checkCountProductsInCart("1");
    }

}
