package UI.Market;

import constants.Errors;
import constants.data.*;
import helpers.api.ApiHelper;
import helpers.logs.TestListener;
import io.restassured.specification.RequestSpecification;
import org.testng.annotations.*;
import steps.UI.OpenPageSteps;
import steps.api.admin.Customers.CustomersAdminApiSteps;
import steps.api.market.LoginMarketApiSteps;
import steps.UI.market.AuthSteps;
import env.Base_Container;
import io.qameta.allure.*;

import static constants.data.Phone.*;
import static helpers.Helper.getPhoneForAuth;
import static helpers.api.Spec_api.spec_client_with_token_market;
@Listeners({ TestListener.class })
public class TestAuthUser extends Base_Container {

    AuthSteps authSteps = new AuthSteps();
    OpenPageSteps openPageSteps = new OpenPageSteps();
    LoginMarketApiSteps marketApiSteps = new LoginMarketApiSteps();
    CustomersAdminApiSteps customersAdminApiSteps = new CustomersAdminApiSteps();

    private static RequestSpecification marketSpecification;

    @BeforeMethod
    public void openPage() {
        openPageSteps.stepOpenPageMarket("");
        driver = getWebDriver();
        String apims_session;
        apims_session = ApiHelper.getCookies(APIMS_SESSION);
        marketSpecification = spec_client_with_token_market(url_market, apims_session, APIMS_SESSION);
        marketApiSteps.logoutClientWithoutVerifyStatusCode(marketSpecification);
        openPageSteps.stepOpenPageMarket("");
    }

    @Test
    @Description("Проверка ввода невалидных значений в поле для номера телефона.")
    public void testInputIncorrectSymbolInPhoneField() {

        authSteps.clickLoginBtn()
                .inputLoginField("!@#^&_RED")
                .checkFieldPhone("");
    }

    @Test
    @Description("Проверка на недостаточное количество цифр в телефон номере.")
    public void testInputIncorrectPhoneNumber() {

        authSteps.clickLoginBtn()
                .inputLoginField("123")
                .setCheckboxAgreement()
                .checkErrorFieldPhone(Errors.INVALID_NUMBER);
    }

    @Test
    @Description("Проверка ввода  различных символов в поле четырехзначного кода.")
    public void testInputOtherSymbolsCodeSms() {

        authSteps.InputNumberAndCode(getPhoneForAuth(Phone.FOR_ORDER_1),"#$##")
                .checkErrorFieldSms("Обязательное поле");
    }

    @Test
    @Description("Проверка ввода некорректного кода смс.")
    public void testInput_3_SymbolsCodeSms() {

        authSteps.InputNumberAndCode(getPhoneForAuth(Phone.FOR_ORDER_1),"4324")
                .checkErrorFieldSms(Errors.WRONG_CODE);
    }
    @Test
    @Description("Проверка ввода пятизначного кода в поле для смс.")
    public void testInputIncorrectCodeSms() {

        authSteps.InputNumberAndCode(getPhoneForAuth(Phone.FOR_ORDER_1), "43252");
    }

    @Test
    @Description("Проверка наличия стран в дропдауне выбора формата телефона.")
    public void testCheckCountriesForPhone() {
        String[] expectedTexts = {"Russia (Россия)","Belarus (Беларусь)", "Kazakhstan (Казахстан)"};
        authSteps.clickLoginBtn()
                .clickCountrySelector()
                .checkPhoneCountry(expectedTexts);
    }
    @Test
    @Description("Проверка авторизации проблемного пользователя.")
    public void testCheckAuthProblemClient() throws InterruptedException {
       String idCustomer = customersAdminApiSteps.getListCustomers("BlockUser",Phone.FOR_BLOCK);
       customersAdminApiSteps.setStatusCustomer(idCustomer,"6","Проблемный");

       authSteps.loginUserCorrect(getPhoneForAuth(Phone.FOR_BLOCK), Phone.SMS_CODE);
    }
    @Test
    @Description("Проверка авторизации заблокированного пользователя.")
    public void testCheckAuthBlockClient() throws InterruptedException {
        String idCustomer = customersAdminApiSteps.getListCustomers("BlockUser",Phone.FOR_BLOCK);
        customersAdminApiSteps.setStatusCustomer(idCustomer,"7","Заблокированный");

        authSteps.loginUserCorrectWithoutConfirmLocation(getPhoneForAuth(Phone.FOR_BLOCK), Phone.SMS_CODE);
        authSteps.checkAuthMsg("Вы были заблокированы");
    }

}
