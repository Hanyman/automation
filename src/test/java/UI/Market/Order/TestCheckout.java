package UI.Market.Order;

import constants.data.*;
import env.Base_Container;
import helpers.Generator;
import helpers.api.ApiHelper;
import helpers.logs.TestListener;
import io.qameta.allure.Description;
import io.qameta.allure.Story;
import io.restassured.specification.RequestSpecification;
import org.aeonbits.owner.ConfigFactory;
import org.testng.annotations.*;
import steps.UI.OpenPageSteps;
import steps.UI.market.PersonnelData.AddressesSteps;
import steps.UI.market.PersonnelData.ContactsSteps;
import steps.api.market.AddressApiSteps;
import steps.api.market.CartApiSteps;
import steps.api.market.LoginMarketApiSteps;
import steps.UI.market.AuthSteps;
import steps.UI.market.CheckoutSteps;
import steps.api.market.ProfileApiSteps;

import static helpers.Helper.getPhoneForAuth;
import static helpers.Helper.waitPageLoaded;
import static helpers.api.Spec_api.spec_client_with_token_market;

@Listeners({ TestListener.class })
@Story("Маркет. Оформление заказа")
public class TestCheckout extends Base_Container {

    AuthSteps authSteps = new AuthSteps();
    CheckoutSteps checkoutSteps = new CheckoutSteps();
    AddressesSteps addressSteps = new AddressesSteps();
    CartApiSteps cartApiSteps = new CartApiSteps();
    LoginMarketApiSteps marketApiSteps = new LoginMarketApiSteps();
    ProfileApiSteps profileApiSteps = new ProfileApiSteps();
    AddressApiSteps addressApiSteps = new AddressApiSteps();
    String apims_session;
    private static RequestSpecification marketSpecification;
    OpenPageSteps openPageSteps = new OpenPageSteps();
    ContactsSteps contactsSteps = new ContactsSteps();
    Generator generator;

    // Перед каждым тестом, авторизуемся под клиентом, получаем куку и удаляем содержимое корзины
    @BeforeMethod
    public void setUpMethod() throws InterruptedException {
        generator = new Generator();
        marketSpecification = getTokensMarket(Phone.FOR_ORDER_DEFAULT);
        profileApiSteps.setUpDefaultProfile(marketSpecification);
        cartApiSteps.clearCartApi(marketSpecification);
        cartApiSteps.addProductsInCartApi(marketSpecification, generalMerchConfig.getProductFirst(), 1, generalMerchConfig.getStore());
        profileApiSteps.deleteReceivers(marketSpecification);
        addressApiSteps.setDefaultAddress(marketSpecification);
        marketApiSteps.logoutClient(marketSpecification);

        authSteps.loginUserCorrect(getPhoneForAuth(Phone.FOR_ORDER_DEFAULT), Phone.SMS_CODE);

        apims_session = ApiHelper.getCookies(APIMS_SESSION);
        marketSpecification = spec_client_with_token_market(url_market, apims_session, APIMS_SESSION);
        openPageSteps.stepOpenPageMarket("cart");

    }

    @AfterMethod
    public void tearDownMethod(){
        marketApiSteps.logoutClient(marketSpecification);
        //clearBrowser();
    }

    @Test
    @Description("Оформление заказа курьерской доставкой способ оплаты онлайн.")
    public void testAddOrderExpressDeliveryOnline() {
        // prepare
        String firstName = generator.getFirstName();
        String lastName = generator.getLastName();
        String phone = generator.getPhoneNumb();
        String email = generator.getEmail();

        profileApiSteps.createNewReceivers(marketSpecification,firstName,lastName,phone, email);

        // action
        checkoutSteps.clickCheckoutCartBtn();
        waitPageLoaded(getWebDriver(),globalTimeout);
        checkoutSteps.waitDisappearLoader()
                .clickExpressDeliveryBlock()
                .clickOnRecipient( lastName + " " + firstName)
                .clickOnCheckoutPayment("Онлайн оплата")
                .clickOnConfirmationCheckBox()
                .clickCheckoutBtn()
                .waitDisappearLoaderSpinnerEndCheckout()
                .clickExitPaymentProcess();

        // verification
        checkoutSteps.checkLastOrder("Доставка курьером");

    }

    @Test
    @Description("Оформление заказа курьерской доставкой способ наличные.")
    public void testAddOrderExpressDeliveryCash() {
        // prepare
        String firstName = generator.getFirstName();
        String lastName = generator.getLastName();
        String phone = generator.getPhoneNumb();
        String email = generator.getEmail();

        profileApiSteps.createNewReceivers(marketSpecification,firstName,lastName,phone,email);

        // action
        checkoutSteps.clickCheckoutCartBtn();
        waitPageLoaded(getWebDriver(),globalTimeout);
        checkoutSteps.waitDisappearLoader()
                .clickExpressDeliveryBlock()
                .clickOnRecipient( lastName + " " + firstName)
                .clickOnCheckoutPayment("Наличные")
                .clickOnConfirmationCheckBox()
                .clickCheckoutBtn()
                .waitDisappearLoaderSpinnerEndCheckout();

        // verification
        checkoutSteps.checkLastOrder("Доставка курьером");

    }

    @Test
    @Description("Оформление заказа самовывоз способ оплаты банковская карта.")
    public void testAddOrderPickupBankCard() {
        // prepare
        String firstName = generator.getFirstName();
        String lastName = generator.getLastName();
        String phone = generator.getPhoneNumb();
        String email = generator.getEmail();

        profileApiSteps.createNewReceivers(marketSpecification,firstName,lastName,phone,email);

        // action
        checkoutSteps.clickCheckoutCartBtn();
        waitPageLoaded(getWebDriver(),globalTimeout);
        checkoutSteps.waitDisappearLoader()
                .choosePickupPoint()
                .clickOnRecipient( lastName + " " + firstName)
                .clickOnCheckoutPayment("Банковский перевод")
                .clickOnConfirmationCheckBox()
                .clickCheckoutBtn()
                .waitDisappearLoaderSpinnerEndCheckout();

        // verification
        checkoutSteps.checkLastOrder("Доставка в пункт выдачи");

    }

    @Test
    @Description("Оформление заказа без получателя.")
    public void testAddOrderWithoutRecipients() {
        profileApiSteps.deleteReceivers(marketSpecification);

        // action
        checkoutSteps.clickCheckoutCartBtn();
        waitPageLoaded(getWebDriver(),globalTimeout);
        checkoutSteps.waitDisappearLoader()
                .clickOnConfirmationCheckBox()
                .clickCheckoutBtn();

        // verification
        checkoutSteps.checkMsgInBlockRecipient();

    }

    @Test
    @Description("Оформление заказа без адреса.")
    public void testAddOrderWithoutAddress() {
        String firstName = generator.getFirstName();
        String lastName = generator.getLastName();
        String phone = generator.getPhoneNumb();
        String email = generator.getEmail();

        profileApiSteps.createNewReceivers(marketSpecification,firstName,lastName,phone,email);
        addressApiSteps.deleteAddresses(marketSpecification);

        // action
        checkoutSteps.clickCheckoutCartBtn();
        waitPageLoaded(getWebDriver(),globalTimeout);
        checkoutSteps.waitDisappearLoader()
                .clickExpressDeliveryBlock()
                .clickOnRecipient( lastName + " " + firstName)
                .clickOnConfirmationCheckBox()
                .clickCheckoutBtn();

        // verification
        checkoutSteps.checkMsgInBlockAddress();

    }

    @Test
    @Description("Оформление заказа без подтверждения.")
    public void testAddOrderWithoutConfirmation() {
        String firstName = generator.getFirstName();
        String lastName = generator.getLastName();
        String phone = generator.getPhoneNumb();
        String email = generator.getEmail();

        profileApiSteps.createNewReceivers(marketSpecification,firstName,lastName,phone,email);

        // action
        checkoutSteps.clickCheckoutCartBtn();
        waitPageLoaded(getWebDriver(),globalTimeout);
        checkoutSteps.waitDisappearLoader()
                .clickExpressDeliveryBlock()
                .clickOnRecipient( lastName + " " + firstName);

        // verification
        checkoutSteps.checkMsgInBlockCheckoutConfirmation();
    }

    @Test
    @Description("Добавление нового получателя при оформлении заказа.")
    public void testAddNewRecipientsInCheckout() throws InterruptedException {
        profileApiSteps.deleteReceivers(marketSpecification);
        String firstName = generator.getFirstName();
        String lastName = generator.getLastName();
        String phone = "+7 900 555-44-33";
        String email = generator.getEmail();

        // action
        checkoutSteps.clickCheckoutCartBtn();
        waitPageLoaded(getWebDriver(),globalTimeout);
        checkoutSteps.waitDisappearLoader().clickAddNewRecipient();
        contactsSteps.inputRecipientFields(lastName + " " + firstName,"9005554433",email);

        // verification
        checkoutSteps.checkRecipientRow(lastName + " " + firstName,phone,email);
    }

    @Test
    @Description("Редактирование существующего получателя при оформлении заказа.")
    public void testEditCurrentRecipientsInCheckout() throws InterruptedException {
        profileApiSteps.deleteReceivers(marketSpecification);
        String defaultFirstName = generator.getFirstName();
        String defaultLastName = generator.getLastName();
        String defaultPhone = generator.getPhoneNumb();
        String defaultEmail = generator.getEmail();

        profileApiSteps.createNewReceivers(marketSpecification,defaultFirstName,defaultLastName,defaultPhone,defaultEmail);

        String firstName = generator.getFirstName();
        String lastName = generator.getLastName();
        String phone = "+7 900 666-55-33";
        String email = generator.getEmail();

        // action
        checkoutSteps.clickCheckoutCartBtn();
        waitPageLoaded(getWebDriver(),globalTimeout);
        checkoutSteps.waitDisappearLoader().clickEditRecipient();
        contactsSteps.inputRecipientFields(lastName + " " + firstName,"9006665533",email);

        // verification
        checkoutSteps.checkRecipientRow(lastName + " " + firstName,phone,email);
    }

    @Test
    @Description("Добавление нового адреса при оформлении заказа.")
    public void testAddNewAddressInCheckout() throws InterruptedException {
        addressApiSteps.deleteAddresses(marketSpecification);
        String address = "г Москва, ул Казакова, д 3";

        // action
        checkoutSteps.clickCheckoutCartBtn();
        waitPageLoaded(getWebDriver(),globalTimeout);
        checkoutSteps.waitDisappearLoader().clickAddNewAddress();
        addressSteps.setNewAddress(address);
        addressSteps.clickBringHereBtn();

        // verification
        checkoutSteps.checkAddressRow(address);
    }

}
