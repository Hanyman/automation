package UI.Market.Order;

import constants.data.*;
import env.Base_Container;
import helpers.Generator;
import helpers.api.ApiHelper;
import helpers.logs.TestListener;
import io.qameta.allure.Story;
import io.qameta.allure.Description;
import io.restassured.specification.RequestSpecification;
import org.testng.ITestResult;
import org.testng.annotations.*;
import org.testng.annotations.Listeners;
import steps.UI.OpenPageSteps;
import steps.api.market.CartApiSteps;
import steps.api.market.LoginMarketApiSteps;
import steps.UI.market.AuthSteps;
import steps.UI.market.CancelOrderSteps;

import java.util.HashMap;
import java.util.Map;

import static helpers.Helper.getPhoneForAuth;
import static helpers.Helper.getTestName;
import static helpers.api.Spec_api.spec_client_with_token_market;

@Listeners({ TestListener.class })
@Story("Маркет. Отмена заказа")
public class TestCancelOrder extends Base_Container {

    AuthSteps authSteps = new AuthSteps();
    CartApiSteps cartApiSteps = new CartApiSteps();
    LoginMarketApiSteps marketApiSteps = new LoginMarketApiSteps();
    int idOrder;
    String numbOrder,apims_session;
    Map<String, String> idsOrder = new HashMap<String, String>();
    private static RequestSpecification marketSpecification;
    OpenPageSteps openPageSteps = new OpenPageSteps();
    CancelOrderSteps cancelOrderSteps = new CancelOrderSteps();
    Generator generator;

    @BeforeMethod
    public void setUpMethod(ITestResult iTestResult) throws InterruptedException {
        generator = new Generator();
        marketSpecification = getTokensMarket(Phone.FOR_ORDER_DEFAULT);
        cartApiSteps.clearCartApi(marketSpecification);
        cartApiSteps.addProductsInCartApi(marketSpecification, generalMerchConfig.getProductFirst(), 1, generalMerchConfig.getStore());
        String bodyForOrder = cartApiSteps.checkDataCheckout(marketSpecification,"3");
        idsOrder = cartApiSteps.checkoutCommit(marketSpecification,bodyForOrder);
        idOrder = Integer.parseInt(idsOrder.get("idOrder"));
        numbOrder = idsOrder.get("numberOrder");
        marketApiSteps.logoutClient(marketSpecification);

        authSteps.loginUserCorrect(getPhoneForAuth(Phone.FOR_ORDER_DEFAULT), Phone.SMS_CODE);
        apims_session = ApiHelper.getCookies(APIMS_SESSION);
        marketSpecification = spec_client_with_token_market(url_market, apims_session, APIMS_SESSION);
        openPageSteps.stepOpenPageMarket("profile/orders/"+"?v="+getTestName(iTestResult)+"");
    }

    @AfterMethod
    public void tearDownMethod(){
        marketApiSteps.logoutClient(marketSpecification);
        //clearBrowser();
    }

    @Test(enabled = true)
    @Description("Отмена неоплаченного заказа.")
    public void testCancelOrderUnPayment() {

        // action
        cancelOrderSteps.clickCancelOrder(numbOrder)
                        .checkStatusOrderCancel("ОТМЕНЕН");
    }
}
