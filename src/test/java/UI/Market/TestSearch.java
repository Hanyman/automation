package UI.Market;

import constants.data.*;
import env.Base_Container;
import helpers.logs.TestListener;
import io.qameta.allure.Description;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;
import steps.UI.OpenPageSteps;
import steps.UI.market.SearchFiltersSteps;

@Listeners({ TestListener.class })
public class TestSearch extends Base_Container {
    SearchFiltersSteps searchFiltersSteps = new SearchFiltersSteps();
    OpenPageSteps openPageSteps = new OpenPageSteps();

    @Test
    @Description("S-2. Проверка отображения подсказки при поиске и результатов поиска.")
    public void testSearchItemThroughHint() {
        openPageSteps.stepOpenPageMarket("");

        searchFiltersSteps.inputProductNameInSearchField(Products.PRODUCT_TEST_1);
    }

    @Test
    @Description("S-3. Поиск несуществующего товара.")
    public void testSearchNotExistProduct() {

       searchFiltersSteps.inputNameFieldInSearch("FGTR")
               .checkEmptyProduct();

    }

    @Test
    @Description("S-4. Поиск не согласованного товара.")
    public void testSearchNotAgreedProduct() {

       searchFiltersSteps.inputNameFieldInSearch(Products.PRODUCT_NOT_AGREED)
               .checkEmptyProduct();

    }

}
