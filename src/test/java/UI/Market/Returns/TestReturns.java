package UI.Market.Returns;

import constants.data.*;
import constants.data.Phone;
import env.Base_Container;
import helpers.api.ApiHelper;
import helpers.logs.TestListener;
import io.qameta.allure.Description;
import io.qameta.allure.Story;
import io.restassured.specification.RequestSpecification;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;
import steps.UI.OpenPageSteps;
import steps.UI.market.AuthSteps;
import steps.UI.market.ReturnsSteps;
import steps.api.admin.Returns.ReasonsAdminApiSteps;
import steps.api.market.*;
import steps.api.merchant.OrdersMerchApiSteps;

import java.util.ArrayList;

import static constants.data.DEFAULT_ADDRESS;
import static constants.data.DEFAULT_ADDRESS_FOR_RETURN;
import static helpers.Helper.getPhoneForAuth;
import static helpers.Helper.getTestName;
import static helpers.api.Spec_api.spec_client_with_token_market;

@Listeners({ TestListener.class })
@Story("")
public class TestReturns extends Base_Container {

    AuthSteps authSteps = new AuthSteps();
    OpenPageSteps openPageSteps = new OpenPageSteps();
    ReturnsApiSteps returnsApiSteps = new ReturnsApiSteps();
    OrdersMerchApiSteps ordersMerchApiSteps = new OrdersMerchApiSteps();
    ReturnsSteps returnsSteps = new ReturnsSteps();
    ReasonsAdminApiSteps reasonsAdminApiSteps = new ReasonsAdminApiSteps();
    LoginMarketApiSteps marketApiSteps = new LoginMarketApiSteps();
    private static RequestSpecification marketSpecification,marketSpec;

    String numbOrderForReturn;
    ArrayList<Integer> itemId;
    // Перед каждым тестом, авторизуемся под клиентом, получаем куку и удаляем содержимое корзины
    @BeforeMethod
    public void setUpMethod(ITestResult iTestResult) throws InterruptedException {
        newOrder(iTestResult,"no");
    }

    @AfterMethod
    private void postconditionMethod(){
        marketApiSteps.logoutClient(marketSpec);
    }

    @Test
    @Description("Оформление возврата курьерской доставкой.")
    public void testAddReturnExpressDelivery() throws InterruptedException {
        String clientNameReason = reasonsAdminApiSteps.getListReasonsClientName(ReturnReasons.SIZE_NOT_FIT);
        returnsSteps.clickReturnOrderOnCard(numbOrderForReturn);
        returnsSteps.chooseProductToReturn(String.valueOf(itemId.get(0)));
        returnsSteps.clickOrderReturnBtn();
        returnsSteps.clickOnReasonReturn(clientNameReason);
        returnsSteps.textProblemDescription("описание проблемы");
        returnsSteps.uploadPhoto();
        returnsSteps.nextReturnBtn();

        returnsSteps.clickExpressDelivery();
        returnsSteps.fillAddress(DEFAULT_ADDRESS_FOR_RETURN);
        returnsSteps.checkTimeslotsBlock();
        returnsSteps.createReturnBtn();
        returnsSteps.finishReturnBtn();
        returnsSteps.checkStatusReturn("ОФОРМЛЕН ВОЗВРАТ");

    }

    private void newOrder(ITestResult ITestResult, String alwaysNew) throws InterruptedException {
        marketSpecification = getTokensMarket(Phone.FOR_RETURN_2);
        numbOrderForReturn = String.valueOf(ordersMerchApiSteps.createNewOrder(marketSpecification, generalMerchConfig.getProductFirst(), 1, PaymentMethods.CASH,alwaysNew));
        itemId = returnsApiSteps.getOrderData(marketSpecification, Integer.parseInt(numbOrderForReturn));
        marketApiSteps.logoutClient(marketSpecification);
        marketSpec = login(Phone.FOR_RETURN_2);
        openPageSteps.stepOpenPageMarket("profile/orders/"+"?v="+getTestName(ITestResult)+"");
    }

    private RequestSpecification login(String phone) throws InterruptedException {
        RequestSpecification marketSpec;
        String apims_session;
        authSteps.loginUserCorrect(getPhoneForAuth(phone),Phone.SMS_CODE);
        apims_session = ApiHelper.getCookies(APIMS_SESSION);
        marketSpec = spec_client_with_token_market(url_market, apims_session, APIMS_SESSION);
        return marketSpec;
    }


}
