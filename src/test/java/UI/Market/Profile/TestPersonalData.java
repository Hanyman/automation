package UI.Market.Profile;

import com.codeborne.selenide.Selenide;
import constants.ErrorsMsg;
import constants.data.*;
import env.Base_Container;
import helpers.Generator;
import helpers.api.ApiHelper;
import helpers.logs.TestListener;
import io.qameta.allure.Description;
import io.restassured.specification.RequestSpecification;

import org.testng.ITestResult;
import org.testng.annotations.*;
import steps.UI.OpenPageSteps;
import steps.UI.market.AuthSteps;
import steps.UI.market.PersonnelData.ProfileSteps;
import steps.api.market.LoginMarketApiSteps;
import steps.api.market.ProfileApiSteps;

import static helpers.Helper.getPhoneForAuth;
import static helpers.Helper.getTestName;
import static helpers.api.Spec_api.spec_client_with_token_market;
@Listeners({ TestListener.class })
public class TestPersonalData extends Base_Container {

    Generator generator = new Generator();

    AuthSteps authSteps = new AuthSteps();
    ProfileSteps profileSteps = new ProfileSteps();
    OpenPageSteps openPageSteps = new OpenPageSteps();
    String apims_session;
    private static RequestSpecification marketSpecification;
    ProfileApiSteps profileApiSteps = new ProfileApiSteps();
    LoginMarketApiSteps marketApiSteps = new LoginMarketApiSteps();
    @BeforeClass
    public void openPage() throws InterruptedException {
        //authSteps.loginUserCorrect(getPhoneForAuth(Phone.FOR_PROFILE),Phone.SMS_CODE);
        authSteps.loginUserCorrect(getPhoneForAuth(Phone.FOR_PROFILE),Phone.SMS_CODE);
        apims_session = ApiHelper.getCookies(APIMS_SESSION);
        marketSpecification = spec_client_with_token_market(url_market, apims_session, APIMS_SESSION);

    }

    @BeforeMethod
    public void setUpMethod(ITestResult iTestResult) {
        profileApiSteps.setUpDefaultProfile(marketSpecification);
        openPageSteps.stepOpenPageMarket("profile"+"?v="+getTestName(iTestResult)+"");

    }

    @AfterClass
    public void tearDownClass(){
        marketApiSteps.logoutClient(marketSpecification);
        Selenide.closeWebDriver();
    }

    @Test
    @Description("Проверка редактирования профиля.")
    public void testEditDataProfile() {
        String newName = generator.lastName;

        profileSteps.stepClearFieldFio()
            .inputNameField(newName)
            .stepClickReadyBtn()
            .stepCheckDisplayAlert();

        //verification
        profileSteps.stepCheckFieldFio(newName);
    }

    @Test
    @Description("Проверка сохранения профиля с пустыми полями.")
    public void testCheckFieldNameOnObligatory() {

        profileSteps.stepClearFieldFio()
             .stepClearFieldFio()
             .stepClearFieldEmail()
             .clearFieldPhone();

        //verification
        profileSteps.stepCheckErrorFieldFio(ErrorsMsg.obligatory_name_field.getErrorMsg());
               // .stepCheckErrorInvalidEmailFieldEmail(ErrorsMsg.obligatory_email_field.getErrorMsg());
    }

    @Test
    @Description("Проверка лимита ввода символов в поле ФИО.")
    public void testCheckExceedingLimitFieldName() {
        String longName = generator.getLongName();

        profileSteps.stepClearFieldFio()
            .inputNameField(longName);

        //verification
        profileSteps.stepCheckErrorFieldFio(ErrorsMsg.maximum_length__surname_field.getErrorMsg());
    }

    @Test
    @Description("Проверка ввода некорректных символов в поле Телефон.")
    public void testCheckFieldPhoneIncorrectSymbols() {

        profileSteps.clearFieldPhone()
                .inputPhoneField("!@#^&_RED");

        //verification
        profileSteps.stepCheckFieldPhone();
    }

    @Test
    @Description("Проверка ввода невалидного Email.")
    public void testInputIncorrectEmail() {

        profileSteps.stepClearFieldEmail()
                .inputEmailField("email");

        //verification
        profileSteps.stepCheckErrorInvalidEmailFieldEmail(ErrorsMsg.incorrect_email.getErrorMsg());
    }


}
