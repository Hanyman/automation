package UI.Market.Profile;

import com.codeborne.selenide.Selenide;
import constants.ErrorsMsg;
import constants.data.Phone;
import env.Base_Container;
import helpers.Generator;
import helpers.api.ApiHelper;
import helpers.logs.TestListener;
import io.qameta.allure.Description;
import io.restassured.specification.RequestSpecification;
import org.testng.ITestResult;
import org.testng.annotations.*;
import steps.UI.OpenPageSteps;
import steps.UI.market.AuthSteps;
import steps.UI.market.PersonnelData.ProfileSteps;
import steps.api.market.LoginMarketApiSteps;
import steps.api.market.ProfileApiSteps;

import static helpers.Helper.getPhoneForAuth;
import static helpers.Helper.getTestName;
import static helpers.api.ApiHelper.getLengthJson;
import static helpers.api.Spec_api.spec_client_with_token_market;
@Listeners({ TestListener.class })
public class TestMessages extends Base_Container {

    Generator generator = new Generator();

    AuthSteps authSteps = new AuthSteps();
    ProfileSteps profileSteps = new ProfileSteps();
    OpenPageSteps openPageSteps = new OpenPageSteps();
    String apims_session;
    private static RequestSpecification marketSpecification;
    ProfileApiSteps profileApiSteps = new ProfileApiSteps();
    LoginMarketApiSteps marketApiSteps = new LoginMarketApiSteps();
    @BeforeClass
    public void openPage() throws InterruptedException {
        authSteps.loginUserCorrect(getPhoneForAuth(Phone.FOR_PROFILE),Phone.SMS_CODE);
        apims_session = ApiHelper.getCookies(APIMS_SESSION);
        marketSpecification = spec_client_with_token_market(url_market, apims_session, APIMS_SESSION);
    }
    @AfterClass
    public void tearDownClass(){
        marketApiSteps.logoutClient(marketSpecification);
        Selenide.closeWebDriver();
    }

    @BeforeMethod
    public void setUpMethod(ITestResult iTestResult) {
        profileApiSteps.setUpDefaultProfile(marketSpecification);
        openPageSteps.stepOpenPageMarket("profile"+"?v="+getTestName(iTestResult)+"");

    }

    @Test
    @Description("Проверка редактирования профиля.")
    public void testEditDataProfile() {

    }


}
