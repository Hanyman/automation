package UI.Market.Profile;

import com.codeborne.selenide.Selenide;
import constants.data.Phone;
import env.Base_Container;
import helpers.Generator;
import helpers.api.ApiHelper;
import helpers.logs.TestListener;
import io.qameta.allure.Description;
import io.restassured.specification.RequestSpecification;
import org.testng.ITestResult;
import org.testng.annotations.*;
import steps.UI.OpenPageSteps;
import steps.UI.market.AuthSteps;
import steps.UI.market.PersonnelData.ContactsSteps;
import steps.UI.market.PersonnelData.ProfileSteps;
import steps.api.market.LoginMarketApiSteps;
import steps.api.market.ProfileApiSteps;

import static helpers.Helper.getPhoneForAuth;
import static helpers.Helper.getTestName;
import static helpers.api.Spec_api.spec_client_with_token_market;

@Listeners({ TestListener.class })
public class TestContacts extends Base_Container {

    Generator generator = new Generator();

    AuthSteps authSteps = new AuthSteps();
    OpenPageSteps openPageSteps = new OpenPageSteps();
    String apims_session;
    private static RequestSpecification marketSpecification;
    ProfileApiSteps profileApiSteps = new ProfileApiSteps();
    LoginMarketApiSteps marketApiSteps = new LoginMarketApiSteps();
    ContactsSteps contactsSteps = new ContactsSteps();
    @BeforeClass
    public void openPage() throws InterruptedException {
        authSteps.loginUserCorrect(getPhoneForAuth(Phone.FOR_PROFILE),Phone.SMS_CODE);
        apims_session = ApiHelper.getCookies(APIMS_SESSION);
        marketSpecification = spec_client_with_token_market(url_market, apims_session, APIMS_SESSION);
        profileApiSteps.deleteReceivers(marketSpecification);
    }
    @AfterClass
    public void tearDownClass(){
        marketApiSteps.logoutClient(marketSpecification);
        Selenide.closeWebDriver();
    }

    @BeforeMethod
    public void setUpMethod(ITestResult iTestResult) {
        profileApiSteps.deleteReceivers(marketSpecification);
        openPageSteps.stepOpenPageMarket("profile/contacts"+"?v="+getTestName(iTestResult)+"");

    }

    @Test
    @Description("Проверка Добавления нового получателя.")
    public void testAddNewRecipient() throws InterruptedException {
        String firstName = generator.getFirstName();
        String lastName = generator.getLastName();
        String phone = "+7 900 276-33-08";
        String email = generator.getEmail();

        // action
        contactsSteps.clickAddNewRecipient()
                .inputRecipientFields(lastName + " " + firstName,"9002763308",email);

        // verification
        contactsSteps.checkRecipientRow(lastName + " " + firstName,phone,email);
    }

    @Test
    @Description("Проверка редактирования получателя.")
    public void testEditCurrentRecipient() throws InterruptedException {
        profileApiSteps.createNewReceivers(marketSpecification,"firstName","lastName","9009999999","testemail11@gmail.com");

        String firstName = generator.getFirstName();
        String lastName = generator.getLastName();
        String phone = "+7 900 111-11-99";
        String email = generator.getEmail();
        openPageSteps.stepOpenPageMarket("profile/contacts");
        // action
        contactsSteps.clickEditRecipient("lastName")
                .inputRecipientFields(lastName + " " + firstName,"9001111199",email);

        // verification
        contactsSteps.checkRecipientRow(lastName + " " + firstName,phone,email);
    }

    @Test
    @Description("Проверка удаления получателя.")
    public void testDeleteCurrentRecipient() throws InterruptedException {
        profileApiSteps.createNewReceivers(marketSpecification,"firstName","lastName","9009999999","testemail11@gmail.com");
        openPageSteps.stepOpenPageMarket("profile/contacts");
        // action
        contactsSteps.clickRemoveRecipient("lastName");

        // verification
        contactsSteps.checkRecipientEmptyRow();
    }

    @Test
    @Description("Проверка обязательных полей при добавлении нового получателя.")
    public void testCheckRequiredFields() throws InterruptedException {
        profileApiSteps.createNewReceivers(marketSpecification,"firstName","lastName","9009999999","testemail11@gmail.com");

        // action
        contactsSteps.clickAddNewRecipient()
                .clickSaveRecipient();

        // verification
        contactsSteps.checkRecipientNameFieldMsg("Обязательное поле");
        contactsSteps.checkRecipientPhoneFieldMsg("Обязательное поле");
    }


}
