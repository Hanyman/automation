package UI.Market.Profile.Preferences;

import com.codeborne.selenide.Selenide;
import constants.data;
import constants.data.Phone;
import env.Base_Container;
import helpers.api.ApiHelper;
import helpers.logs.TestListener;
import io.qameta.allure.Description;
import io.restassured.specification.RequestSpecification;
import org.testng.ITestResult;
import org.testng.annotations.*;
import steps.UI.OpenPageSteps;
import steps.UI.market.AuthSteps;
import steps.UI.market.PersonnelData.AddressesSteps;
import steps.api.market.AddressApiSteps;
import steps.api.market.LoginMarketApiSteps;

import static helpers.Helper.*;
import static helpers.api.Spec_api.spec_client_with_token_market;

@Listeners({ TestListener.class })
public class TestAddresses extends Base_Container {

    AuthSteps authSteps = new AuthSteps();
    OpenPageSteps openPageSteps = new OpenPageSteps();
    String apims_session;
    private static RequestSpecification marketSpecification;
    AddressApiSteps addressApiSteps = new AddressApiSteps();
    AddressesSteps addressPage = new AddressesSteps();
    LoginMarketApiSteps marketApiSteps = new LoginMarketApiSteps();
    @BeforeClass
    public void openPage() throws InterruptedException {
        authSteps.loginUserCorrect(getPhoneForAuth(Phone.FOR_PROFILE),Phone.SMS_CODE);
        apims_session = ApiHelper.getCookies(APIMS_SESSION);
        marketSpecification = spec_client_with_token_market(url_market, apims_session, APIMS_SESSION);

    }
    @AfterClass
    public void tearDownClass(){
        marketApiSteps.logoutClient(marketSpecification);
        Selenide.closeWebDriver();
    }

    @BeforeMethod
    public void setUpMethod(ITestResult iTestResult) {
        addressApiSteps.deleteAddresses(marketSpecification);
        openPageSteps.stepOpenPageMarket("profile/addresses"+"?v="+getTestName(iTestResult)+"");

    }

    @Test
    @Description("Проверка удаления существующего адреса.")
    public void testDeleteExistAddress() {

        // action
        addressApiSteps.setDefaultAddress(marketSpecification);
        openPageSteps.stepOpenPageMarket("profile/addresses");
        addressPage.clickOnRemoveAddressBtn(data.DEFAULT_ADDRESS);
        waitPageLoaded(getWebDriver(),globalTimeout);

        // verification
        addressPage.checkNoExistAddressCard(data.DEFAULT_ADDRESS);
    }

    @Test(enabled = false)
    @Description("Проверка редактирования существующего адреса.")
    public void testEditAddress() throws InterruptedException {

        // prepare
        String address = "г Москва, ул Лениногорская, д 1";

        // action
        addressApiSteps.setDefaultAddress(marketSpecification);
        addressPage.clickOnEditAddressBtn(data.DEFAULT_ADDRESS);
        addressPage.setNewAddress(address);
        addressPage.clickBringHereBtn();

        // verification
        addressPage.checkAddressCard(address);
    }

    @Test
    @Description("Проверка добавления нового адреса.")
    public void testAddNewAddress() throws InterruptedException {

        // prepare
        String address = "г Москва, ул Казакова, д 3";

        // action
        addressPage.clickAddAddressBtn();
        waitPageLoaded(getWebDriver(),globalTimeout);
        addressPage.setNewAddress(address);
        addressPage.clickBringHereBtn();

        // verification
        addressPage.checkAddressCard(address);
    }

    @Test
    @Description("Проверка выбора адреса как дефолтного.")
    public void testCheckSetDefaultAddress() {

        // prepare
        String street = "ул Казакова";
        String numberHome = "д 16";
        String block = "стр 2";
        String geo_lat = "55.762646";
        String geo_lon = "37.66566";
        String index = "105064";
        String address = "г Москва, ул Казакова, д 16";

        // action
        addressApiSteps.setDefaultAddress(marketSpecification);
        addressApiSteps.setAddress(marketSpecification,street,numberHome,block,geo_lat,geo_lon, index);
        openPageSteps.stepOpenPageMarket("profile/addresses");
        addressPage.clickAddressCard(address);

        // verification
        addressPage.checkDefaultAddressCard(address);
    }


}
