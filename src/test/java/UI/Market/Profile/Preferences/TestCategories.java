package UI.Market.Profile.Preferences;

import com.codeborne.selenide.Selenide;
import constants.data.*;
import env.Base_Container;
import helpers.api.ApiHelper;
import helpers.logs.TestListener;
import io.qameta.allure.Description;
import io.restassured.specification.RequestSpecification;
import org.testng.ITestResult;
import org.testng.annotations.*;
import steps.UI.OpenPageSteps;
import steps.UI.market.AuthSteps;
import steps.UI.market.PersonnelData.PreferencesSteps;
import steps.api.market.CatalogApiSteps;
import steps.api.market.LoginMarketApiSteps;
import steps.api.market.ProfileApiSteps;

import java.util.ArrayList;

import static helpers.Helper.*;
import static helpers.api.ApiHelper.getLengthJson;
import static helpers.api.ApiHelper.getNamePreference;
import static helpers.api.Spec_api.spec_client_with_token_market;
@Listeners({ TestListener.class })
public class TestCategories extends Base_Container {

    AuthSteps authSteps = new AuthSteps();
    PreferencesSteps preferencesSteps = new PreferencesSteps();
    OpenPageSteps openPageSteps = new OpenPageSteps();
    String apims_session,responseCategories, type = "categories";;
    private static RequestSpecification marketSpecification;
    ProfileApiSteps profileApiSteps = new ProfileApiSteps();
    LoginMarketApiSteps marketApiSteps = new LoginMarketApiSteps();
    CatalogApiSteps catalogApiSteps = new CatalogApiSteps();
    int countCategories;

    @BeforeClass
    public void openPage() throws InterruptedException {
        authSteps.loginUserCorrect(getPhoneForAuth(Phone.FOR_PROFILE),Phone.SMS_CODE);
        apims_session = ApiHelper.getCookies(APIMS_SESSION);
        marketSpecification = spec_client_with_token_market(url_market, apims_session, APIMS_SESSION);
        responseCategories = catalogApiSteps.getBrands(marketSpecification);
        countCategories = getLengthJson(responseCategories,type);
    }

    @AfterClass
    public void tearDownClass(){
        marketApiSteps.logoutClient(marketSpecification);
        Selenide.closeWebDriver();
    }

    @BeforeMethod
    public void setUpMethod(ITestResult iTestResult) {
        profileApiSteps.removeAllCategories(marketSpecification);
        //profileApiSteps.removeAllCategories(marketSpecification);
        openPageSteps.stepOpenPageMarket("profile/preferences"+"?v="+getTestName(iTestResult)+"");
    }

    @Test
    @Description("Проверка добавления категорий через кнопку Добавить по центру блока.")
    public void testAddCategoriesViaMiddleBtn() {
        // prepare
        String firstCategory = getNamePreference(responseCategories,0,type);

        // action
        preferencesSteps.clickAddCategoriesBtnMiddle()
                .chooseCategoryFromListAndSave(deleteQuotes(firstCategory));

        //verification
        preferencesSteps.CheckExistCategoryInBlock(deleteQuotes(firstCategory));
        preferencesSteps.clickAddCategoriesBtnRight()
                .CheckCategoryIsNotList(deleteQuotes(firstCategory));
    }

    @Test
    @Description("Проверка добавления категорий через кнопку Добавить справа от блока.")
    public void testAddCategoriesViaRightBtn() {
        // prepare
        String firstCategory = getNamePreference(responseCategories,0,type);

        // action
        preferencesSteps.clickAddCategoriesBtnRight()
                .chooseCategoryFromListAndSave(deleteQuotes(firstCategory));

        //verification
        preferencesSteps.CheckExistCategoryInBlock(deleteQuotes(firstCategory));
        preferencesSteps.clickAddCategoriesBtnRight()
                .CheckCategoryIsNotList(deleteQuotes(firstCategory));
    }

    @Test
    @Description("Проверка поиска категории в списке.")
    public void testSearchCategories() {
        // prepare
        String firstCategory = getNamePreference(responseCategories,0,type);
        String secondCategory = getNamePreference(responseCategories,1,type);

        // action
        preferencesSteps.clickAddCategoriesBtnRight()
                .SetValueInSearchFieldCategories(deleteQuotes(firstCategory));

        //verification
        preferencesSteps.CheckCategoryIsList(deleteQuotes(firstCategory));
        preferencesSteps.CheckCategoryIsNotList(deleteQuotes(secondCategory));

    }

    @Test
    @Description("Проверка закрытия окна списка категорий после выбора категории.")
    public void testCheckCloseModalPreferenceCategories() {
        // prepare
        String firstCategory = getNamePreference(responseCategories,0,type);

        // action
        preferencesSteps.clickAddCategoriesBtnRight()
                .chooseCategoryFromListAndClose(deleteQuotes(firstCategory));

        //verification
        preferencesSteps.CheckNotExistCategoryInBlock(deleteQuotes(firstCategory));
    }

    @Test
    @Description("Проверка удаления конкретной категории из блока.")
    public void testCheckDeleteCategory() {
        // prepare
        String firstCategory = getNamePreference(responseCategories,0,type);

        // action
        // prepare
        preferencesSteps.clickAddCategoriesBtnRight()
                .chooseCategoryFromListAndSave(deleteQuotes(firstCategory));


        //action
        preferencesSteps.clickRemoveCategoryBtn(deleteQuotes(firstCategory));
        //verification
        preferencesSteps.CheckNotExistCategoryInBlock(deleteQuotes(firstCategory));
    }

    @Test
    @Description("Проверка удаления всех категорий.")
    public void testCheckDeleteAllCategories() {
        // prepare
        String firstCategory = getNamePreference(responseCategories,0,type);
        String secondCategory = getNamePreference(responseCategories,1,type);
        // action
        preferencesSteps.clickAddCategoriesBtnRight()
                .chCategoryFromList(deleteQuotes(firstCategory))
                .chCategoryFromList(deleteQuotes(secondCategory))
                .ClickAddBtnInListCategories();;

        //action
        preferencesSteps.clickRemoveAllCategoryBtn();
        //verification
        preferencesSteps.CheckNotExistCategoryInBlock(deleteQuotes(firstCategory));
        preferencesSteps.CheckNotExistCategoryInBlock(deleteQuotes(secondCategory));
    }


}
