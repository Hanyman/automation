package UI.Market.Profile.Preferences;

import com.codeborne.selenide.Selenide;
import constants.data.Phone;
import env.Base_Container;
import helpers.api.ApiHelper;
import helpers.logs.TestListener;
import io.qameta.allure.Description;
import io.restassured.specification.RequestSpecification;
import org.testng.ITestResult;
import org.testng.annotations.*;
import steps.UI.OpenPageSteps;
import steps.UI.market.AuthSteps;
import steps.UI.market.PersonnelData.PreferencesSteps;
import steps.api.market.CatalogApiSteps;
import steps.api.market.LoginMarketApiSteps;
import steps.api.market.ProfileApiSteps;

import java.util.ArrayList;

import static helpers.Helper.*;
import static helpers.api.ApiHelper.getLengthJson;
import static helpers.api.ApiHelper.getNamePreference;
import static helpers.api.Spec_api.*;
@Listeners({ TestListener.class })
public class TestBrands extends Base_Container {

    AuthSteps authSteps = new AuthSteps();
    PreferencesSteps preferencesSteps = new PreferencesSteps();
    OpenPageSteps openPageSteps = new OpenPageSteps();
    String apims_session, responseBrands, type = "brands";
    int countBrands;
    private static RequestSpecification marketSpecification;
    ProfileApiSteps profileApiSteps = new ProfileApiSteps();
    CatalogApiSteps catalogApiSteps = new CatalogApiSteps();
    LoginMarketApiSteps marketApiSteps = new LoginMarketApiSteps();
    @BeforeClass
    public void openPage() throws InterruptedException {
        authSteps.loginUserCorrect(getPhoneForAuth(Phone.FOR_PROFILE),Phone.SMS_CODE);
        apims_session = ApiHelper.getCookies(APIMS_SESSION);
        marketSpecification = spec_client_with_token_market(url_market, apims_session, APIMS_SESSION);
        responseBrands = catalogApiSteps.getBrands(marketSpecification);
        countBrands = getLengthJson(responseBrands,type);
    }
    @AfterClass
    public void tearDownClass(){
        marketApiSteps.logoutClient(marketSpecification);
        Selenide.closeWebDriver();
    }

    @BeforeMethod
    public void setUpMethod(ITestResult iTestResult) {
        profileApiSteps.removeAllBrands(marketSpecification);
        //profileApiSteps.removeAllCategories(marketSpecification);
        openPageSteps.stepOpenPageMarket("profile/preferences"+"?v="+getTestName(iTestResult)+"");
    }

    @Test
    @Description("Проверка добавления брендов через кнопку Добавить по центру блока.")
    public void testAddBrandsViaMiddleBtn() {
        // prepare
        String firstBrand = getNamePreference(responseBrands,0,type);

        // action
        preferencesSteps.clickAddBtnMiddle()
                .chooseBrandFromListAndSave(deleteQuotes(firstBrand));

        //verification
        preferencesSteps.CheckExistBrandInBlock(deleteQuotes(firstBrand));
        preferencesSteps.clickAddBtnRight()
                .CheckBrandIsNotList(deleteQuotes(firstBrand));
    }

    @Test
    @Description("Проверка добавления брендов через кнопку Добавить справа от блока.")
    public void testAddBrandsViaRightBtn() {
        // prepare
        String firstBrand = getNamePreference(responseBrands,0,type);

        // action
        preferencesSteps.clickAddBtnRight()
                .chooseBrandFromListAndSave(deleteQuotes(firstBrand));

        //verification
        preferencesSteps.CheckExistBrandInBlock(deleteQuotes(firstBrand));
        preferencesSteps.clickAddBtnRight()
                .CheckBrandIsNotList(deleteQuotes(firstBrand));
    }

    @Test
    @Description("Проверка поиска бренда в списке.")
    public void testSearchBrands() {
        // prepare
        String firstBrand = getNamePreference(responseBrands,0,type);
        String secondBrand = getNamePreference(responseBrands,1,type);
        // action
        preferencesSteps.clickAddBtnRight()
                .SetValueInSearchField(deleteQuotes(firstBrand));

        //verification
        preferencesSteps.CheckBrandIsList(deleteQuotes(firstBrand));
        preferencesSteps.CheckBrandIsNotList(deleteQuotes(secondBrand));

    }

    @Test
    @Description("Проверка закрытия окна списка брендов после выбора брендов.")
    public void testCheckCloseModalPreference() {
        // prepare
        String firstBrand = getNamePreference(responseBrands,0,type);

        // action
        preferencesSteps.clickAddBtnRight()
                .chooseBrandFromListAndClose(deleteQuotes(firstBrand));

        //verification
        preferencesSteps.CheckNotExistBrandInBlock(deleteQuotes(firstBrand));
    }

    @Test
    @Description("Проверка удаления конкретного бренда из блока.")
    public void testCheckDeleteBrand() {
        // prepare
        String firstBrand = getNamePreference(responseBrands,0,type);

        // action
        // prepare
        preferencesSteps.clickAddBtnRight()
                .chooseBrandFromListAndSave(deleteQuotes(firstBrand));

        //action
        preferencesSteps.clickRemoveBrandBtn(deleteQuotes(firstBrand));
        //verification
        preferencesSteps.CheckNotExistBrandInBlock(deleteQuotes(firstBrand));
    }

    @Test
    @Description("Проверка удаления всех брендов.")
    public void testCheckDeleteAllBrand() {
        // prepare
        String firstBrand = getNamePreference(responseBrands,0,type);
        String secondBrand = getNamePreference(responseBrands,1,type);

        // action
        // prepare
        preferencesSteps.clickAddBtnRight()
                .chooseBrandFromList(deleteQuotes(firstBrand))
                .chooseBrandFromList(deleteQuotes(secondBrand))
                .ClickAddBtnInListBrands();;

        //action
        preferencesSteps.clickRemoveAllBrandBtn();
        //verification
        preferencesSteps.CheckNotExistBrandInBlock(deleteQuotes(firstBrand));
        preferencesSteps.CheckNotExistBrandInBlock(deleteQuotes(secondBrand));
    }


}
